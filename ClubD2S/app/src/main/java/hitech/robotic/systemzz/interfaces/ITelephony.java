package hitech.robotic.systemzz.interfaces;

public interface ITelephony {

    boolean endCall();

    void answerRingingCall();

    void silenceRinger();

}
