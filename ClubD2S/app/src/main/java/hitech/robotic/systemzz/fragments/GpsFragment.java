package hitech.robotic.systemzz.fragments;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import hitech.robotic.systemzz.LatLngPass;
import hitech.robotic.systemzz.R;
import hitech.robotic.systemzz.VehicleLocation;
import hitech.robotic.systemzz.geocoder.AndroidGeocoderDataSource;
import hitech.robotic.systemzz.geocoder.GeocoderPresenter;
import hitech.robotic.systemzz.geocoder.GeocoderRepository;
import hitech.robotic.systemzz.geocoder.GeocoderViewInterface;
import hitech.robotic.systemzz.geocoder.GoogleGeocoderDataSource;
import hitech.robotic.systemzz.geocoder.api.AddressBuilder;
import hitech.robotic.systemzz.geocoder.api.NetworkClient;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;

public class GpsFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnMapClickListener, GeocoderViewInterface {

    private GoogleMap map;
    private GoogleApiClient googleApiClient;
    private Location currentLocation;
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private Marker currentMarker;
    private boolean hasWiderZoom = false;
    private static final int DEFAULT_ZOOM = 16;
    private static final int WIDER_ZOOM = 6;
    private SupportMapFragment mMapFragment;
    private GoogleGeocoderDataSource apiInteractor;
    private GeocoderPresenter geocoderPresenter;
    private LocationRequest locationRequest;
    private static final int UPDATE_INTERVAL = 300;
    private static final int FASTEST_INTERVAL = 0;
    private static final int DISPLAYMENT = 10;
    LatLngPass latLngPass;

    public GpsFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gps, container, false);
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        apiInteractor = new GoogleGeocoderDataSource(new NetworkClient(), new AddressBuilder());
        GeocoderRepository geocoderRepository = new GeocoderRepository(new AndroidGeocoderDataSource(geocoder), apiInteractor);
        geocoderPresenter = new GeocoderPresenter(new ReactiveLocationProvider(getActivity()), geocoderRepository);
        geocoderPresenter.setUI((this));
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
        return view;

    }

    private synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this.getContext()).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //latLngPass= (LatLngPass) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        geocoderPresenter.setUI((this));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
        geocoderPresenter.stop();
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (googleApiClient != null) {
            googleApiClient.unregisterConnectionCallbacks(this);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (currentLocation == null) {
            geocoderPresenter.getLastKnownLocation();
        }
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(DISPLAYMENT);

        if (ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {

            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            Toast.makeText(getActivity(), "Cannot Get Current Location", Toast.LENGTH_LONG).show();
        } else {
            LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
            currentLocation = location;
            setNewPosition(ll);
            updateGPSCoordinates();
//            latLngPass.onDatapass(ll);
        }

    }

    private void updateGPSCoordinates() {

        if (currentLocation == null) {
            return;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());


        // Prepare the HTTP request
        VehicleLocation vehicleLocation = new VehicleLocation(1, "60", "200", String.valueOf(currentLocation.getLatitude()), String.valueOf(currentLocation.getLongitude()), "" + dateFormat.format(new Date()));


        // Asynchronously execute HTTP request

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.style_json)));
        if (map == null) {
            map = googleMap;
            buildGoogleApiClient();
            setDefaultMapSettings();
            setCurrentPositionLocation();
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        setNewPosition(latLng);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        setNewPosition(latLng);
    }

    private void setNewPosition(LatLng latLng) {
        if (currentLocation == null) {
            currentLocation = new Location(getString(R.string.network_resource));
        }
        currentLocation.setLatitude(latLng.latitude);
        currentLocation.setLongitude(latLng.longitude);
        setCurrentPositionLocation();
    }


    private void setNewMapMarker(LatLng latLng) {
        if (map != null) {
            if (currentMarker != null) {
                currentMarker.remove();
            }
            CameraPosition cameraPosition =
                    new CameraPosition.Builder().target(latLng)
                            .zoom(getDefaultZoom())
                            .build();
            hasWiderZoom = false;
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            currentMarker = addMarker(latLng);
            map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {
                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    if (currentLocation == null) {
                        currentLocation = new Location(getString(R.string.network_resource));
                    }
                    currentLocation.setLongitude(marker.getPosition().longitude);
                    currentLocation.setLatitude(marker.getPosition().latitude);
                    setCurrentPositionLocation();
                }
            });
        }
    }

    private int getDefaultZoom() {
        int zoom;
        if (hasWiderZoom) {
            zoom = WIDER_ZOOM;
        } else {
            zoom = DEFAULT_ZOOM;
        }
        return zoom;
    }


    private void setDefaultMapSettings() {
        if (map != null) {
            map.setMapType(MAP_TYPE_NORMAL);
            map.setOnMapLongClickListener(this);
            map.setOnMapClickListener(this);
            map.getUiSettings().setCompassEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.getUiSettings().setMapToolbarEnabled(false);
        }
    }

    private void setUpDefaultMapLocation() {
        if (currentLocation != null) {
            setCurrentPositionLocation();
        } else {

        }
    }

    private void setCurrentPositionLocation() {
        if (currentLocation != null) {
            setNewMapMarker(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
            geocoderPresenter.getInfoFromLocation(new LatLng(currentLocation.getLatitude(),
                    currentLocation.getLongitude()));
        }
    }


    private Marker addMarker(LatLng latLng) {
        return map.addMarker(new MarkerOptions().position(latLng).draggable(true));
    }

    private Marker addPoiMarker(LatLng latLng, String title, String address) {
        return map.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .title(title)
                .snippet(address));
    }


    @Override
    public void willLoadLocation() {

    }

    @Override
    public void showLocations(List<Address> addresses) {

    }

    @Override
    public void showDebouncedLocations(List<Address> addresses) {

    }

    @Override
    public void didLoadLocation() {

    }

    @Override
    public void showLoadLocationError() {

    }

    @Override
    public void showLastLocation(Location location) {
        currentLocation = location;
    }

    @Override
    public void didGetLastLocation() {
        setUpDefaultMapLocation();
    }

    @Override
    public void showLocationInfo(List<Address> addresses) {

    }

    @Override
    public void willGetLocationInfo(LatLng latLng) {

    }

    @Override
    public void didGetLocationInfo() {

    }

    @Override
    public void showGetLocationInfoError() {

    }


}
