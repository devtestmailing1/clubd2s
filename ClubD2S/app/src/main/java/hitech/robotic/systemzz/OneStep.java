package hitech.robotic.systemzz;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;

public class OneStep extends AppCompatActivity {

    @BindView(R.id.web_view)
    WebView wv1;
    TextView progressBarText;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        progressBarText = findViewById(R.id.progress_bar_text);

        WebView wv1 = findViewById(R.id.web_view);
        progressBarText = findViewById(R.id.progress_bar_text);
        progressBarText.setVisibility(View.VISIBLE);
        wv1.setVisibility(View.VISIBLE);
        wv1.loadUrl("http://www.clubd2s.com/");
        wv1.getSettings().setDomStorageEnabled(true);
        wv1.getSettings().setSaveFormData(true);
        wv1.getSettings().setAllowContentAccess(true);
        wv1.getSettings().setAllowFileAccess(true);
        wv1.getSettings().setAllowFileAccessFromFileURLs(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setWebViewClient(new WebViewClient());
        wv1.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                progressBarText.setVisibility(View.GONE);
                return true;

            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                progressBarText.setVisibility(View.VISIBLE);
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
