package hitech.robotic.systemzz;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import hitech.robotic.systemzz.utils.PreferenceHelper;

public class SpeedControler extends AppCompatActivity {

    private EditText editText;
    private Button button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        editText = findViewById(R.id.speedbox);
        button = findViewById(R.id.submit);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceHelper.setStringPreference(SpeedControler.this,"setspeed",editText.getText().toString());
            }
        });

    }
}
