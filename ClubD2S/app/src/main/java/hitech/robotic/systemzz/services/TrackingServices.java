package hitech.robotic.systemzz.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.Date;
import java.util.List;

import hitech.robotic.systemzz.HomeActivity;
import hitech.robotic.systemzz.IncomingCallReceiver;
import hitech.robotic.systemzz.R;

public class TrackingServices extends Service {

    final String CHANNEL_DEFAULT_IMPORTANCE = "1414";
    final int ONGOING_NOTIFICATION_ID = 1111;
    long timerStart = 4000;
    CountDownTimer countDownTimer;


    @Override
    public void onCreate() {
        super.onCreate();
      //  createNotification();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        backGroundTime();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PHONE_STATE");
        filter.addAction("android.intent.action.NEW_OUTGOING_CALL");
     //   detectBackground();
       // startForeground(ONGOING_NOTIFICATION_ID, notification);
      //  registerReceiver(new detectCalls(),filter);
        return START_STICKY;
    }

    public void createNotification(){
        Intent notificationIntent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Notification.Builder builder = new Notification.Builder(this, CHANNEL_DEFAULT_IMPORTANCE)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("RUNNING")
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            Notification notification1 = builder.build();
            startForeground(1, notification1);

        } else {

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("running")
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true);

            Notification notification2 = builder.build();

            startForeground(1, notification2);
        }

      //  backGroundTime();
    }

    private void backGroundTime(){
         countDownTimer = new CountDownTimer(timerStart,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerStart = timerStart+1000;
                Log.e("backservice>>","running");
                detectBackground();
            }

            @Override
            public void onFinish() {
                Log.e("backservice>>","restarting");
                countDownTimer.start();

            }
        }.start();
    }



    public class detectCalls extends IncomingCallReceiver {

        @Override
        protected void onIncomingCallReceived(Context ctx, String number, Date start) {
            Log.e("uploading12>>", number);
        }

        @Override
        protected void onIncomingCallAnswered(Context ctx, String number, Date start) {
            Log.e("uploading123>>", number);
        }

        @Override
        protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
            Log.e("uploading1234>>", number);
        }

        @Override
        protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
            Log.e("uploading12345>>", number);
        }

        @Override
        protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
            Log.e("uploading123456>>", number);
        }

        @Override
        protected void onMissedCall(Context ctx, String number, Date start) {
            Log.e("uploading>>", number);
        }
    }

    private void detectBackground(){
        ActivityManager manager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfo = manager.getRunningAppProcesses();

        for (int i = 0; i < runningAppProcessInfo.size(); i++) {
            Log.i("foreground1>>", runningAppProcessInfo.get(i).processName + " > "+ runningAppProcessInfo.size());
            if(runningAppProcessInfo.get(i).importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND){
                Log.i("foreground>>", runningAppProcessInfo.get(i).processName);
            }
        }


    }



}
