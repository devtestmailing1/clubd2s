package hitech.robotic.systemzz.services;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class AlarmManagerBroadcastReceiver extends BroadcastReceiver {

    final public static String ONE_TIME = "onetime";
    Context mContext;

    // private ConnectivityReceiver mConnectivityReceiver;
    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onReceive(Context context, Intent intent) {

        // mConnectivityReceiver = new ConnectivityReceiver(this);

        Log.e("uploading > ", "on Recieve of broadcast");
        this.mContext = context;

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = null;
        if (pm != null) {
            wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YOUR TAG");
            //Acquire the lock
            wl.acquire();
        }


//        if (wl != null) {
//            wl.acquire(10*60*1000L /*10 minutes*/);
//        }

        //You can do the processing here update the widget/remote views.
        Bundle extras = intent.getExtras();
        StringBuilder msgStr = new StringBuilder();

        if (extras != null && extras.getBoolean(ONE_TIME, Boolean.FALSE)) {
            msgStr.append("One time Timer : ");
        }
        Format formatter = new SimpleDateFormat("hh:mm:ss a");
        msgStr.append(formatter.format(new Date()));

     //   Toast.makeText(context, msgStr, Toast.LENGTH_LONG).show();
        Log.e("uploading > ",  "Time > "+ msgStr.toString());
        Log.e("uploading > ", "now sending to service class");

        if(!isMyServiceRunning(UploadVideoService.class)) {
            Intent i = new Intent(context, UploadVideoService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(i);
                Log.e("uploading > ", "started foreground service");
                //        context.startService(i);
            } else {
                context.startService(i);

                Log.e("uploading > ", "start service");

            }
        }
        else{
            Log.e("uploading >","Service Already Running");
            Intent myIntent = new Intent(context, UploadVideoService.class);
            context.stopService(myIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(myIntent);
                Log.e("uploading > ", "rrrstarted foreground service");
                //        context.startService(i);
            } else {
                context.startService(myIntent);

                Log.e("uploading > ", "rrrstart service");

            }
        }
        //Release the lock
        wl.release();

    }

    public void SetAlarm(Context context) {

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.FALSE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //After after 30 seconds
        Objects.requireNonNull(am).setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 30*1000 * 60, pi);

        Log.e("uploading", "Repeating Alarm is broadcasted");

    }

    public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Objects.requireNonNull(alarmManager).cancel(sender);
        Log.e("Broadcaste", "Cancel Alarm is broadcasted");
    }

    public void setOnetimeTimer(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        Objects.requireNonNull(am).set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pi);
        Log.e("Broadcaste", "one time Alarm is broadcasted");

    }




    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager)  this.mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}