package hitech.robotic.systemzz;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class Share extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "One Crore Steps");
        String message = "\nLet me recommend you this application *https://play.google.com/store/apps/details?id=com.horusisys.onecroresteps* \n\n";

        i.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(i, "choose one"));
    }
}
