package hitech.robotic.systemzz;

public class QueryResponseResult {
    public String Status;
    public String password;
    public String user_id;
    public String otp;
    public String mobile;

    public String getStatus() {
        return Status;
    }
    public String getPassword() {
        return password;
    }
    public String getUserId() {
        return user_id;
    }
    public String getOtp() { return otp; }
    public String getMobile() { return mobile; }

}
