package hitech.robotic.systemzz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class Me extends AppCompatActivity {

    EditText name;
    EditText emailid;
    EditText phnum;
    String nm, em, ph;
    Button btnChange;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        //read from reg table and fill in details
        name = findViewById(R.id.edit_user_name);
        emailid = findViewById(R.id.edit_email_id);
        phnum = findViewById(R.id.edit_mobile_no);
        name.setFocusable(false);
        name.setClickable(false);
        emailid.setFocusable(false);
        emailid.setClickable(false);
        phnum.setFocusable(false);
        phnum.setClickable(false);
        RegInfo reg = DBHandlerWalk.Instance(getApplicationContext()).findRegHandler();
        if(reg != null) {
            name.setText(reg.getName());
            emailid.setText(reg.getEmailid());
            phnum.setText(reg.getPhonenum());
        }
        else
        {
            //could not find reg details!
        }
        ButterKnife.bind(this);
    }
    @OnClick(R.id.change)
    public void clickChange(){

        if(!validate())
        {
            Toast.makeText(this,"Update has failed!, Enter valid details", Toast.LENGTH_SHORT).show();
        } else
        {
            onChangeRequest();
        }

    }

    public void initialize()
    {
        nm = name.getText().toString();
        em = emailid.getText().toString().trim();
        ph = phnum.getText().toString().trim();
    }


    public boolean validate(){
        boolean valid=true;
        initialize();
        if(nm.isEmpty()||nm.length()>20)
        {
            name.setError("Please enter valid Name");
            valid=false;
        }


        if(ph.isEmpty()|| ph.length()>10 || ph.length()<10)
        {
            phnum.setError("Please enter Valid Phone Number");
            valid=false;
        }

        if(em.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(em).matches())
        {
            emailid.setError("Please enter valid Email");
            valid=false;
        }

        return valid;
    }
    public void onChangeRequest(){
        //do uploading here
        RegInfo reg = new RegInfo( nm, em, ph);
        DBHandlerWalk.Instance(getApplicationContext()).updateHandler(reg);
        Toast.makeText(getApplicationContext(),"User details updated",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Me.this,MainActivity.class);
        startActivity(intent);
    }

}
