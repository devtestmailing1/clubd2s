package hitech.robotic.systemzz;

import android.Manifest;
import android.app.AppOpsManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import hitech.robotic.systemzz.OBD.TripDatabaseHandler;
import hitech.robotic.systemzz.utils.PreferenceHelper;
import io.grpc.Server;


/**
 * This service class handles all the process in background, When user runs the trip in background mode.
 */
public class SensorService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String CHANNEL_ID = "D4D4D4D4D";
    public static final String SLOW_SPEED_CHANNEL_ID = "D5D5D5D5D";
    public static final String START_TRIP_CHANNEL_ID = "D5D5D5D6D";
    public static final String PRE_TRIP_CHANNEL_ID = "D5D5D5D7D";

    private SensorEventListener accelerometerListener, eventListener;
    ArrayList<Float> z_accel = new ArrayList<Float>();
    private int accel_window_size = 15, hardBreakCounter = 0, overSpeedCounter = 0;
    double accelX, accelY, accelZ, linearAccel, mLastX, mLastY, mLastZ, Speed = 0, speedHolder = 0, latitude[], longitude[], lat, longi;
    private float[] linear_acceleration = new float[3];
    public boolean mInitialized, isTimerRunning = false, isOverSpeedTimerRunning = false, overSpeedHolderFlag = false,
            isGyroTimerRunning, isLowSpeerTimerRunning=false, isMainTimerRunning = false, slowSpeedNotification = false;
    private final float NOISE = (float) 2.0;
    private CopyOnWriteArrayList<Double> avgSpeedArray = new CopyOnWriteArrayList<>();
    private String brakeEvent = " ",sPeed_units = " ", OverSpeedLimit,Beep, gryoEvent, startingTime,phoneUsages;
    private SensorManager mSensorManager, sensorManager, SensorManager;
    private Sensor mAccelerometer;
    GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 500;
    LocationCallback locationCallback;
    ArrayList<Location> locations = new ArrayList<>();
    private FusedLocationProviderClient mFusedLocationClient, mFLocationClient;
    public CountDownTimer overSpeedTimer,countDownTimer, mainTimer,lowSpeedTimer;
    ToneGenerator toneGen1;
    private ArrayList<Float> avgSpeedPointsList = new ArrayList<>();
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Long times[];
    private Location mCurrentLocation;
    private LocationSettingsRequest mLocationSettingsRequest;
    private float timestamp;
    private Sensor sensor;
    private static final float NS2S = 1.0f / 1000000000.0f;
    CopyOnWriteArrayList<Double> oMagnitude = new CopyOnWriteArrayList<>();
    private final float[] deltaRotationVector = new float[4];
    private int gyroCounter = 0, tripStartFlag= 0;
    private Date tripEndDate, tripStartDate;
    private long startTimeInMills,totalPhoneUsages = 0;
    private double avgSpeed = 0;
    private int gCounter = 0;
    private FirebaseFirestore firebaseFirestore;
    private int checkFlat = 23, pushFlag = 0;
    private ServiceCallback serviceCallback;
    private int autoStartFlag = 0;

    IBinder iBinder = new LocalBinder();
    public NotificationManager globalNotificationManager;


    @Override
    public void onCreate() {
        super.onCreate();
    }


    /**
     * This method call first when user starts the service.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String input = intent.getStringExtra("inputExtra");
        firebaseFirestore = FirebaseFirestore.getInstance();
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Safe Drive Club")
                .setContentText(input)
                .setSmallIcon(R.drawable.logo_new)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        //do heavy work on a background thread
        Log.e("startss","sss" + checkFlat);
        tripStartFlag = 0;
        isLowSpeerTimerRunning = false;
        pushFlag = 0;
        //timer();

        //stopSelf();

      //  enableGPS();

        startSensorProcessing();

       // new Experient().timer();

        return START_STICKY;
    }

    /**
     * This method return the binder which used for bind the service with the activity.
     * @param intent
     * @return
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }


    private void timer(){

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                countDownTimer = new CountDownTimer(1000000000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        Log.e("timer","timer");

                    }

                    @Override
                    public void onFinish() {

                    }
                }.start();
            }
        });
    }


    /**
     * We register sensors in this method.
     */
    private void startSensorProcessing(){


        /*SensorManager.registerListener((SensorEventListener) SensorService.this, SensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);*/
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorManager.registerListener(accelerometerListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(accelerometerListener, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);

        toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 50000);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        OverSpeedLimit = String.valueOf(sharedPreferences.getInt("SEEKBAR_VALUE", 60));
        Beep = sharedPreferences.getString("BEEP_UPLOAD", "");
        String speed_units = sharedPreferences.getString(getString(R.string.speed_Units), "0");

        if (speed_units.equals("0")) {
            sPeed_units = "km/hr";
        }
        if (speed_units.equals("1")) {
            sPeed_units = "mph";

        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm",Locale.US);
        startingTime = timeFormat.format(new Date().getTime());

        startTimeInMills = System.currentTimeMillis();
        tripStartDate = new Date();

        getSpeed();

        showNotification("simpleend","Safe Drive Club", "Your trip will start once you cross speed of 10 Km/h",false);

        usesStats();

    }


    /**
     * Here we get reading from accelorameter and based on those reading we analyse hard brake event.
     */
    private void startAccelorameter(){
        accelerometerListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {

                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];
                float z = sensorEvent.values[2];

                if (z_accel.size() < accel_window_size) {
                    z_accel.add(z);
                } else {
                    z_accel.remove(0);
                    z_accel.add(z);
                }

                accelX = x;
                accelY = y;
                accelZ = z;
                final float alpha = 0.8f;

                linear_acceleration = sensorEvent.values;
                linearAccel = Math.sqrt((linear_acceleration[0] * linear_acceleration[0]) +
                        (linear_acceleration[1] * linear_acceleration[1]) +
                        (linear_acceleration[2] * linear_acceleration[2]));


                if (!mInitialized) {
                    mLastX = x;
                    mLastY = y;
                    mLastZ = z;
                    mInitialized = true;
                } else {
                    double deltaX = (mLastX - x);
                    double deltaY = (mLastY - y);
                    double deltaZ = (mLastZ - z);

                    if (deltaX < NOISE) deltaX = (float) 0.0;
                    if (deltaY < NOISE) deltaY = (float) 0.0;
                    if (deltaZ < NOISE) deltaZ = (float) 0.0;

                    if (z > 3) {
                        //Toast.makeText(getApplicationContext(),"Moving back: " + z, Toast.LENGTH_SHORT).show();
                        Log.d("Accel: ", "+ve accel: " + z_accel.get(0));
                    }
                    if (z < -3) {
                        Log.d("Accel: ", "-ve accel: " + z_accel.get(0));
                        //Toast.makeText(getApplicationContext(),"Moving forward: " + z, Toast.LENGTH_SHORT).show();
                    }

                    mLastX = x;
                    mLastY = y;
                    mLastZ = z;

                    double sum = 0;
                    float sumZ = 0;
                    for (Double d : avgSpeedArray)
                        sum += d;

                    for (Float k : z_accel)
                        sumZ += k;

                    Double avgSpeed = sum / 10;
                    float avgZ = sumZ / accel_window_size;


                    // if (linearAccel > 5 && avgSpeed > 20) { //yahan bi 20 karna hai

                    if (avgZ < -3) {
                        Log.d("Hard break hit >>>", "hard break hit");
                        brakeEvent = "HARD BRAKE";
                        Log.d("xyzm ", " x > " + x + " y > " + y + " z > " + z + " > " + linearAccel + " > " + avgZ);
                        breakTimer();

                    }
                    //  }
                }

            }


            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    }


    /**
     * This timer runs for 3 seconds after hard brake event.
     */
    private void breakTimer() {
        if (!isTimerRunning) {

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    new CountDownTimer(3000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            isTimerRunning = true;
                        }

                        public void onFinish() {
                            isTimerRunning = false;
                            saveEventInDatabase(brakeEvent);
                            brakeEvent = " ";
                            hardBreakCounter++;
                        }
                    }.start();

                }
            });
        }
    }


    /**
     * This method make entries of events in database.
     * @param eventName
     */
    private void saveEventInDatabase(String eventName) {

        Log.d("background", "background");

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
        String timeStamp = ((Long) (System.currentTimeMillis() / 1000)).toString();
        String date = dateFormat.format(new Date());
        String time = timeFormat.format(Calendar.getInstance().getTime());

        if (!PreferenceHelper.getPreferences(this).contains("tripId")) {
            PreferenceHelper.setStringPreference(this, "tripId", timeStamp);
        }

        String tripId = PreferenceHelper.getStringPreference(this, "tripId");

        TripDatabaseHandler databaseHandler = new TripDatabaseHandler(this);
        databaseHandler.open();
        databaseHandler.insertTripData(timeStamp, date, time, eventName, tripId);
        databaseHandler.getTripData();

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    /**
     * This method check for availability of play services in users's phone.
     * @return
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
              //  apiAvailability.getErrorDialog(, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
               //  finish();
            }

            return false;
        }

        return true;
    }


    /**
     * This method continues giving current location of user.
     */
    private void calculateTotalTravelDistance() {

        LocationRequest mLocationRequest;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        // mLocationRequest.setSmallestDisplacement(2);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                if (locationResult.getLastLocation() != null) {
                    Log.d("lastlocations1>>>>>", locationResult.getLastLocation().getLatitude() + " >> " +
                            locationResult.getLastLocation().getLongitude());

                    locationCollector(locationResult.getLastLocation());

                    Log.d("lastlocation5>>>>>>", locationResult.getLastLocation().getBearing() + " >>> " +
                            locationResult.getLastLocation().bearingTo(locations.get(0)));

                }
            }


            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
                Log.d("lastlocation4>>", locationAvailability.isLocationAvailable() + "");
            }
        };

        mFLocationClient = LocationServices.getFusedLocationProviderClient(SensorService.this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mFLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    Log.d("lastlocation2 >>>> ", location.getLatitude() + " " + location.getLongitude());
                    locationCollector(location);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("lastloaction3>", e.getMessage());
            }
        });

        mFLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper());

    }


    /**
     * Here we are adding location in the list.
     * @param location
     */
    private void locationCollector(Location location) {

        if (locations.size()==0){
            locations.add(location);
        }else {
            if (getDistanceBetweenTwoLocations(locations.get(locations.size()-1).getLatitude(),
                    locations.get(locations.size()-1).getLongitude(),location.getLatitude(),location.getLongitude()) > 0.2){
                locations.add(location);
            }
        }
        Log.e("uploadinglll>", locations.size() + " >>> " + locations.toString());
    }


    /**
     * This method return the distance between two locations.
     * @param startLat
     * @param startLong
     * @param endLat
     * @param endLong
     * @return
     */
    private double getDistanceBetweenTwoLocations(double startLat, double startLong, double endLat, double endLong) {
        float[] result = new float[10];
        Location.distanceBetween(startLat, startLong, endLat, endLong, result);
        double distatnce = ((double) result[0]) / 1000;
        // Log.d("distancedou>", (double)result[0] + " > " + distatnce+" > "+ round(distatnce,2) + " > " + BigDecimal.valueOf(distatnce).setScale(0,RoundingMode.HALF_UP));
        //  return (BigDecimal.valueOf(distatnce).setScale(0,RoundingMode.HALF_UP)).doubleValue();
        return distatnce;
    }


    /**
     * This method keep observing speed and fire over speed timer if over speed event come.
     */
    private void continueRunner(){

        if (sPeed_units.equals("mph")) {

            if (((int) (Speed * 0.621371)) > Integer.valueOf(OverSpeedLimit) * 0.621371) {
                overSpeedTimer();
            } else {
                overSpeedHolderFlag = false;
            }

        } else {

            if (((int) (Speed)) > Integer.valueOf(OverSpeedLimit)) {
                //  if (((int) (Speed)) > -1) {
                overSpeedTimer();
            } else {
                overSpeedHolderFlag = false;
            }
        }

        calculateSpeedPoints();

    }


    /**
     * This method run when over speed event come and start timer for 3 seconds.
     */
    private void overSpeedTimer() {

        if (!isOverSpeedTimerRunning) {

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    overSpeedTimer = new CountDownTimer(3000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            // Log.d("Timer Start >>>", "timer start");
                            isOverSpeedTimerRunning = true;
                        }

                        public void onFinish() {
                            //  Log.d("Timer Finish >>>", "timer finsh");

                            isOverSpeedTimerRunning = false;
                            brakeEvent = " ";

                            if (!overSpeedHolderFlag) {

                                if (Beep.equals("ON")) {
                                    toneGen1.startTone(AudioManager.STREAM_MUSIC, 800);
                                }

                                saveEventInDatabase(brakeEvent);
                                overSpeedCounter++;
                                overSpeedHolderFlag = true;
                            }

                        }
                    }.start();
                }
            });
        }
    }


    /**
     * This method add speed points in arraylist.
     */
    private void calculateSpeedPoints() {

        if (speedHolder != Speed) {
            avgSpeedPointsList.add((float) Speed);
            speedHolder = Speed;
        }
    }

    /**
     * This method give the current speed and also send different notifications to user based on current which
     * tells the user current state of trip. This method executes one time in one second and give the current location
     * of user.
     */
    private void getSpeed(){

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                Integer counter = 0;
                latitude = new double[2];
                longitude = new double[2];
                times = new Long[2];

                super.onLocationResult(locationResult);
                // location is received
                Double d1;
                Long t1;
                // Double speed = 0.0;
                d1 = 0.0;
                t1 = 0l;
                mCurrentLocation = locationResult.getLastLocation();
                lat = mCurrentLocation.getLatitude();
                longi = mCurrentLocation.getLongitude();
                latitude[counter] = mCurrentLocation.getLatitude();
                longitude[counter] = mCurrentLocation.getLongitude();
                times[counter] = mCurrentLocation.getTime();
                try {

                    d1 = distance(latitude[counter], longitude[counter], latitude[(counter + 1) % 2], longitude[(counter + 1) % 2]);
                    t1 = times[counter] - times[(counter + 1) % 2];
                } catch (NullPointerException e) {

                }
                if (mCurrentLocation.hasSpeed()) {
                    Speed = mCurrentLocation.getSpeed() * 1.0;
                } else {
                    if (t1 == 0) Speed = 0.0;
                    else Speed = d1 / t1;
                }

                counter = (counter + 1) % 2;
                Speed = Speed * 3.6d;

                Log.e("speeding>",Speed + " <speed");

                calculateAvgSpeed();

                if (avgSpeedArray!=null && avgSpeedArray.size()>0) {
                    double sum = 0;
                    for (Double d : avgSpeedArray) {
                        sum += d;
                    }
                    avgSpeed = sum / avgSpeedArray.size();
                }

               // tripStartFlag = 1;

              //  avgSpeed = 9;

               // Log.e("avgspeed555>>>>", avgSpeed+"  >>>> "+avgSpeedArray.toString() );

                /*if (gCounter<20 && tripStartFlag == 0){
                gCounter++;}else {
                    if (gCounter>0)
                    gCounter --;
                }*/

                Log.e("gcounter> ",gCounter+"");

                Log.e("flaggg>>>",tripStartFlag+"");

               // Speed = 11;

                if (!PreferenceHelper.getStringPreference(SensorService.this,"setspeed").isEmpty()) {
                    Speed = Double.valueOf(PreferenceHelper.getStringPreference(SensorService.this, "setspeed"));
                }

                if (Speed>10 && tripStartFlag==0){
                    showNotification("lowspeed","Safe Drive Club", "Your Trip Started",false);
                      // saveLocation("tripStart",lat,longi,Speed+"", new Date().toString());
                      // Log.e("avgspeed>>>>", gCounter+"  >>>> "+avgSpeedArray.toString() );
                    tripStartFlag = 1;
                    startTripProcessing();
                }

              //  Speed = 8;


                if (Speed < 9 && isLowSpeerTimerRunning == false && tripStartFlag==1){
                    lowSpeedTimer();
                  //  saveLocation("lowspeed",lat,longi,Speed+"", new Date().toString());
                }
                    if (isLowSpeerTimerRunning && Speed>10 ){
                        if (autoStartFlag == 0) {
                            showNotification("lowspeed", "Safe Drive Club", "Your Trip Started", false);
                        autoStartFlag = 1;
                        }
                        isLowSpeerTimerRunning = false;
                        slowSpeedNotification = false;
                        lowSpeedTimer.cancel();
                    }

                    checkFlat = 88;

                    /**************************************/

                    Toast.makeText(SensorService.this,"Speed> "+ Speed+"",Toast.LENGTH_SHORT).show();

                // saveLocation("constSpeedd",lat,longi,Speed+"" , new Date().toString());

                    /***************************************/

              //  calculateAvgSpeed();
            }
        };

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());

    }


    private double deg2rad(double deg) {

        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {

        return (rad * 180.0 / Math.PI);
    }

    /**
     * Calculating distance between two locations.
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    private double distance(double lat1, double lon1, double lat2, double lon2) {
        // haversine great circle distance approximation, returns meters
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60; // 60 nautical miles per degree of seperation
        dist = dist * 1852; // 1852 meters per nautical mile
        return (dist);
    }

    /**
     * Calculating average speed of vehicle base on last 10 speed points.
     */
    private void calculateAvgSpeed() {
        if (avgSpeedArray.size() < 10) {
            avgSpeedArray.add(Speed);
        } else {
            avgSpeedArray.remove(0);
            avgSpeedArray.add(Speed);
        }

      /* avgSpeedArray.clear();
      // avgSpeedArray.add(6d);
       avgSpeedArray.add(9d);
       avgSpeedArray.add(8d);
      // avgSpeedArray.add(6d);*/

    }


    /**
     * Getting data from Gyro
     */
    private void checkGyroscope(){

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        eventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                // timestamp = event.timestamp;
                //  Log.d("timeStamp>>>",timestamp+"");

                if (timestamp != 0) {
                    final float dT = (event.timestamp - timestamp) * NS2S;
                    // Axis of the rotation sample, not normalized yet.
                    float axisX = event.values[0];
                    float axisY = event.values[1];
                    float axisZ = event.values[2];

                    // Calculate the angular speed of the sample
                    double omegaMagnitude = Math.sqrt(axisX * axisX + axisY * axisY + axisZ * axisZ);

                    if (oMagnitude.size()<10){
                        oMagnitude.add(omegaMagnitude);
                    }else {
                        oMagnitude.remove(0);
                        oMagnitude.add(omegaMagnitude);
                    }

                    // Normalize the rotation vector if it's big enough to get the axis
                    // (that is, EPSILON should represent your maximum allowable margin of error)
                    if (omegaMagnitude > 0.000000001f) {
                        axisX /= omegaMagnitude;
                        axisY /= omegaMagnitude;
                        axisZ /= omegaMagnitude;
                    }

                    checkGyro();


                    double thetaOverTwo = omegaMagnitude * dT / 2.0f;
                    float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
                    float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
                    deltaRotationVector[0] = sinThetaOverTwo * axisX;
                    deltaRotationVector[1] = sinThetaOverTwo * axisY;
                    deltaRotationVector[2] = sinThetaOverTwo * axisZ;
                    deltaRotationVector[3] = cosThetaOverTwo;

                    //  Log.d("rotationvector> ", deltaRotationVector[0]+ " > "+deltaRotationVector[1] + " > "+
                    //   deltaRotationVector[2]+" > "+deltaRotationVector[3]);

                    //  Log.d("rotationvector>1 ",axisX+" > "+axisY+" > "+axisZ + " > "+ omegaMagnitude);

                    /*writeDataToCSV(axisX+"",axisY+"",axisZ+"",omegaMagnitude+"",
                            deltaRotationVector[0]+"",deltaRotationVector[1]+"",
                            deltaRotationVector[2]+"",deltaRotationVector[3]+"");*/

                }

                timestamp = event.timestamp;
                float[] deltaRotationMatrix = new float[9];
                SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);


            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        sensorManager.registerListener(eventListener,sensor,SensorManager.SENSOR_DELAY_NORMAL);

    }

    /**
     * Calculating sharp turn on the basis of gyro readings.
     */
    private void checkGyro(){

       /* Random r = new Random();
        double randomValue = 10 + (40 - 10) * r.nextDouble();*/

        //  avgSpeedArray.add(randomValue);

        double avgSpeed = 0,avgGyro = 0;

        if (avgSpeedArray!=null && avgSpeedArray.size()>0) {
            double sum = 0;
            for (Double d : avgSpeedArray)
                sum += d;
            avgSpeed = sum / 10;
        }


        if (oMagnitude.size()>0) {
            double gyroValue = 0;
            for (Double dd : oMagnitude)
                gyroValue += dd;
            avgGyro = gyroValue / 10;
        }

        //  Log.d("gyro>>> ", avgSpeed + " >> " + avgGyro);


        if (avgSpeed > 40 && avgGyro>1.3){
            gryoEvent = "Sharp Turn";
            gyroTimer();
        }

    }

    /**
     * This timer runs for 3 seconds when sharp turn event occurs.
     */

    private void gyroTimer() {
        if (!isGyroTimerRunning) {

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    countDownTimer = new CountDownTimer(3000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            isGyroTimerRunning = true;
                        }

                        public void onFinish() {
                            isGyroTimerRunning = false;
                            // saveEventInDatabase(brakeEvent);
                            gyroCounter++;
                            gryoEvent = " ";
                        }
                    }.start();

                }
            });
        }
    }


    /**
     * We call this method when we need to end the trip.
     */
    public void endTrip(){

        if (mFusedLocationClient!=null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
        if (mFLocationClient!=null) {
            mFLocationClient.removeLocationUpdates(locationCallback);
        }

        if (isLowSpeerTimerRunning) {
            lowSpeedTimer.cancel();
        }
        if (isMainTimerRunning){
            mainTimer.cancel();
        }

        PreferenceHelper.cleanPerticualrPref(SensorService.this, "tripId");
       // onstopFlag = true;
       // onDestroyFlag = true;
       // onBackPressed();

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm",Locale.US);
        String endTime = timeFormat.format(Calendar.getInstance().getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy",Locale.US);
        String date = dateFormat.format(new Date());

        tripEndDate = new Date();


      //  Crashlytics.log(Log.ERROR,"fulldate",tripStartDate.getTime() + " >>> " +tripEndDate.getTime()+"");

        Date startDate = new Date(PreferenceHelper.getLongPreference(SensorService.this,"sDate"));

        Crashlytics.log(Log.ERROR, "date",startDate + " >> " + tripEndDate +" >> "+
                startDate.getTime() +" >> "+tripEndDate.getTime());

        long totalTripDuration = tripEndDate.getTime() - startDate.getTime();



        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(1);

        double distance = 0;

        if (locations.size() > 2) {

            for (int i = 0; i < locations.size() - 1; i++) {
                distance = distance + getDistanceBetweenTwoLocations(locations.get(i).getLatitude(), locations.get(i).getLongitude(), locations.get(i + 1).getLatitude(), locations.get(i + 1).getLongitude());
            }
        }

        double avgSpeed = distance * 3600000 / totalTripDuration;
        distance = distance+5;

       // Intent intent = new Intent(SensorService.this, TripReport.class);
        Intent intent = new Intent();
        intent.putExtra("startTime", startingTime + "-"+endTime);
        intent.putExtra("distance", df.format(distance) + "" + " Km");
        intent.putExtra("time", convertmiliSecondsToHMmSs(totalTripDuration));
        intent.putExtra("speed", df.format(avgSpeed) + "" + " Km/h");
        intent.putExtra("hardbrake", hardBreakCounter + "");
        intent.putExtra("overSpeed", overSpeedCounter + "");
        intent.putExtra("endTime", endTime);
        intent.putExtra("currentDate", date);
        intent.putExtra("speedPoints", avgSpeedPointsList);
        intent.putExtra("startTimeMills", startTimeInMills);
        intent.putExtra("sharpTurn", gyroCounter + " ");
        double ddd = TimeUnit.MILLISECONDS.toHours(totalTripDuration);
        intent.putExtra("durationInHours", ddd + "");

        double durr = ((totalTripDuration / (1000 * 60 * 60)) % 24);

        Log.d("duration>", durr + ">>> " + totalTripDuration + " >>> " + ddd);

        PreferenceHelper.setStringPreference(SensorService.this, "reportway", "camera");

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);



      //  startActivity(intent);



        /////////*************/////////

        if (checkPackageUsagesPermission()){
            phoneUsages = usesStats();
        }else {
            phoneUsages = "No Permission";
        }

        String timeStamp = ((Long) (System.currentTimeMillis() / 1000)).toString();
        if (!PreferenceHelper.getPreferences(this).contains("tripId")) {
            PreferenceHelper.setStringPreference(this, "tripId", timeStamp);
        }

        String tripId = PreferenceHelper.getStringPreference(this, "tripId");

        String distance1 = intent.getExtras().getString("distance").substring(0, intent.getExtras().getString("distance").indexOf(" "));

        Log.e("speeding>>","distance > "+distance1);
       // distance1 = "1.0";

       // if (Double.valueOf(distance1)>0.5) {
            addDataReportWithFirestore(intent.getExtras().getString("startTime"),
                    intent.getExtras().getString("distance").substring(0,intent.getExtras().getString("distance").indexOf(" ")),
                    intent.getExtras().getString("durationInHours"),
                    intent.getExtras().getString("speed").substring(0, intent.getExtras().getString("speed").indexOf(" ")),
                    intent.getExtras().getString("hardbrake"),
                    intent.getExtras().getString("overSpeed"),
                    intent.getExtras().getString("endTime"),
                    String.valueOf(TimeUnit.MILLISECONDS.toHours(totalPhoneUsages)),
                    intent.getExtras().getString("sharpTurn").substring(0,intent.getExtras().getString("sharpTurn").indexOf(" ")),
                    intent.getExtras().getString("currentDate"),
                    tripId,  intent.getExtras().getString("time"),
                    phoneUsages);
      //  }

        /************************///



        Toast.makeText(SensorService.this,"Your trip is finished. For details check old trips.",Toast.LENGTH_LONG).show();

        showNotification("tripend","Drive Safe Club","Your trip is finished. For Details Check Old Trips.",false);




    }

    /**
     * We call this method to end the trip when trip is not already running.
     */
    public void simplyEndService(){
        if (isLowSpeerTimerRunning) {
            lowSpeedTimer.cancel();
        }
        if (isMainTimerRunning){
            mainTimer.cancel();
        }

        if (serviceCallback!=null){
            serviceCallback.stopService();
        }

        stopForeground(true);
        stopSelf();
    }

    public String convertmiliSecondsToHMmSs(long millis) {
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
        return hms;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
      /*  stopThread = true;
        mStorageFlag = false;

        if (onDestroyFlag) {
            PreferenceHelper.cleanPerticualrPref(this, "tripId");
        }*/

      if (mFusedLocationClient!=null) {
          mFusedLocationClient.removeLocationUpdates(mLocationCallback);
      }
      if (mFLocationClient!=null) {
          mFLocationClient.removeLocationUpdates(locationCallback);
      }
    }

    /**
     * This timer keep the process running on same speed as camera frame speed.
     */
    private void startMainTimer(){
        mainTimer = new CountDownTimer(1000,25) {
            @Override
            public void onTick(long millisUntilFinished) {
                continueRunner();
               // Log.e("maintimer>","timer>"+millisUntilFinished);
                isMainTimerRunning = true;
               if (millisUntilFinished<100) {
                 //  Log.e("mainttimer>>","startagain");
                   isMainTimerRunning = false;
                   start();
               }
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }


    /**
     * Call this method to bind the service with activity.
     */
    public class LocalBinder extends Binder {
        public SensorService getServerInstance() {
            return SensorService.this;
        }
    }


    /**
     * This low speed timer is run for 20 second when speed goes lower then 10 Km per hour.
     */
    private void lowSpeedTimer(){
        lowSpeedTimer = new CountDownTimer(20000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                isLowSpeerTimerRunning = true;
                Log.e("speeding>>","lowspeedrunning "+isLowSpeerTimerRunning);
            }

            @Override
            public void onFinish() {

                if (!slowSpeedNotification){
                    autoStartFlag = 0;
                showNotification("lowspeed","Safe Drive Club", "If you are not moving. Then cancel your trip.",true);
                    isLowSpeerTimerRunning = false;
                    Log.e("speeding>>","fininshlowspeed "+isLowSpeerTimerRunning);
                }else {
                    tripStartFlag = 5;
                    endTrip();
                    Log.e("speeding>>","endingtrip");
                }
            }
        }.start();
    }

    /**
     * This method send notification to user according to different conditions.
     * @param extras
     * @param title
     * @param content
     * @param slowNotification
     */
    private void showNotification(String extras, String title, String content, Boolean slowNotification){

        Log.e("speeding>>","stop");
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(SLOW_SPEED_CHANNEL_ID
                    ,
                    "Foreground Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

          //  NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }


        Intent notificationIntent = new Intent(this, HomeActivity.class);
        notificationIntent.putExtra(extras,extras);
        notificationIntent.setAction(Long.toString(System.currentTimeMillis()));
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this, SLOW_SPEED_CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.drawable.logo_new)
              //  .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();

        manager.notify(666666666,notification);
        globalNotificationManager = manager;
        slowSpeedNotification = slowNotification;

    }

    /**
     * To dismiss the notification we call this method.
     */
    public void cancelNotification(){
        if (globalNotificationManager!=null) {
            globalNotificationManager.cancel(666666666);
        }
    }


    /**
     * this method register sensors and also call main timer method which run all the process in this service and run process
     * on same speed as camera frame.
     */
    private void startTripProcessing(){
        startAccelorameter();

        mSensorManager.registerListener(accelerometerListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(accelerometerListener, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);

        PackageManager packageManager = getPackageManager();
        boolean gyroExists = packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);

        if (gyroExists) {
            checkGyroscope();
        }

        if (!checkPlayServices()) {
            Toast.makeText(SensorService.this, "You need to install google play services to use app properly", Toast.LENGTH_SHORT).show();
        }

        calculateTotalTravelDistance();

        startMainTimer();
    }


        private void saveLocation(String dataType, Double lat, Double longi, String speed, String time){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM hh:mm:ss");

            Map<String,Object> report = new HashMap<>();
            report.put("speed",speed);
            report.put("lat", lat+"");
            report.put("lng",longi+"");
            report.put("time",time);

            FirebaseFirestore firebaseFirestore;
            firebaseFirestore = FirebaseFirestore.getInstance();
            firebaseFirestore.collection(dataType).document(simpleDateFormat.format(new Date()))
                    .set(report)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(SensorService.this, "Speed added", Toast.LENGTH_SHORT).show();
                        }
                    });


        }

            //faltu comment


    /**
     * This method add all the data to firestore.
     * @param StartTime
     * @param distance
     * @param duration
     * @param avgSpeed
     * @param hardbreak
     * @param overSpeed
     * @param endTime
     * @param phoneUsages
     * @param sharpTurn
     * @param tripDate
     * @param tripId
     * @param durationToShowOnly
     * @param phoneUsagesTime
     */
    private void addDataReportWithFirestore(String StartTime, String distance, String duration, String avgSpeed,
                                            String hardbreak, String overSpeed, String endTime,String phoneUsages, String sharpTurn,
                                            String tripDate, String tripId, String durationToShowOnly, String phoneUsagesTime) {
        Map<String, Object> report = new HashMap<>();
        report.put("startTime", StartTime);
        report.put("distance", distance);
        report.put("duration", durationToShowOnly);
        report.put("avgSpeed", avgSpeed);
        report.put("hardbreak", hardbreak);
        report.put("overSpeed", overSpeed);
        report.put("endTime", endTime);
        report.put("phoneUsages",phoneUsagesTime);
        report.put("sharpTurn",sharpTurn);
        report.put("currentDate",tripDate);
        report.put("tripId",tripId);

        double phoneUsagesPerT = 0;

        Double mAvgSpeed = Double.valueOf(avgSpeed);
        Double mDistance = Double.valueOf(distance);
        double mDuration = Double.valueOf(duration);
        int mHardBreak = Integer.valueOf(hardbreak);
        int mOverSpeed = Integer.valueOf(overSpeed);
        int mPhoneUsages = Integer.valueOf(phoneUsages);
        int mSharpTurn = Integer.valueOf(sharpTurn);

        if (mDistance==0.0) {
            mDistance = 0.1;
        }

        Double overSpeedingperT = mOverSpeed*1000/mDistance;
        Double hardBreakPerT = mHardBreak*1000/mDistance;
        Double sharpTurnPerT = mSharpTurn*1000/mDistance;
        if (mDuration!=0.0) {
            phoneUsagesPerT = mPhoneUsages / mDuration;
        }
        Double pOverSpeeding = 0.1*overSpeedingperT;
        Double pHardBreak = 0.1*hardBreakPerT;
        Double pSharpTurn = 2.5*sharpTurnPerT;
        Double pPhoneUsages = 0.05*phoneUsagesPerT;

        Log.d("finalscore>>>",mDistance + " >> "+ overSpeedingperT + " >> "+ mOverSpeed + " >> "+ mHardBreak + " >> "+hardBreakPerT);

        report.put("overSpeedPerThousand",overSpeedingperT+"");
        report.put("hardBreakPerThousand",hardBreakPerT+"");// yahan se string banaya
        report.put("sharpTurnPerThousand",sharpTurnPerT+"");
        report.put("phoneUsagesPerThousand",sharpTurnPerT+"");

        report.put("percentOverSpeed",pOverSpeeding+"");
        report.put("percentHardBreak",pHardBreak+"");
        report.put("percentSharpTurn",pSharpTurn+"");
        report.put("percentPhoneUsages",pPhoneUsages+"");

        Double score = 100 - (pOverSpeeding + pHardBreak + pSharpTurn + pPhoneUsages);

        report.put("FinalScore", score.intValue()+"");

        Log.d("finalscore>>>",pOverSpeeding + " >> "+ pHardBreak + " >> "+ pSharpTurn + " >> "+ pPhoneUsages + " >> "+score);

        if (score.intValue()<0) {

          //  finalScore.setText("0");
        }else {
          //  finalScore.setText(score.intValue() + "");
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        Log.d("reportData>>>", StartTime + " >>> "+ distance +" >>> "+ duration + " >>>>> "+ avgSpeed + " >>>> "+
                hardbreak + " >>>>> "+ overSpeed + " >>>>> "+ endTime + " >>>> "+phoneUsages + " >>>>> "+ sharpTurn
                + " >>>>>> ");

        Log.d(" thousand >>> ", sharpTurnPerT + " >>>> "+ hardBreakPerT + " >>> "+ phoneUsagesPerT + " >>>>> "+overSpeedingperT);

        Log.d(" Percent >>>>  ", pOverSpeeding + " >>>> "+ pHardBreak + " >>>>> "+ pSharpTurn + " >>>> "+ pPhoneUsages);

        SharedPreferences sharedPreferencesLoginData = getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE);
        String str = sharedPreferencesLoginData.getString("PHONE", null);


        if (firebaseFirestore!=null){

        }else {
            firebaseFirestore = FirebaseFirestore.getInstance();
        }

        firebaseFirestore.collection("ClubD2S").document("TripData").collection("report")
                .document(str) .collection("all_report").document(dateFormat.format(new Date()))
                .set(report)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("firestore>>", "success");

                        if (pushFlag ==6){
                            stopThisService();
                        }
                        pushFlag = 5;

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("firestore>>", "success");

                    }
                });



        firebaseFirestore.collection("ClubD2SS").document("TripData").collection("report")
                .document(str) .collection("all_report").document(dateFormat.format(new Date()))
                .set(report)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("firestore>>", "success");
                        if (pushFlag ==5){
                            stopThisService();
                        }
                        pushFlag =6;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("firestore>>", "success");
                    }
                });



        addPhoneNumberToDatabase(str);

    }


    /**
     * This method add phone numbers to firestore.
     * @param phone
     */
    private void addPhoneNumberToDatabase(String phone){
        Map<String,Object>data = new HashMap<>();
        data.put("phone",phone);
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection("PhoneNumbers").document(phone).set(data).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("success>","done>");
            }
        });
    }

    /****************************************************************/

    /**
     * This method checks the user permission for allowing tracking phone usages.
     * @return
     */
    private boolean checkPackageUsagesPermission(){
        AppOpsManager appOps = (AppOpsManager) this
                .getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow("android:get_usage_stats",
                android.os.Process.myUid(), this.getPackageName());
        boolean granted = mode == AppOpsManager.MODE_ALLOWED;
        return granted;
    }


    /**
     * This method provide the phone usages stats of user.
     * @return
     */
    private String usesStats(){

        int lauchCount;
        UsageEvents.Event currentEvent;
        List<UsageEvents.Event> allEvents = new ArrayList<>();
        HashMap<String, AppUsageInfo> map = new HashMap <String, AppUsageInfo> ();

        final UsageStatsManager usageStatsManager=(UsageStatsManager)this.getSystemService(Context.USAGE_STATS_SERVICE);
        //  final Map<String,UsageStats> queryUsageStats=usageStatsManager.queryAndAggregateUsageStats(startTimeMills, System.currentTimeMillis());
        UsageEvents usageEvents = usageStatsManager.queryEvents(startTimeInMills, System.currentTimeMillis());
        Log.d("log>>>",usageStatsManager.toString());
        /*for (int i=0; i<queryUsageStats.size(); i++){
            Log.d("usess1>",queryUsageStats.get(i).getPackageName());
            Log.d("usess1>",queryUsageStats.get(i).getPackageName().getClass().getSimpleName());
            Log.d("usess1>",queryUsageStats.get(i).getLastTimeUsed()+"");
            Log.d("usess1>",queryUsageStats.get(i).getTotalTimeInForeground()+"");
            totalPhoneUsages = totalPhoneUsages+ queryUsageStats.get(i).getTotalTimeInForeground();
        }*/

       /* for (Map.Entry<String, UsageStats> entry : queryUsageStats.entrySet())
        {
            System.out.println(entry.getKey() + " / " + entry.getValue());
            Log.d("usess1>",entry.getValue().getPackageName());
            Log.d("usess1>",entry.getValue().getPackageName().getClass().getSimpleName());
            Log.d("usess1>",entry.getValue().getLastTimeUsed()+"");
            Log.d("usess1>",entry.getValue().getTotalTimeInForeground()+"");
            totalPhoneUsages = totalPhoneUsages+ entry.getValue().getTotalTimeInForeground();

        }*/

        while (usageEvents.hasNextEvent()) {
            currentEvent = new UsageEvents.Event();
            usageEvents.getNextEvent(currentEvent);
            if (currentEvent.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND ||
                    currentEvent.getEventType() == UsageEvents.Event.MOVE_TO_BACKGROUND) {
                allEvents.add(currentEvent);
                String key = currentEvent.getPackageName();
// taking it into a collection to access by package name
                if (map.get(key)==null)
                    map.put(key,new AppUsageInfo(key));
            }
        }

        for (int i=0;i<allEvents.size()-1;i++){
            UsageEvents.Event E0=allEvents.get(i);
            UsageEvents.Event E1=allEvents.get(i+1);

//for launchCount of apps in time range
            if (!E0.getPackageName().equals(E1.getPackageName()) && E1.getEventType()==1 && !E1.getPackageName().contains("robotic") &&
                    !E1.getPackageName().contains("launcher") && !E1.getPackageName().contains("systemui")
                    && !E1.getPackageName().contains("home")){
// if true, E1 (launch event of an app) app launched
                map.get(E1.getPackageName()).launchCount++;
                Log.d("launch1>>>",E1.getPackageName()+"");
            }

//for UsageTime of apps in time range
            if (E0.getEventType()==1 && E1.getEventType()==2
                    && E0.getClassName().equals(E1.getClassName()) && !E0.getPackageName().contains("robotic")
                    && !E0.getPackageName().contains("launcher")&& !E0.getPackageName().contains("systemui")
                    && !E0.getPackageName().contains("home")){
                long diff = E1.getTimeStamp()-E0.getTimeStamp();
                totalPhoneUsages+=diff; //gloabl Long var for total usagetime in the timerange
                map.get(E0.getPackageName()).timeInForeground+= diff;
                Log.d("launch2>>>",E0.getPackageName());
            }
        }

        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(totalPhoneUsages),
                TimeUnit.MILLISECONDS.toMinutes(totalPhoneUsages) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(totalPhoneUsages) % TimeUnit.MINUTES.toSeconds(1));
        return hms;

    }


    void setCallback(ServiceCallback callback){
        serviceCallback = callback;
    }


    /**
     * We call this method to stop service manually.
     */
    private void stopThisService(){
        if (serviceCallback!=null){
            serviceCallback.stopService();
        }

        stopForeground(true);

        stopSelf();
    }


}
