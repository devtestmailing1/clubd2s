package hitech.robotic.systemzz;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import hitech.robotic.systemzz.csvReadWrite.CSVWriter;
import hitech.robotic.systemzz.utils.PreferenceHelper;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


/**
 * This class defines home page of app.
 */
public class HomeActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,ServiceCallback {

    private SliderLayout mtopSlider;
    private Button driveSafe, campaign, d2sInNews, contactUs, register, bgProcess;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;
    private ImageView  navMenu,corporateIcon, c1,c2,c3,c4, corporationIcon1;
    private AlertDialog alertDialog;
   // TextView trackYourSafety;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private static final float NS2S = 1.0f / 1000000000.0f;
    private final float[] deltaRotationVector = new float[4];
    private float timestamp;


    String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
    String fileName = "AnalysisData.csv";
    String filePath = baseDir + File.separator + fileName;
    File f = new File(filePath);
    CSVWriter writer;
    FileWriter fileWriter;
    private SensorService sensorService;
    private boolean mBounded;
    private Intent serviceIntent;
    private CountDownTimer imagesTimer;
    private boolean isTimerRunning;
    private ArrayList<Integer> indexes = new ArrayList<>();
    private int imgFlag = 1;
    private LinearLayout secondHeader;
    private Date tripStartTime;

    GoogleApiClient googleApiClient;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);
        intializeView();

/*
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);*/

      //  Crashlytics.getInstance().crash();


        try {
            PendingIntent pendingIntent = (PendingIntent) (getIntent().getParcelableExtra("resolution"));
            if (pendingIntent!=null) {
                startIntentSenderForResult(pendingIntent.getIntentSender(), 1, null, 0, 0, 0);
            }
            } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }

        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            if (extras.containsKey("lowspeed")){
                showTripCanceTrip(1);
            }
            if (extras.containsKey("simpleend")){
                showTripCanceTrip(2);
            }
        }




    }

    /**
     * Getting intent data when user click on notifications.
     * lowspeed and simple means when user click on lowspeed related notification
     * and trip start normal notification.
     * @param intent
     */

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras!=null){
            if (extras.containsKey("lowspeed")){
                showTripCanceTrip(1);
            }
            if (extras.containsKey("simpleend")){
                showTripCanceTrip(2);
            }
        }
    }

    private void intializeView(){
        driveSafe = findViewById(R.id.drive_safe);
        driveSafe.setOnClickListener(this);
        campaign = findViewById(R.id.readmore2);
        d2sInNews = findViewById(R.id.readmore3);
        contactUs = findViewById(R.id.readmore5);
        register = findViewById(R.id.register);
      //  howToUse = findViewById(R.id.how_to_use);
        navMenu = findViewById(R.id.nav_menu);
        register.setOnClickListener(this);
        corporateIcon = findViewById(R.id.corporate_icon);
        corporationIcon1 = findViewById(R.id.corporate_icon1);
       // trackYourSafety = findViewById(R.id.safty_hint);
        bgProcess = findViewById(R.id.bg_process);
        secondHeader = findViewById(R.id.second_header);
        c1 = findViewById(R.id.ds_logo);
        c2 = findViewById(R.id.siam);
        c3 = findViewById(R.id.asdc);
        c4 = findViewById(R.id.how_to_use);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        navigationView = (NavigationView)findViewById(R.id.nv);

      //  howToUse.startAnimation(AnimationUtils.loadAnimation(HomeActivity.this,R.anim.fade_in_fade_out));

        campaign.setOnClickListener(this);
        d2sInNews.setOnClickListener(this);
        contactUs.setOnClickListener(this);
      //  howToUse.setOnClickListener(this);
        navMenu.setOnClickListener(this);
        bgProcess.setOnClickListener(this);

        animateView(driveSafe);
        animateView(register);
        setTopBanner();
        checkAndRequestPermissions();

        userTourManagement();

       // calculateAngle();

        /**
         * Checking phone has gyro or not.
         */
        checkGyroscope();

        navigationManager();

        if (PreferenceHelper.getPreferences(this).contains("company")){

            String uri = "@drawable/"+PreferenceHelper.getStringPreference(this,"company");  // where myresource (without the extension) is the file

            int imageResource = getResources().getIdentifier(uri, null, getPackageName());

            Drawable res = getResources().getDrawable(imageResource);
            corporateIcon.setImageDrawable(res);
            corporationIcon1.setImageDrawable(res);

        }else {
            secondHeader.setVisibility(View.GONE);
        }

        ImageView speedpage= findViewById(R.id.speedpage);
        speedpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(HomeActivity.this);
                dialog.setContentView(R.layout.speed_controler);
                EditText editText = dialog.findViewById(R.id.speedbox);
                Button button = dialog.findViewById(R.id.submit);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PreferenceHelper.setStringPreference(HomeActivity.this,"setspeed",editText.getText().toString());
                        Toast.makeText(HomeActivity.this,"Set Speed",Toast.LENGTH_SHORT).show();
                    }
                });


                dialog.show();
            }
        });

        getToken();

    }


    @Override
    protected void onResume() {
        super.onResume();
        setTopBanners();
        enableGPS();
    }


    /**
     * Handling all options of notification drawer here.
     */
    private void navigationManager(){

        Menu menu = navigationView.getMenu();
        MenuItem logout = menu.findItem(R.id.logout);


        SharedPreferences sharedPreferencesLastData = getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE);
        if (sharedPreferencesLastData.contains("PHONE")){
            logout.setTitle("Logout");
        }else {
            logout.setTitle("Login");
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id)
                {
                    /*case R.id.account:
                        Toast.makeText(HomeActivity.this,"settings",Toast.LENGTH_LONG).show();
                        break;*/
                    case R.id.old_trips:
                      //  Toast.makeText(HomeActivity.this,"settings",Toast.LENGTH_LONG).show();
                        if (checkLogin()){

                            Intent intent = new Intent(HomeActivity.this,PreviousTrips.class);
                            startActivity(intent);

                        }else {
                            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                        break;
                    case R.id.settings:
                      //  Toast.makeText(HomeActivity.this,"settings",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(HomeActivity.this,SettingsActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.privacy:
                        Intent intent1 = new Intent(HomeActivity.this,PrivacyPolicy.class);
                        startActivity(intent1);
                        break;

                    case R.id.how_to_use:
                        Intent intent2 = new Intent(HomeActivity.this,HowToUse.class);
                        startActivity(intent2);
                        break;

                    case R.id.logout:
                       // Intent intent3 = new Intent(HomeActivity.this,SensorService.class);
                        stopService(serviceIntent);
                        if (sensorService.isMainTimerRunning){
                            sensorService.mainTimer.cancel();
                        }
                        SharedPreferences sharedPreferencesLastData = getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE);
                        if (sharedPreferencesLastData.contains("PHONE")) {
                            sharedPreferencesLastData.edit().clear().apply();
                           // logout.setVisible(false);
                            logout.setTitle("Login");
                            Toast.makeText(HomeActivity.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                        }else {
                            Intent intent8 = new Intent(HomeActivity.this, LoginActivity.class);

                            intent8.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent8);
                        }
                        break;

                    case R.id.rankings:
                        Intent intent4 = new Intent(HomeActivity.this,LeaderBoard.class);
                        startActivity(intent4);
                        break;
                    case R.id.coc:
                        Intent intent5 = new Intent(HomeActivity.this,CodeOfConduct.class);
                        startActivity(intent5);
                        break;
                    case R.id.about_us:
                        Intent intent6 = new Intent(HomeActivity.this,AboutUs.class);
                        startActivity(intent6);
                        break;

                    case R.id.shout_out:
                        Intent intent3 = new Intent(HomeActivity.this,ShoutOut.class);
                        startActivity(intent3);
                        break;

                }

                drawerLayout.closeDrawer(GravityCompat.START);

                return true;
            }
        });
    }

    /**
     * Checking user is login or not.
     * @return
     */
    private boolean checkLogin(){
        SharedPreferences sharedPreferencesLoginData = getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE);
        String str = sharedPreferencesLoginData.getString("PHONE", null);
        if (sharedPreferencesLoginData.contains("PHONE") && str!=null){
            return true;
        }else{
            return false;
        }
    }


    /**
     * Setting sliding banner on home page.
     */
    private void setTopBanner(){
        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Drive Safe",R.drawable.banner_1);
        file_maps.put("Follow Traffic Signs",R.drawable.banner_2);
        file_maps.put("Someone is waiting at Home",R.drawable.banner_3);
        file_maps.put("Avoid Mobile Use", R.drawable.banner_4);

        mtopSlider = (SliderLayout)findViewById(R.id.slider);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            mtopSlider.addSlider(textSliderView);
        }

        mtopSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));
        mtopSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mtopSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mtopSlider.setCustomAnimation(new DescriptionAnimation());
        mtopSlider.setDuration(4000);

    }

    /**
     * Animating button on home page.
     * @param button
     */
    private void animateView(Button button) {

        ObjectAnimator anim = ObjectAnimator.ofInt(button, "textColor", Color.WHITE, Color.TRANSPARENT);
        anim.setDuration(1000); //You can manage the blinking time with this parameter
        anim.setEvaluator(new ArgbEvaluator());
        anim.setRepeatMode(ValueAnimator.REVERSE);
        anim.setRepeatCount(ValueAnimator.INFINITE);
        anim.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.drive_safe:

                PreferenceHelper.setStringPreference(HomeActivity.this,"camera","yes");

                //todo: maintain login session
                SharedPreferences sharedPreferencesLastData = getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE);
                String str = sharedPreferencesLastData.getString("PHONE", null);

                if((str != null) && (DBHandlerWalk.Instance(getApplicationContext()).registeredUser()))
                {
                            //Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                            Intent intent = new Intent(HomeActivity.this,CameraSplashActivity.class);
                           // Intent intent = new Intent(HomeActivity.this,TwoOptions.class);
                            startActivity(intent);
                            finish();

                } else{

                            // Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);

                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                          //  finish();


                }

              //  Intent intent = new Intent(HomeActivity.this, CameraSplashActivity.class);
              //  startActivity(intent);
                break;

            case R.id.readmore2:
                openClickLinks("https://clubd2s.com/campaigns/");
                break;
            case R.id.readmore3:
                openClickLinks("https://clubd2s.com/blog/");
                break;
            case R.id.readmore5:
                openClickLinks("https://clubd2s.com/contact/");
                break;
            case R.id.register:
                openClickLinks("https://onecrorehaath.com/");
                break;
           /* case R.id.how_to_use:
              //  checkUsegesPermission();
               // usesStats();
                break;*/

            case R.id.nav_menu:
                drawerLayout.openDrawer(Gravity.LEFT);
                break;

            case R.id.bg_process:

                PreferenceHelper.setStringPreference(HomeActivity.this,"camera","no");

                //todo: maintain login session
                SharedPreferences sharedPreferencesLastData1 = getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE);
                String str1 = sharedPreferencesLastData1.getString("PHONE", null);

                if((str1 != null) && (DBHandlerWalk.Instance(getApplicationContext()).registeredUser()))
                {
                   // callBackgroundProcess();
                    showWithoutCameraDialog();

                } else{

                    // Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    //  finish();


                }

                break;



        }
    }

    private void openClickLinks(String url){
        Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }


    /**
     * Checking all user permissions here.
     * @return
     */
    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        int permissionCLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionFLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        int callPermission =  ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int callPermission1 = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            callPermission1 = ContextCompat.checkSelfPermission(this, Manifest.permission.ANSWER_PHONE_CALLS);
        }
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (permissionCLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (permissionFLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (callPermission!= PackageManager.PERMISSION_GRANTED){
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        if (callPermission1!= PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(Manifest.permission.ANSWER_PHONE_CALLS);
            }
        }

        if (result != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (result1 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_ACCOUNTS);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("granted>","yes");
                    checkAppUsesPermissions();
                } else {
                    Log.d("granted>","no");
                }
                break;
        }
    }


    /**
     * Here we organising tour for first time user.
     */
    private void userTourManagement(){
        if (!PreferenceHelper.getPreferences(HomeActivity.this).contains("pressed")){
            firstTimePrompt(driveSafe,"Enable Safe Drive","Tap here to track your trip safety.");
        }else {
            if (PreferenceHelper.getStringPreference(HomeActivity.this,"pressed").equalsIgnoreCase("esd")){
                firstTimePrompt(register,"Register Yourself", "Tap here to register yourself.");
            }else {
                if (!PreferenceHelper.getStringPreference(HomeActivity.this,"pressed").equalsIgnoreCase("htu")) {
                   // firstTimePrompt(howToUse, "User Manual", "To read complete manual tap here.");
                }
            }
        }
    }


    /**
     * Setting tour for first time user.
     * @param view
     * @param heading
     * @param description
     */
    private void firstTimePrompt(View view,String heading, String description){
        new MaterialTapTargetPrompt.Builder(HomeActivity.this)
                .setTarget(view)
                .setPrimaryText(heading)
                .setSecondaryText(description)
                .setIdleAnimationEnabled(true)
                .setPromptStateChangeListener(new MaterialTapTargetPrompt.PromptStateChangeListener()
                {
                    @Override
                    public void onPromptStateChanged(MaterialTapTargetPrompt prompt, int state)
                    {
                        if ((state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED || state == MaterialTapTargetPrompt.STATE_NON_FOCAL_PRESSED) && view.getId()== R.id.drive_safe )
                        {
                            Log.d("yes prompted","yes");
                            PreferenceHelper.setStringPreference(HomeActivity.this,"pressed","esd");
                            userTourManagement();
                        }else if ((state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED || state == MaterialTapTargetPrompt.STATE_NON_FOCAL_PRESSED) && view.getId()== R.id.register){
                            PreferenceHelper.setStringPreference(HomeActivity.this,"pressed","register");
                            userTourManagement();
                        }else if ((state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED || state == MaterialTapTargetPrompt.STATE_NON_FOCAL_PRESSED) && view.getId()== R.id.how_to_use){
                            PreferenceHelper.setStringPreference(HomeActivity.this,"pressed","htu");
                        }
                    }
                })
                .show();
    }


        private void checkUsegesPermission(){
            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
            startActivity(intent);
        }


    /**
     * Setting phone usages permission dialog box.
     */
    private void checkAppUsesPermissions(){
        alertDialog =  new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Dialog_Alert)
                .setTitle("DETECT PHONE USES WHILE DRIVING")
                .setMessage("We will track your phone uses while you are driving. It helps you to analyse your" +
                        " common phone distractions. Please grant permission to track phone uses during driving.")

                .setPositiveButton("Lets do it.", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        checkUsegesPermission();
                        alertDialog.dismiss();
                    }
                })
                .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .show();
    }


    private void calculateAngle(){
        Location location = new Location("");
        location.setLatitude(20.237234);
        location.setLongitude(75.334708);

        Location location1 = new Location("");
        location1.setLatitude(20.638728);
        location1.setLongitude(76.048507);

        Log.d("bearing>>>>", location.bearingTo(location1)+" >>>> "+ location1.bearingTo(location));


    }


    /**
     * Checking phone has gyro or not.
     */
    private void checkGyroscope(){

        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);


        SensorEventListener eventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
               // timestamp = event.timestamp;
              //  Log.d("timeStamp>>>",timestamp+"");

                if (timestamp != 0) {
                    final float dT = (event.timestamp - timestamp) * NS2S;
                    // Axis of the rotation sample, not normalized yet.
                    float axisX = event.values[0];
                    float axisY = event.values[1];
                    float axisZ = event.values[2];

                    // Calculate the angular speed of the sample
                    double omegaMagnitude = Math.sqrt(axisX * axisX + axisY * axisY + axisZ * axisZ);

                    // Normalize the rotation vector if it's big enough to get the axis
                    // (that is, EPSILON should represent your maximum allowable margin of error)
                    if (omegaMagnitude > 0.000000001f) {
                        axisX /= omegaMagnitude;
                        axisY /= omegaMagnitude;
                        axisZ /= omegaMagnitude;
                    }


                    double thetaOverTwo = omegaMagnitude * dT / 2.0f;
                    float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
                    float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
                    deltaRotationVector[0] = sinThetaOverTwo * axisX;
                    deltaRotationVector[1] = sinThetaOverTwo * axisY;
                    deltaRotationVector[2] = sinThetaOverTwo * axisZ;
                    deltaRotationVector[3] = cosThetaOverTwo;

                   /* Log.d("rotationvector> ", deltaRotationVector[0]+ "     >   "+deltaRotationVector[1] + "   >   "+
                            deltaRotationVector[2]+"    >    "+deltaRotationVector[3]);*/

                   // Log.d("rotationvector>1 ",axisX+" > "+axisY+" > "+axisZ + " > "+ omegaMagnitude);

                   /* writeDataToCSV(axisX+"",axisY+"",axisZ+"",omegaMagnitude+"",
                            deltaRotationVector[0]+"",deltaRotationVector[1]+"",
                            deltaRotationVector[2]+"",deltaRotationVector[3]+"");*/

                }

                timestamp = event.timestamp;
                float[] deltaRotationMatrix = new float[9];
                SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);


            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        sensorManager.registerListener(eventListener,sensor,SensorManager.SENSOR_DELAY_NORMAL);

    }


    private void writeDataToCSV(String axisX, String axisY, String axisZ, String omegamegnitude, String deltaRotationVector1,
                                String deltaRotationVector2, String deletaRotationVector3, String deletaRotationVector4){
        if(f.exists()&&!f.isDirectory())
        {
            try {
                fileWriter = new FileWriter(filePath, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            writer = new CSVWriter(fileWriter);
        }
        else
        {
            try {
                writer = new CSVWriter(new FileWriter(filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String[] data = {axisX,axisY,axisZ,omegamegnitude,deltaRotationVector1,deltaRotationVector2,deletaRotationVector3,deletaRotationVector4};

        writer.writeNext(data);

        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void callBackgroundProcess(){


        if (mBounded){
            unbindService(mConnection);
            mBounded = false;
        }
        stopService(serviceIntent);

        serviceIntent = new Intent(this,SensorService.class);
        bindService(serviceIntent, mConnection, BIND_AUTO_CREATE);


      //  enableGPS();
        tripStartTime = new Date();

        PreferenceHelper.setLongPreference(HomeActivity.this,"sDate",new Date(System.currentTimeMillis()).getTime());


        HomeActivity.this.moveTaskToBack(true);
       /* final PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(BackgroundProcessing.class, 15, TimeUnit.MINUTES).build();
        WorkManager.getInstance(this).enqueue(periodicWorkRequest);*/
     /*  Intent intent = new Intent(this,SensorService.class);*/
       serviceIntent.putExtra("inputExtra","Drive Safe Club Enabled");
       ContextCompat.startForegroundService(this,serviceIntent);
    }


    @Override
    protected void onStart() {
        super.onStart();
         serviceIntent = new Intent(this,SensorService.class);
        bindService(serviceIntent, mConnection, BIND_AUTO_CREATE);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sensorService!=null && sensorService.isMainTimerRunning){
            sensorService.mainTimer.cancel();
        }

        if (mBounded){
            unbindService(mConnection);
            mBounded = false;
        }

        stopService(serviceIntent);
    }

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBounded = true;
            SensorService.LocalBinder localBinder = (SensorService.LocalBinder)service;
            sensorService = localBinder.getServerInstance();
            sensorService.setCallback(HomeActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBounded = false;
            sensorService = null;
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        /*if (mBounded){
            unbindService(mConnection);
            mBounded = false;
        }*/

        if (isTimerRunning){
            imagesTimer.cancel();
        }
    }


    /**
     * Cancel trip dialog box.
     * @param endFlag
     */
    private void showTripCanceTrip(int endFlag){
        new androidx.appcompat.app.AlertDialog.Builder(HomeActivity.this)
                .setTitle("Stop your trip")
                .setMessage("Wants to cancel the trip?")
                .setPositiveButton(R.string.endtrip, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (endFlag==1){
                        sensorService.endTrip();
                        }else {
                            sensorService.simplyEndService();
                        }
                        /*if (mBounded){
                            unbindService(mConnection);
                            mBounded=false;
                        }
                        stopService(serviceIntent);*/
                        sensorService.cancelNotification();
                    }
                }).setNegativeButton(R.string.canceltrip,null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    /**
     * This dialog show when we click on "Without camera" button.
     */
    private void showWithoutCameraDialog(){
        new androidx.appcompat.app.AlertDialog.Builder(HomeActivity.this)
                .setTitle("Background Mode")
                .setMessage("App will track your driving in background. When you stop your vehicle for more then 30 seconds, " +
                        "you will get a notification to stop your trip. Click on that notification when you want to stop your trip. And do not remove app from background to run it properly." +
                        " Let's start the journey.")
                .setPositiveButton(R.string.gotit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBackgroundProcess();
                    }
                }).setNegativeButton(R.string.dontstart,null)
                .show();
    }


    /**
     * Setting banners of random companies on top.
     */
    private void setTopBanners(){
        imagesTimer = new CountDownTimer(Long.MAX_VALUE,5000) {
            @Override
            public void onTick(long millisUntilFinished) {

                ImageViewAnimatedChange(HomeActivity.this,c1,randomNumber());
                ImageViewAnimatedChange(HomeActivity.this,c2,randomNumber());
                ImageViewAnimatedChange(HomeActivity.this,c3,randomNumber());
                ImageViewAnimatedChange(HomeActivity.this,c4,randomNumber());

                isTimerRunning = true;
            }

            @Override
            public void onFinish() {
                indexes.clear();
                isTimerRunning=false;
               // imagesTimer.start();
            }
        }.start();
    }

    private Bitmap randomNumber(){

        String [] companies = new String[]{"Select Your Company","SIAM","SAFE","CII","ASDC","ACMA","IRSC","AIMA","CLUBDTWOS"
                /*"Apollo Tyres Ltd", "Mahindra Rise", "Hella India Lighting Ltd", "Hella India Automotive Pvt Ltd", "WABCO India Ltd",*/
               /* "Brakes India Pvt Ltd"*/};

        int randomNum = ThreadLocalRandom.current().nextInt(1, 12 + 1);
       // Log.d("randomimage>",companies[randomNum].toLowerCase().replaceAll("\\s",""));


        if ( indexes.isEmpty() || !indexes.contains(randomNum)){
            indexes.add(randomNum);
           // Log.e("indexes>",indexes.toString());
        }

        String uri = "@drawable/"+companies[imgFlag].toLowerCase().replaceAll("\\s","");  // where myresource (without the extension) is the file

        int imageResource = getResources().getIdentifier(uri, null, getPackageName());

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                imageResource);

        /*if (indexes.size()==4){
            indexes.clear();
        }*/

        if (imgFlag!=(companies.length - 1)){
            imgFlag++;
            /*if (imgFlag==11){
                imgFlag++;
            }*/
        }else {
            imgFlag= 1;
        }

        return icon;
    }

    /**
     * Setting animation for top banners.
     * @param c
     * @param v
     * @param new_image
     */
    public static void ImageViewAnimatedChange(Context c, final ImageView v, final Bitmap new_image) {
        final Animation anim_out = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
        final Animation anim_in  = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
        anim_out.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                v.setImageBitmap(new_image);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(anim_in);
            }
        });
        v.startAnimation(anim_out);
    }


    /**
     * Enable GPS
     */
    private void enableGPS() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(HomeActivity.this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:

                           // setTopBanner();

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            try {
                                /*googleApiClient.getContext().startActivity(new Intent(googleApiClient.getContext(), HomeActivity.class)
                                        .putExtra("resolution",status.getResolution()).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));*/
                                 status.startResolutionForResult(HomeActivity.this, 1000);
                            } catch (Exception e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            break;
                    }
                }
            });
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void stopService() {
        if (mBounded){
            unbindService(mConnection);
            mBounded = false;
            Log.e("unbind>>","success");
        }
        if (serviceIntent!=null) {
            stopService(serviceIntent);
            Log.e("stopservice>","success");
        }
    }

    private void getToken(){
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                String token = task.getResult().getToken();
                Log.e("deviceToken > ", token+"");
            }
        });
    }

}
