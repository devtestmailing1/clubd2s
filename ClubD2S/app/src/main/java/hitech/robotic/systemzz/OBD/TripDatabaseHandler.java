package hitech.robotic.systemzz.OBD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.LinkedHashSet;

public class TripDatabaseHandler {

    private static TripDatabaseHelper dbHelper;
    private static SQLiteDatabase database;
    private Context context;
    private LinkedHashSet<TripData> ArrayTrip = new LinkedHashSet<>();

    public TripDatabaseHandler(Context c) {
        context = c;
    }

    public TripDatabaseHandler open() throws SQLException {
        dbHelper = new TripDatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }


    public void insertTripData(String timeStamp, String date, String time, String eventName, String tripId ){
        ContentValues values = new ContentValues();
        values.put(TripDatabaseHelper.COLUMN_TIMESTAMP, timeStamp);
        values.put(TripDatabaseHelper.COLUMN_DATE, date);
        values.put(TripDatabaseHelper.COLUMN_TIME, time );
        values.put(TripDatabaseHelper.COLUMN_EVENTNAME, eventName);
        values.put(TripDatabaseHelper.COLUMN_TRIPID, tripId);
        database.insert(TripDatabaseHelper.TRIPEVENT_TABLE_NAME, null, values);
    }


    public LinkedHashSet<TripData> getTripData() {

        try {

            String[] columns = new String[] {TripDatabaseHelper.COLUMN_TIMESTAMP, TripDatabaseHelper.COLUMN_DATE,TripDatabaseHelper.COLUMN_TIME,
                    TripDatabaseHelper.COLUMN_EVENTNAME,TripDatabaseHelper.COLUMN_TRIPID};
            Cursor cursor = database.query(TripDatabaseHelper.TRIPEVENT_TABLE_NAME, columns, null, null, null, null, null);

            cursor.moveToFirst();
            String result = "";

            for(int i = 0;i<cursor.getCount();i++)
            {

                    String result_0 = cursor.getString(0);
                    String result_1 = cursor.getString(1);
                    String result_2 = cursor.getString(2);
                    String result_3 = cursor.getString(3);
                    String result_4 = cursor.getString(4);
                    result += String.valueOf(result_0) + " " + result_1 + " " + result_2 + " " + result_3 + " " + result_4 + " "+ System.getProperty("line.separator");
                    Log.d("Fetch Data", result);
                    TripData tripData = new TripData(result_0, result_1, result_2, result_3, result_4);
                    ArrayTrip.add(tripData);
                cursor.moveToNext();
        }
        return  ArrayTrip;
        }
        catch (SQLiteException e) {

        }
        return null;
    }

    public void clearSqliteDb()
    {
        database.execSQL("delete from "+ TripDatabaseHelper.TRIPEVENT_TABLE_NAME);
    }


}
