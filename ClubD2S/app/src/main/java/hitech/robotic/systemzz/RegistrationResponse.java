package hitech.robotic.systemzz;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegistrationResponse {
    @SerializedName("response")
    public List<RegistrationResponseResult> registrationResponse;

    public List<RegistrationResponseResult> getRegistrationResponse() { return registrationResponse; }
}
