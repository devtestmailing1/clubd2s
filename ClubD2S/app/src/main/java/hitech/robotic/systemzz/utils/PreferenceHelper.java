package hitech.robotic.systemzz.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceHelper {
    public static final String PREF_NAME = PreferenceHelper.class.getName();
    public static final String PERMANENT_PREF_NAME = PreferenceHelper.class.getName() + "remember";
    private static SharedPreferences preferences;

    public static void setStringPreference(Context context, String KEY,
                                           String value) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putString(KEY, value);
        editor.commit();
    }

    public static String getStringPreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        return preferences.getString(KEY, "");
    }




    public static void setLongPreference(Context context, String KEY,
                                           long value) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putLong(KEY, value);
        editor.apply();
    }


    public static long getLongPreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        return  preferences.getLong(KEY, 0);
    }




    public static void setDoublePreference(Context context, String KEY,
                                           double value) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putFloat(KEY, (float) value);
        editor.commit();
    }

    public static double getDoublePreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        return  preferences.getFloat(KEY, 0);
    }



    public static void setIntPreference(Context context, String KEY, int value) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putInt(KEY, value);
        editor.commit();
    }

    public static int getIntPreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);

        return preferences.getInt(KEY, 0);
    }

    public static void setBooleanPreference(Context context, String KEY,
                                            boolean value) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putBoolean(KEY, value);
        editor.commit();
    }

    public static boolean getBooleanPreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);

        return preferences.getBoolean(KEY, false);
    }

    public static void clearAllPreferences(Context context) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);

        preferences.edit().clear().commit();

    }

    public static void setPermanentStringPreference(Context context, String KEY,
                                                    String value) {
        preferences = context.getSharedPreferences(PERMANENT_PREF_NAME,
                Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putString(KEY, "" + value);
        editor.commit();
    }

    public static String getPermanentStringPreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PERMANENT_PREF_NAME,
                Context.MODE_PRIVATE);
        return preferences.getString(KEY, "");
    }


    public static void setPermanentBooleanPreference(Context context, String KEY,
                                                     boolean value) {
        preferences = context.getSharedPreferences(PERMANENT_PREF_NAME,
                Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putBoolean(KEY, value);
        editor.commit();
    }

    public static boolean getPermanentBooleanPreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PERMANENT_PREF_NAME,
                Context.MODE_PRIVATE);
        return preferences.getBoolean(KEY, false);
    }

    public static SharedPreferences getPreferences(Context context){
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return preferences;
    }

    public static void cleanPerticualrPref(Context context, String key ){
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

}