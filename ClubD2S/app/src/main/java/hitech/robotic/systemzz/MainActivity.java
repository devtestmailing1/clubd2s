package hitech.robotic.systemzz;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import hitech.robotic.systemzz.fragments.AboutFragment;
import hitech.robotic.systemzz.fragments.FacebookFragment;
import hitech.robotic.systemzz.fragments.SubuFragment;
import hitech.robotic.systemzz.fragments.TwitterFragment;
import hitech.robotic.systemzz.fragments.WalkFragment;

public class MainActivity extends AppCompatActivity implements ForceUpdateChecker.OnUpdateNeededListener {

    int internet = 0;
    private FloatingActionButton mFloatingActionButton  ;
    private BottomNavigationView.OnNavigationItemSelectedListener
            mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_about:
                    startCameraActivity();
                    /*Intent about=new Intent(MainActivity.this,About.class);
                    startActivity(about);*/
                    //  aboutFragment();
                    //aboutFragment();
                    return true;
                case R.id.navigation_walk:
                    Intent contribute = new Intent(MainActivity.this, MapsActivity.class);
                    startActivity(contribute);
                    //walkFragment();

                    //startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    return true;
                case R.id.navigation_facebook:
                    fbFragment();
                    return true;

                case R.id.navigation_twitter:
                    twitterFragment();
                    return true;

                case R.id.navigation_home:
                    /*Intent subu = new Intent(MainActivity.this, Subbu.class);
                    startActivity(subu);*/
                   // homeFragment();
                    return true;
            }
            return false;
        }
    };

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ForceUpdateChecker.with(this).onUpdateNeeded(MainActivity.this).check();
        /*initializeFirebase();
        String playStoreVersionCode = FirebaseRemoteConfig.getInstance().getString(
                "android_latest_version_code");
        PackageInfo pInfo = null;
        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentAppVersionCode = String.valueOf(pInfo.versionCode);
        if(playStoreVersionCode != currentAppVersionCode){
            //Show update popup or whatever best for you
            Toast.makeText(getApplicationContext(),"playStoreVersionCode"+ playStoreVersionCode,Toast.LENGTH_SHORT).show();
            Toast.makeText(getApplicationContext(),"current: " + currentAppVersionCode,Toast.LENGTH_SHORT).show();

            Toast.makeText(getApplicationContext(),"Update",Toast.LENGTH_SHORT).show();
        } else{
            Toast.makeText(getApplicationContext(),"SUPERRRRRRRR",Toast.LENGTH_SHORT).show();

        }*/
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().getItem(2).setChecked(true);
       // homeFragment();
    }

    /*public void initializeFirebase() {
        if (FirebaseApp.getApps(getApplicationContext()).isEmpty()) {
            FirebaseApp.initializeApp(getApplicationContext(), FirebaseOptions.fromResource(getApplicationContext()));
        }
        final FirebaseRemoteConfig config = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        config.setConfigSettings(configSettings);
    }*/

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    /*private void homeFragment() {
        HomeFragment homeFragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container_main, homeFragment);
        transaction.commit();

    }*/

    private void fbFragment() {
        FacebookFragment facebookFragment = new FacebookFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container_main, facebookFragment);
        transaction.commit();

    }

    private void twitterFragment() {
        TwitterFragment twitterFragment = new TwitterFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container_main, twitterFragment);
        transaction.commit();

    }

    private void walkFragment() {
        WalkFragment walkFragment = new WalkFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container_main, walkFragment);
        transaction.commit();
    }

    private void aboutFragment() {
        AboutFragment aboutFragment = new AboutFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container_main, aboutFragment);
        transaction.commit();
    }

    private void subuFragment() {
        SubuFragment subuFragment = new SubuFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container_main, subuFragment);
        transaction.commit();
    }



    private void startCameraActivity(){
        Intent cameraActivity = new Intent(this,CameraSplashActivity.class);
        startActivity(cameraActivity);
    }

    @Override
    public void onUpdateNeeded(final String updateUrl) {

        String verstr = ForceUpdateChecker.with(this).getplaystoreappversion();//ForceUpdateChecker.with(getApplicationContext()).getAppVersion();

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version "+verstr+" to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //finish();
                            }
                        }).create();
        //dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}

