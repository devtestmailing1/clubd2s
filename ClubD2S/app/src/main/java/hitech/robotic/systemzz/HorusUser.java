package hitech.robotic.systemzz;

public class HorusUser {
    public int id;
    public String email;
    public String phone;
    public String name;
    public String password;

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public HorusUser(int uid, String em, String nm, String ph, String pswd)
    {
        this.id = uid;
        this.email = em;
        this.name = nm;
        this.phone = ph;
        this.password = pswd;
    }

}
