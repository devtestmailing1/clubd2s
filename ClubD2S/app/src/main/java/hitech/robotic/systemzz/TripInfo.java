package hitech.robotic.systemzz;

public class TripInfo {
    private int tripnum;
    private long startTimestamp;
    private long endTimestamp;
    private long steps;
    private float distace;
    private int uploadStatus;

    TripInfo(){}
    TripInfo(int tn, long startTs, long endTs, long st, float dist, int upls)
    {
        this.tripnum = tn;
        this.startTimestamp = startTs;
        this.endTimestamp = endTs;
        this.steps = st;
        this.distace = dist;
        this.uploadStatus = upls;
    }

    public void setTripnum(int tn){ this.tripnum = tn; }
    public int getTripnum(){ return tripnum; }
    public void setStartTimestamp(long ts){ this.startTimestamp = ts; }
    public long getStartTimestamp(){ return startTimestamp; }
    public void setEndTimestamp(long ts){ this.endTimestamp = ts; }
    public long getEndTimestamp(){ return endTimestamp; }
    public void setSteps(long st){ this.steps = st; }
    public long getSteps(){ return steps; }
    public void setDistance(float dist){ this.distace = dist; }
    public float getDistance(){ return distace; }
    public void setUploadStatus( int upls ) { this.uploadStatus = upls; }
    public int getUploadStatus() { return uploadStatus; }

}
