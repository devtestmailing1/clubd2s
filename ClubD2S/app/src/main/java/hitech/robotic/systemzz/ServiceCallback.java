package hitech.robotic.systemzz;

/**
 * Sensor service callback
 */
public interface ServiceCallback {
    void stopService();
}
