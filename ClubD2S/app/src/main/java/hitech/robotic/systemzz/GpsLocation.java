package hitech.robotic.systemzz;


public class GpsLocation {
    private long timestamp;
    private String latitude;
    private String longitude;
    private int tripnum;



    private float speed;
    //constructors
    GpsLocation(){}
    public GpsLocation(long ts, String lat, String lng, int tn){
        this.timestamp = ts;
        this.latitude = lat;
        this.longitude = lng;
        this.tripnum = tn;
    }

    public GpsLocation(long ts, String lat, String lng, float sp, int tn){
        this.timestamp = ts;
        this.latitude = lat;
        this.longitude = lng;
        this.speed = sp;
        this.tripnum = tn;
    }

    public void setTimestamp(long ts)
    {
        this.timestamp = ts;
    }

    public long getTimestamp()
    {
        return this.timestamp;
    }

    public void setLatitude(String lat)
    {
        this.latitude = lat;
    }

    public String getLatitude()
    {
        return this.latitude;
    }

    public void setLongitude(String lng)
    {
        this.longitude = lng;
    }

    public String getLongitude()
    {
        return this.longitude;
    }

    public void setTripnum(int tn) { this.tripnum = tn; }

    public int getTripnum() { return this.tripnum; }

    public float getSpeed() {return speed;}

    public void setSpeed(float speed) {this.speed = speed;}
}
