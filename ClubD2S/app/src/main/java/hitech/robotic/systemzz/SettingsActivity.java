package hitech.robotic.systemzz;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

import hitech.robotic.systemzz.R;
import hitech.robotic.systemzz.utils.SeekBarPreference;

/**
 * This class is used to setup settings page.
 */

public class SettingsActivity extends PreferenceActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();




    private static Preference.OnPreferenceChangeListener listener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String stringvalue = newValue.toString();
            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringvalue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else if (preference instanceof SwitchPreference) {
               /* if (preference.getKey().equals("key_gallery_name")) {
                    // update the changed gallery name to summary filed
                }*/

                preference.setSummary(stringvalue);
            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");*/
        // load settings fragment
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MainPreferenceFragment()).commit();
    }

    public static class MainPreferenceFragment extends PreferenceFragment  {


        String Accelerometer;
        String Gyroscope;
        String VideoRecord;
        String AudioRecord;
        String BeepSound;
        String Detection;

        private SeekBarPreference _seekBarPref;

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings_main);

            /*Preference accelero = findPreference(getString(R.string.notifications_new_message));

            accelero.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    boolean isOn = (boolean) newValue;
                    if (isOn) {
                        Accelerometer = "ON";
                        Gyroscope= "ON";
                        //Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
                    } else {
                        Accelerometer="OFF";
                        Gyroscope="OFF";
                        // Toast.makeText(getActivity(), "not clicked", Toast.LENGTH_SHORT).show();
                    }
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("SETTING", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor1 = sharedPreferences.edit();
                    editor1.putString("ACCELERO_STATE", Accelerometer);
                    editor1.putString("GYRO_STATE", Gyroscope);
                    editor1.apply();
                    return true;
                }
            });*/

            /************************* Speeed Bar **************************/

            _seekBarPref = (SeekBarPreference) this.findPreference("SEEKBAR_VALUE");
            int radius = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getInt("SEEKBAR_VALUE", 60);
            _seekBarPref.setSummary(this.getString(R.string.settings_summary).replace("$1", ""+radius));

            _seekBarPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    int radius = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt("SEEKBAR_VALUE", 60);
                    _seekBarPref.setSummary(getString(R.string.settings_summary).replace("$1", ""+radius));

                    Log.d("seekbar1>>", radius+" > " + _seekBarPref.getSummary());



                    return true;
                }
            });


            /*************************************************************************/



            Preference Detect = findPreference(getString(R.string.key_detection));

            /*Detect.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    boolean isOn = (boolean) newValue;
                    if (isOn) {
                       Detection = "ON";

                        //Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
                    } else {
                        Detection="OFF";

                        // Toast.makeText(getActivity(), "not clicked", Toast.LENGTH_SHORT).show();
                    }
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("SETTING", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor1 = sharedPreferences.edit();
                    editor1.putString("DETECT_STATE", Detection);

                    editor1.apply();
                    return true;
                }
            });*/

           /*Preference gyroscope = findPreference(getString(R.string.key_notifications_new_message_ringtone));

           gyroscope.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
               @Override
               public boolean onPreferenceChange(Preference preference, Object newValue) {
                   boolean isOn = (boolean) newValue;
                   if (isOn) {
                       Gyroscope = "ON";
                       Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
                   } else {
                       Gyroscope="OFF";

                       Toast.makeText(getActivity(), "not clicked", Toast.LENGTH_SHORT).show();
                   }
                   SharedPreferences sharedPreferences = getActivity().getSharedPreferences("SETTING", Context.MODE_PRIVATE);
                   SharedPreferences.Editor editor1 = sharedPreferences.edit();
                   editor1.putString("GYRO_STATE", Gyroscope);
                   editor1.apply();
                   return true;
               }
           });*/

          //  Preference AutoRecordVideo = findPreference(getString(R.string.key_vibrate));

            /*AutoRecordVideo.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    boolean isOn = (boolean) newValue;
                    if (isOn) {
                        VideoRecord = "ON";
                        //Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
                    } else {
                        VideoRecord="OFF";

                        // Toast.makeText(getActivity(), "not clicked", Toast.LENGTH_SHORT).show();
                    }
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("SETTING", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor1 = sharedPreferences.edit();
                    editor1.putString("AUTO_VIDEO_RECORD_STATE", VideoRecord);
                    editor1.apply();
                    return true;
                }
            });*/

           /* Preference AutoRecordAudio = findPreference(getString(R.string.key_gallery_name));

            AutoRecordAudio.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    boolean isOn = (boolean) newValue;
                    if (isOn) {
                        AudioRecord = "ON";
                        //Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
                    } else {
                        AudioRecord="OFF";

                        //Toast.makeText(getActivity(), "not clicked", Toast.LENGTH_SHORT).show();
                    }
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("SETTING", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor1 = sharedPreferences.edit();
                    editor1.putString("AUTO_AUDIO_RECORD_STATE", AudioRecord);
                    editor1.apply();
                    return true;
                }
            });*/

            Preference Beep = findPreference(getString(R.string.key_upload_over_wifi));

            Beep.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    boolean isOn = (boolean) newValue;
                    if (isOn) {
                        BeepSound = "ON";
                        //Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
                    } else {
                        BeepSound="OFF";

                        //Toast.makeText(getActivity(), "not clicked", Toast.LENGTH_SHORT).show();
                    }
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("SETTING", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor1 = sharedPreferences.edit();
                    editor1.putString("BEEP_UPLOAD", BeepSound);
                    editor1.apply();
                    return true;
                }
            });

            Preference DeleteVideos=findPreference(getString(R.string.clear_videos));

            DeleteVideos.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    Toast.makeText(getActivity(), "deleted", Toast.LENGTH_SHORT).show();
                    deleteTempFolder("ClubD2S/Videos");
                    return true;

                }
            });
            Preference DeleteLogs=findPreference(getString(R.string.clear_logs));

            DeleteLogs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    Toast.makeText(getActivity(), "deleted", Toast.LENGTH_SHORT).show();
                    deleteTempFolder("ClubD2S/Logs");
                    return true;

                }
            });

            Preference DeleteAll=findPreference(getString(R.string.clear_all_data));
            DeleteAll.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    Toast.makeText(getActivity(), "deleted", Toast.LENGTH_SHORT).show();
                    deleteTempFolder("ClubD2S/Videos");
                    deleteTempFolder("ClubD2S/Logs");
                    return true;

                }
            });
        }


        /* boolean isOn = (boolean) newValue;
                     if (isOn) {
             Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
         } else {
             Toast.makeText(getActivity(), "not clicked", Toast.LENGTH_SHORT).show();
         }*/
        private void deleteTempFolder(String dir) {
            File myDir = new File(Environment.getExternalStorageDirectory() + "/"+dir);
            if (myDir.isDirectory()) {
                String[] children = myDir.list();
                for (int i = 0; i < children.length; i++) {
                    new File(myDir, children[i]).delete();
                }
            }
        }

        private void makeSensorsOnByDefault(){
            Accelerometer = "ON";
            Gyroscope= "ON";

            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("SETTING", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor1 = sharedPreferences.edit();
            editor1.putString("ACCELERO_STATE", Accelerometer);
            editor1.putString("GYRO_STATE", Gyroscope);
            editor1.apply();
        }

    }



}


