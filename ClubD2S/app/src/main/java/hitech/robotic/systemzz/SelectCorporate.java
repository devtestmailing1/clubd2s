package hitech.robotic.systemzz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import hitech.robotic.systemzz.utils.PreferenceHelper;

public class SelectCorporate extends AppCompatActivity implements View.OnClickListener {

    private ImageView aima, asdc, cii, fada, irsc, siam;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_employer);
        initializeViews();
    }

    private void initializeViews(){
        aima = findViewById(R.id.aima);
        asdc = findViewById(R.id.asdc);
        cii = findViewById(R.id.cii);
        fada = findViewById(R.id.fada);
        irsc = findViewById(R.id.irsc);
        siam = findViewById(R.id.siam);

        aima.setOnClickListener(this);
        asdc.setOnClickListener(this);
        cii.setOnClickListener(this);
        fada.setOnClickListener(this);
        irsc.setOnClickListener(this);
        siam.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.aima:
                goToNextScreen("aima");
                break;
            case R.id.asdc:
                goToNextScreen("asdc");
                break;
            case R.id.cii:
                goToNextScreen("cii");
                break;
            case R.id.fada:
                goToNextScreen("fada");
                break;
            case R.id.irsc:
                goToNextScreen("irsc");
                break;
            case R.id.siam:
                goToNextScreen("siam");
                break;
        }
    }


    private void goToNextScreen(String company){
        PreferenceHelper.setStringPreference(this,"company",company);
        Intent intent = new Intent(SelectCorporate.this, CameraSplashActivity.class);
        startActivity(intent);
        finish();
    }

}
