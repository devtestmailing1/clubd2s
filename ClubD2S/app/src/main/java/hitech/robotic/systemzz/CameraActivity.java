package hitech.robotic.systemzz;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.crashlytics.android.Crashlytics;
import com.github.tcking.giraffecompressor.GiraffeCompressor;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.snatik.storage.Storage;
import com.snatik.storage.helpers.OrderType;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import hitech.robotic.systemzz.OBD.BluetoothIOGateway;
import hitech.robotic.systemzz.OBD.DeviceBroadcastReceiver;
import hitech.robotic.systemzz.OBD.MyLog;
import hitech.robotic.systemzz.OBD.PairedDevicesDialog;
import hitech.robotic.systemzz.OBD.PairedListAdapter;
import hitech.robotic.systemzz.OBD.TripData;
import hitech.robotic.systemzz.OBD.TripDatabaseHandler;
import hitech.robotic.systemzz.csvReadWrite.CSVWriter;
import hitech.robotic.systemzz.fragments.GpsFragment;
import hitech.robotic.systemzz.retrofit.SmsInterface;
import hitech.robotic.systemzz.services.AlarmManagerBroadcastReceiver;
import hitech.robotic.systemzz.services.TrackingServices;
import hitech.robotic.systemzz.utils.Constants;
import hitech.robotic.systemzz.utils.PreferenceHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class CameraActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2, SensorEventListener,
        PairedDevicesDialog.PairedDeviceDialogListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    public static final int JAVA_DETECTOR = 0;
    public static final int REQUEST_AUDIO_PERMISSION_CODE = 1;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;
    private final static int WRITE_EXTERNAL_STORAGE_REQUEST = 1000;
    private static final Scalar FACE_RECT_COLOR = new Scalar(0, 255, 0, 255);
    private static final Scalar ROI_COLOR_YELLOW = new Scalar(255, 255, 0);
    private static final Scalar ROI_COLOR_RED = new Scalar(255, 0, 0);

    private static final int REQUEST_WRITE_STORAGE = 112;
    private static final String LOG_TAG = "AudioRecording";
    final static String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ClubD2S/";
    // location updates interval - 30sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 500;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private static String TAG = "MainActivity";
    private static String mFileName = null;
    private int hardBreakCounter = 0, overSpeedCounter = 0, gyroCounter = 0;
    private CopyOnWriteArrayList<Double> avgSpeedArray = new CopyOnWriteArrayList<>();
    private ArrayList<Double> allSpeedArray = new ArrayList<>();
    private ArrayList<Float> avgSpeedPointsList = new ArrayList<>();

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
   // private final BroadcastReceiver smsReceiver = new detectCalls();


    public enum NfeType {
        NFE_DISTANCE,
        NFE_ACCELEROMETER,
        NFE_SPEED;
    }

    float dist;

    //Bluetooth part
    private static final int REQUEST_ENABLE_BT = 101;
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 102;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 103;

    // Message types accessed from the BluetoothIOGateway Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    ArrayList<Double> speed_buffer = new ArrayList<Double>();
    boolean speed_track = false;
    boolean harshbreak = false;
    int harshbreak_timecount = 0;
    int speed_dist_nfetime_buffer = 0;

    private BluetoothIOGateway mIOGateway;
    private static BluetoothAdapter mBluetoothAdapter;
    private DeviceBroadcastReceiver mReceiver;
    private PairedDevicesDialog dialog;
    private List<BluetoothDevice> mDeviceList;

    private boolean inSimulatorMode = false;
    private static StringBuilder mSbCmdResp;
    private static StringBuilder mPartialResponse;
    private String mConnectedDeviceName, startTime, endTime, Beep;
    private static final String TAG_DIALOG = "dialog";
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast_message";
    private int mCMDPointer = -1;
    private static final String[] INIT_COMMANDS = {"AT Z", "AT SP 0", "0105", "010C", "010D", "0131"};
    private Handler mainHandler = new Handler();
    private volatile boolean stopThread = false;
    private ToneGenerator toneGenerator;
    private static int convertHex;
    private boolean flag, overSpeedHolderFlag = false;
    private boolean NFESTATE_FCW = false;
    private boolean NFESTATE_HB;
    String filename = "";
    int states = 0;
    int i;
    private double dPreviousSpeed, speedHolder = 0;
    private int HBThreshold;
    private double dDifference;
    private Sensor mOrientation;
    private String brakeEvent = " ", startingTime, OverSpeedLimit, gryoEvent = " ";
    private CountDownTimer countDownTimer, overSpeedTimer, incomingCallTimer;
    private boolean isTimerRunning = false, isOverSpeedTimerRunning = false, breakout = true,
            onstopFlag = false, onDestroyFlag = false, isGyroTimerRunning;

    //SharedPreferences sharedPreferences =getSharedPreferences("SETTING", Context.MODE_PRIVATE);

    //storage
    private String mSelectedSize;
    private long mConvertToByte;
    private long mFolderSize;
    private boolean mStorageFlag = true;
    private int fSpeed = 0;
    private List<String> mList;
    private long tLength = 0, startTimeInMills;
    private Storage mStorage;
    private static final int _DATA_X = 0;
    private static final int _DATA_Y = 1;
    private static final int _DATA_Z = 2;
    private int ORIENTATION_UNKNOWN = -1;
    public static final int UPSIDE_DOWN = 3;
    public static final int LANDSCAPE_RIGHT = 4;
    public static final int PORTRAIT = 1;
    public static final int LANDSCAPE_LEFT = 2;
    public int mOrientationDeg; //last rotation in degrees
    public int mOrientationRounded; //last orientation int from above
    private int tempOrientRounded = 0;
    LocationCallback locationCallback;
    ArrayList<Location> locations = new ArrayList<>();
    private Date tripStartDate, tripEndDate;

    int frameCounter = 0;


    //Variables added by Aquib
   /* private MyCountDownTimer myCountDownTimer;
    private long totalTime = 30 * 60 * 1000;
*/
    private LocalBroadcastManager local_broadcast_manager;
    private AlarmManagerBroadcastReceiver alarm;
    AmazonS3 s3;
    private Context ServiceContext;
    TransferUtility transferUtility;
    Uri videoUri;
    private final int SELECT_PICTURE = 2;
    private ArrayList<String> VideoListName;
    private ArrayList<String> TripListToUpload;
    private ArrayList<TripData> tripData;
    private ArrayList<String> videoListToUpload;
    private boolean fileUpload;
    private FirebaseFirestore firestore;
    long startTime1 = System.currentTimeMillis();

    //**************************************************************
    GoogleApiClient googleApiClient;

    private static final float NS2S = 1.0f / 1000000000.0f;
    private final float[] deltaRotationVector = new float[4];
    private float timestamp;

    /**
     *  Array list to keep values from Gyrometer. Using CopyOnWrite for handle concurrency.
     */
    CopyOnWriteArrayList<Double> oMagnitude = new CopyOnWriteArrayList<>();
    SensorManager sensorManager;
    Sensor sensor;
    SensorEventListener eventListener;


    private final Handler mMsgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothIOGateway.STATE_CONNECTING:
                            displayMessage(getString(R.string.BT_connecting));
                            break;

                        case BluetoothIOGateway.STATE_CONNECTED:
                            displayMessage(getString(R.string.BT_status_connected_to) + " " + mConnectedDeviceName);
                            //sendDefaultCommands();
                            starOBDLoop();
                            break;

                        case BluetoothIOGateway.STATE_LISTEN:
                        case BluetoothIOGateway.STATE_NONE:
                            //  displayMessage(getString(R.string.BT_status_not_connected));
                            break;

                        default:
                            break;
                    }
                    break;

                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;

                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    readMessage = readMessage.trim();
                    readMessage = readMessage.toUpperCase();
                    displayLog(mConnectedDeviceName + ": " + readMessage);
                    if (!inSimulatorMode) {
                        char lastChar = readMessage.charAt(readMessage.length() - 1);
                        if (lastChar == '>') {
                            parseResponse(mPartialResponse.toString() + readMessage);
                            mPartialResponse.setLength(0);
                        } else {
                            mPartialResponse.append(readMessage);
                        }
                    } else {
                        // first time execution
                        mSbCmdResp.append("R>>");
                        mSbCmdResp.append(readMessage);
                        mSbCmdResp.append("\n");
                        Log.d(TAG, " " + mSbCmdResp.toString());
                        // writeToFile(mSbCmdResp.toString());

                        // mMonitor.setText(mSbCmdResp.toString());
                    }
                    break;

                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;

                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    Log.d(TAG, "*** OUTGOING MSG : " + writeMessage);
                    displayLog("Me: " + writeMessage);
                    mSbCmdResp.append("W>>");
                    mSbCmdResp.append(writeMessage);
                    mSbCmdResp.append("\n");
                    // mMonitor.setText(mSbCmdResp.toString());
                    break;

                case MESSAGE_TOAST:
                    displayMessage(msg.getData().getString(TOAST));
                    break;

                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    break;
            }
        }

    };

    //bluetooth end

    static {
        if (OpenCVLoader.initDebug()) {
            Log.d(TAG, "static initializer: opencv Loaded successfully");
        } else {
            Log.d(TAG, "static initializer: opencv not Loaded");
        }
    }

    private final float NOISE = (float) 2.0;
    DecimalFormat df = new DecimalFormat("0.00");
    ImageView img_home, img_capture, img_settings, img_bluetooth, img_media;
    double accelX, accelY, accelZ;
    double linearAccel;
    double gyroX, gyroY, gyroZ, y;
    TextView speed, choose_unit;
    CameraBridgeViewBase mOpenCvCameraView;
    Mat mRgba;
    //for recording
    ImageView videoRecord, folder, settings;
    Handler handler = new Handler();
    Chronometer focus;
    boolean recording = false;
    int videoCount = 0;
    boolean state = false;
    VideoWriter mVideoWriter;
    String saveVideo = Environment.getExternalStorageDirectory().getAbsolutePath();
    FloatingActionButton fabUpload;
    ImageView mute, unmute, playAudio;
    double latitude[];
    double longitude[];
    double lat;
    double longi;
    double accel_speed;
    int height;
    int width;
    Long times[];
    double prev_speed = 0;
    double Speed = 0;
    String timeStamp;
    int videoDuration = 0;
    int nfeSpeed = 0;
    int cv_text_red = 0;
    int cv_text_green = 0;
    int cv_text_blue = 0;
    int recordTiming = 0;
    double int_fcw_min_dis = 0.0f;
    int int_fcw_speed = 0;
    int int_osw_speed = 0;
    int int_hb_speed;
    int int_hb_count = 0;
    int int_hb_threshold = 0;
    String sPeed_units = "", sPeed_type = "", cAmera_type = "", cv_textColor = "";
    ToneGenerator toneGen1, toneGen2;
    SensorEventListener gyroscopeSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            // More code goes here
            if (sensorEvent.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
                return;
            }
            gyroX = sensorEvent.values[0];
            gyroY = sensorEvent.values[1];
            gyroZ = sensorEvent.values[2];


        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    };
    private String mPath = Environment.
            getExternalStorageDirectory().toString() + "/ClubD2S";
    private int TripNum = 1;
    private String mLogFilename;
    //sensor
    private int accel_window_size = 15;
    ArrayList<Float> z_accel = new ArrayList<Float>();
    private double mLastX, mLastY, mLastZ;
    private float[] gravity = new float[3];
    private float[] linear_acceleration = new float[3];
    private boolean mInitialized;


    /**
     * Using accelerometer to get values of sensor.
     */
    SensorEventListener accelerometerListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];


            // Log.d("xyzvalue>", x + " > " + y + " > " + z);

            if (z_accel.size() < accel_window_size) {
                z_accel.add(z);
            } else {
                z_accel.remove(0);
                z_accel.add(z);
            }

            accelX = x;
            accelY = y;
            accelZ = z;
            final float alpha = 0.8f;

            /*gravity[0] = alpha * gravity[0] + (1 - alpha) * sensorEvent.values[0];
            gravity[1] = alpha * gravity[1] + (1 - alpha) * sensorEvent.values[1];
            gravity[2] = alpha * gravity[2] + (1 - alpha) * sensorEvent.values[2];

            linear_acceleration[0] = sensorEvent.values[0] - gravity[0];
            linear_acceleration[1] = sensorEvent.values[1] - gravity[1];
            linear_acceleration[2] = sensorEvent.values[2] - gravity[2]; */
            linear_acceleration = sensorEvent.values;
            linearAccel = Math.sqrt((linear_acceleration[0] * linear_acceleration[0]) +
                    (linear_acceleration[1] * linear_acceleration[1]) +
                    (linear_acceleration[2] * linear_acceleration[2]));
            //accel_speed = linearAccel*3.6 + prev_speed;
            //write to db

            //  Log.d("xyzvalue2>", linearAccel + "");

            if (!mInitialized) {
                mLastX = x;
                mLastY = y;
                mLastZ = z;
                mInitialized = true;
            } else {
                double deltaX = (mLastX - x);
                double deltaY = (mLastY - y);
                double deltaZ = (mLastZ - z);

                if (deltaX < NOISE) deltaX = (float) 0.0;
                if (deltaY < NOISE) deltaY = (float) 0.0;
                if (deltaZ < NOISE) deltaZ = (float) 0.0;

                if (z > 3) {
                    //Toast.makeText(getApplicationContext(),"Moving back: " + z, Toast.LENGTH_SHORT).show();
                    Log.d("Accel: ", "+ve accel: " + z_accel.get(0));
                }
                if (z < -3) {
                    Log.d("Accel: ", "-ve accel: " + z_accel.get(0));
                    //Toast.makeText(getApplicationContext(),"Moving forward: " + z, Toast.LENGTH_SHORT).show();
                }

                mLastX = x;
                mLastY = y;
                mLastZ = z;

                double sum = 0;
                float sumZ = 0;
                for (Double d : avgSpeedArray)
                    sum += d;

                for (Float k : z_accel)
                    sumZ += k;

                Double avgSpeed = sum / 10;
                float avgZ = sumZ / accel_window_size;


                // if (linearAccel > 5 && avgSpeed > 20) { //yahan bi 20 karna hai

                /**
                 * using avgZ value to handle hard break.
                 */
                if (avgZ < -3) {
                    Log.d("Hard break hit >>>", "hard break hit");
                    brakeEvent = "HARD BRAKE";
                    Log.d("xyzm ", " x > " + x + " y > " + y + " z > " + z + " > " + linearAccel + " > " + avgZ);
                    breakTimer();

                }
                //  }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    };
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mGyroscope;
    private int mDetectorType = JAVA_DETECTOR;
    private String[] mDetectorName;
    private float cameraFocalLength;
    private Mat mGray;
    private File mCascadeFile;
    private CascadeClassifier mJavaDetector;
    private float mRelativeFaceSize = 0.2f;
    private int mAbsoluteFaceSize = 0;
    private SensorManager SensorManage;
    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    //location
    private long mLastLocationUpdateTime;
    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient, mFLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    // define the compass picture that will be use
    private ImageView compassimage;
    // record the angle turned of the compass picture
    private float DegreeStart = 0f;
    private Mat bgr;
    private MatOfRect faces;
    private GPSTracker gpsTracker;
    private float[] mFocalLength;
    private BaseLoaderCallback mLoadercallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");


                    try {
                        // load cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.vehicle);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        mCascadeFile = new File(cascadeDir, "vehicle.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        if (mJavaDetector.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            mJavaDetector = null;
                        } else
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());

                        cascadeDir.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
            CameraManager manager = (CameraManager) getSystemService(CAMERA_SERVICE);
            try {
                Log.d(TAG, "OpenCV: Trying to get Focal lengths of camras");
                int cnt = 0;
                for (String cameraId : manager.getCameraIdList()) {
                    CameraCharacteristics characs = manager.getCameraCharacteristics(cameraId);
                    mFocalLength = characs.get(CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS);
                    Log.d(TAG, "OpenCV: Focal length:" + Arrays.toString(mFocalLength));
                    cnt++;
                }

            } catch (CameraAccessException e) {
                e.printStackTrace();
            }

        }
    };

    public CameraActivity() {
        mDetectorName = new String[1];
        mDetectorName[JAVA_DETECTOR] = "Java";
    }

    public String removeLastChar(String str) {
        if (str != null && str.length() > 0) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    public String removeFirstChar(String str) {
        if (str != null && str.length() > 0) {
            str = str.substring(1, str.length());
        }
        return str;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
       // GiraffeCompressor.init(this);
        getAbsolutePath();
       // compressVideoSize(getAbsolutePath());

        setContentView(R.layout.activity_camera);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        java.util.Date date = new java.util.Date();

        alarm = new AlarmManagerBroadcastReceiver();

        mStorage = new Storage(this);
        SharedPreferences sharedPreferences = getSharedPreferences("SETTING", Context.MODE_PRIVATE);
        NFESTATE_FCW = false;

        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());
        //File mFileName = new File(recordfilepath(), "AUD_" + timeStamp + ".avi");
        //mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName = recordfilepath();
        mFileName = mFileName + "AUD_" + timeStamp + ".3gp";
        mOpenCvCameraView = findViewById(R.id.fd_activity_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        videoRecord = findViewById(R.id.record_button);
        img_home = findViewById(R.id.img_home);
        img_bluetooth = findViewById(R.id.img_bluetooth);
        img_media = findViewById(R.id.img_media);
        img_capture = findViewById(R.id.img_capture);
        img_settings = findViewById(R.id.img_settings);
        speed = findViewById(R.id.speed);
        choose_unit = findViewById(R.id.choose_speed_unit);
        // folder=findViewById(R.id.folder);
        //fabUpload = findViewById(R.id.fab_upload);
        focus = (Chronometer) findViewById(R.id.video_time);
        mute = findViewById(R.id.mute);
        unmute = findViewById(R.id.unmute);
        playAudio = findViewById(R.id.playAudio);
        toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 50000);
        toneGen2 = new ToneGenerator(AudioManager.STREAM_MUSIC, 50000);

        //gpsTracker = new GPSTracker(this,CameraActivity.this);
        mFocalLength = new float[2];

        compassimage = (ImageView) findViewById(R.id.compass_image);
        // initialize your android device sensor capabilities
        SensorManage = (SensorManager) getSystemService(SENSOR_SERVICE);
        mList = new ArrayList<>();
        img_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBluetoothAdapter == null) {
                    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                }
                // make sure Bluetooth is enabled
                // displayLog("Try to check availability...");
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                } else {
                    // displayLog("Bluetooth is available");
                    queryPairedDevices();
                    setupMonitor();
                    starOBDLoop();
                }
            }
        });

        img_media.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent fileManager = new Intent(CameraActivity.this, FileManagerActivity.class);
                //startActivity(fileManager);
            }
        });
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CameraActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        img_capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (states == 0) {

                    //startSound();
                    Toast.makeText(CameraActivity.this, "FullBraking selected", Toast.LENGTH_SHORT).show();
                    states = 1;
                    //filename="FullBraking.txt";
                } else {
                    //stopThread = true;
                    Toast.makeText(CameraActivity.this, "HalfBraking selected", Toast.LENGTH_SHORT).show();
                    //filename="Halfbraking.txt";
                    states = 0;
                }
                /*Dexter.withActivity(CameraActivity.this)
                        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //screenShot();
                                        final Date now = new Date();
                                        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
                                        String imageFileName = "/Horus Adas/" + now + "_" + ".jpg";
                                        String filepath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Horus Adas/images";
                                        Log.d(TAG, "AWS : Filename : " + filepath + imageFileName);
                                        File file = new File(filepath, imageFileName);
                                        Boolean bool = Imgcodecs.imwrite(filepath + imageFileName, mRgba);
                                        if (bool) {
                                            String path = removeLastChar(filepath);
                                            String name = removeFirstChar(imageFileName);
                                            Log.d(TAG, "path : " + path + " filename : " + name);
                                            //uploadWithTransferUtility(path,name);
                                            Log.d(TAG, "AWS: SUCCESS writing image to external storage");
                                        }
                                        else
                                            Log.d(TAG, "AWS: Fail writing image to external storage");

                                    }
                                }, 1000);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                            }
                        }).check();
*/
            }
        });

        /**
         * To go to setting of camera and trip.
         */
        img_settings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(CameraActivity.this, SettingsActivity.class));
            }
        });


        String Auto_Video = sharedPreferences.getString("AUTO_VIDEO_RECORD_STATE", "");

        if (Auto_Video.equals("ON")) {
            //  Autorec();
        }


        //    mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.fd_activity_surface_view);

        init();

        handler.post(new Runnable() {
            @Override
            public void run() {
                recordTiming = recordTiming + 1;
                handler.postDelayed(this, 1000);
            }
        });

        /*fabUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //upload file to s3 bucket
              //  String Phnum = DBHandlerWalk.Instance(getApplicationContext()).getPhoneNum();
                if (mRgba != null)
                {
                    if (ContextCompat.checkSelfPermission(CameraActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    {
                        ActivityCompat.requestPermissions(
                                CameraActivity.this,
                                new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                                REQUEST_WRITE_STORAGE );
                    }
                    Log.d(TAG, "Saving image to " + recordfilepath());
                    String mPath = recordfilepath();
                    File directory = new File(mPath);
                    if (!(directory.exists())) {
                        Log.d(TAG, "Creating new dir!");
                        new File(mPath).mkdirs();
                    }
                    else {
                        Log.d(TAG, "Directory already exists.");
                    }
                    final Date date = new Date();
                    SimpleDateFormat Formater = new SimpleDateFormat("yyyyMMddd_hhmmss");
                    String datestr = Formater.format(date);
                    String filename = *//*Phnum+*//*"_"+datestr+".jpg";
                    Boolean b = Imgcodecs.imwrite(mPath+"/"+filename,mRgba);
                    Toast.makeText(getApplicationContext(), "File saved to: " + mPath, Toast.LENGTH_LONG).show();
                    Log.d(TAG,"Saved file to " + mPath + " ret : " + b);
                    uploadWithTransferUtility(mPath,filename);
                }
            }
        });


*/

        String Auto_Audio = sharedPreferences.getString("AUTO_AUDIO_RECORD_STATE", "");

        if (Auto_Audio.equals("ON")) {
            recordAudio();
        }
        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //unmute.setVisibility(View.VISIBLE);
                //mute.setVisibility(View.GONE);
                //muteAudio();
            }
        });

        playAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRecordedAudio();
            }
        });
        unmute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recordAudio();
                mute.setVisibility(View.VISIBLE);
                unmute.setVisibility(View.GONE);

            }
        });
       /* videoRecord.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                long lConvert = Long.parseLong(mSelectedSize);
                int mConvert = 1024*1024*1024;
                mConvertToByte = lConvert*mConvert;
                Log.d(TAG, "onClick: mConvertToByte : "+mConvertToByte+ " mFolderSize    :" + mFolderSize);
                // Toast.makeText(getApplicationContext(),"mConvertToByte : "+mConvertToByte,Toast.LENGTH_LONG).show();
                if (mFolderSize<mConvertToByte)
                {
                    mStorageFlag = true;
                    if (recording) {
                        img_settings.setEnabled(true);
                        //StopAudioRec();
                        videoRecord.setImageResource(R.drawable.record);
                        recording = false;
                        focus.stop();
                        mVideoWriter.release();
                        Toast.makeText(CameraActivity.this, "Stop recording", Toast.LENGTH_SHORT).show();
                    } else {
                        //recordAudio();
                        img_settings.setEnabled(false);
                        videoRecord.setImageResource(R.drawable.record_red);
                        recording = true;
                        focus.setBase(SystemClock.elapsedRealtime()); // reset the counter to 0
                        focus.start();
                        recordfilepath();
                        java.util.Date date = new java.util.Date();
                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());
                        File file = new File(recordfilepath(), "VID_" + timeStamp + ".avi");
                        Log.d(TAG, "file : " + file);
                        try {
                            if (!file.exists()) {
                                file.createNewFile();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        saveVideo = file.getAbsolutePath();
                        Toast.makeText(CameraActivity.this, "Recording", Toast.LENGTH_SHORT).show();
                    }
                } else{
                    Log.d(TAG, "onClick: Limit Exceed!");
                    Toast.makeText(getApplicationContext(), "Limit Exceed!", Toast.LENGTH_SHORT).show();
                }

            }
        });*/
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        //  replaceFragment();
        mOpenCvCameraView.setVisibility(CameraBridgeViewBase.VISIBLE);
        mOpenCvCameraView.setMaxFrameSize(720, 480);//1280x720 720x480
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorManager.registerListener(accelerometerListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        mSensorManager.registerListener(gyroscopeSensorListener, mGyroscope, SensorManager.SENSOR_DELAY_NORMAL);

        // Bluetooth part
        mSbCmdResp = new StringBuilder();
        mPartialResponse = new StringBuilder();
        mIOGateway = new BluetoothIOGateway(this, mMsgHandler);

        videoRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTrip();
                //  calculateDistance();
                // startService();

            }
        });


        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        startingTime = timeFormat.format(Calendar.getInstance().getTime());

        startTimeInMills = System.currentTimeMillis();


        makeSensorsOnByDefault();
        checkCameraOrientation();

        tripStartDate = new Date();

        checkGyroscope();

    }


    public void openfiles() {
        File fileDirectory = new File(Environment.getExternalStorageDirectory() + "/ClubD2s");
        File[] dirFiles = fileDirectory.listFiles();
        if (dirFiles.length != 0) {
            // loops through the array of files, outputing the name to console
            for (int ii = 0; ii < dirFiles.length; ii++) {
                String fileOutput = dirFiles[ii].toString();
                System.out.println(fileOutput);
            }
        }
       /* for (int i = 0; i < dirFiles.length; i++) {
            //here populate your listview
            Log.d("Files", "FileName:" + dirFiles[i].getName());9

        }*/
    }

    public void StopAudioRec() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        // Toast.makeText(getApplicationContext(), "Recording Stopped", Toast.LENGTH_LONG).show();
    }

    public void showRecordedAudio() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
            Toast.makeText(getApplicationContext(), "Recording Started Playing", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    public void recordAudio() {
        // if (checkAndRequestPermissions()) {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile(mFileName);
        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
        mRecorder.start();
        // Toast.makeText(getApplicationContext(), "Recording Started", Toast.LENGTH_LONG).show();
        // } else {
        RequestPermissions();
        // }
    }

    private void RequestPermissions() {
        ActivityCompat.requestPermissions(CameraActivity.this, new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, REQUEST_AUDIO_PERMISSION_CODE);
    }

    public void Autorec() {
        //  img_settings.setEnabled(false);
        recording = true;
        focus.setBase(SystemClock.elapsedRealtime()); // reset the counter to 0
        // videoRecord.setPressed(true);
        focus.start();
        recordfilepath();
        java.util.Date date = new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());
        File file = new File(recordfilepath(), "VID_" + timeStamp + ".avi");
        Log.d(TAG, "file : " + file);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        saveVideo = file.getAbsolutePath();
        Toast.makeText(CameraActivity.this, "Recording", Toast.LENGTH_SHORT).show();
    }

    /**
     *This methods set the path location of videos.
     */
    private String recordfilepath() {

        File sddir = Environment.getExternalStorageDirectory();
        File vrdir = new File(sddir, "ClubD2s/Videos");
        if (!vrdir.exists()) {
            vrdir.mkdir();
        }
        String filepath = vrdir.getAbsolutePath();
        return filepath;
    }

    private String Imagesrecordfilepath() {

        File sddir = Environment.getExternalStorageDirectory();
        File vrdir = new File(sddir, "ClubD2s/Images");
        if (!vrdir.exists()) {
            vrdir.mkdir();
        }
        String filepath = vrdir.getAbsolutePath();
        return filepath;
    }


    private String logrecordfilepath() {

        File sddir = Environment.getExternalStorageDirectory();
        File vrdir = new File(sddir, "ClubD2s/Logs");
        if (!vrdir.exists()) {
            vrdir.mkdir();
        }
        String filepath = vrdir.getAbsolutePath();
        return filepath;
    }

    @Override
    protected void onPause() {
        super.onPause();
       /* EventBus.getDefault().unregister(this);
        SensorManage.unregisterListener((SensorEventListener) this);
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
            mSensorManager.unregisterListener(accelerometerListener);
            mSensorManager.unregisterListener(gyroscopeSensorListener);
        }
        mFLocationClient.removeLocationUpdates(locationCallback);
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        Log.d("onStopFlag >>> ", onstopFlag + " ");

        sensorManager.unregisterListener(eventListener);*/

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
        // Un register receiver
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
        // Stop scanning if is in progress
        //cancelScanning();
        // Stop mIOGateway
        if (mIOGateway != null) {
            mIOGateway.stop();
        }
        // Clear StringBuilder
        if (mSbCmdResp.length() > 0) {
            mSbCmdResp.setLength(0);
        }
        stopThread = true;
        mStorageFlag = false;

        if (onDestroyFlag) {
            PreferenceHelper.cleanPerticualrPref(this, "tripId");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        startLocationUpdates();
        getAccelerometerUpdates(1000);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        OverSpeedLimit = String.valueOf(sharedPreferences.getInt("SEEKBAR_VALUE", 60));
        String speed_units = sharedPreferences.getString(getString(R.string.speed_Units), "0");
        String speed_type = sharedPreferences.getString(getString(R.string.speed_type), "0");
        String camera_type = sharedPreferences.getString(getString(R.string.camera_type), "0");
        String nfeSpeed_type = sharedPreferences.getString(getString(R.string.nfeSpeed_type), "0");
        String cvColor_type = sharedPreferences.getString(getString(R.string.cvColor_type), "0");
        String hb_thresholdt = sharedPreferences.getString(getString(R.string.hb_threshold), "0");
        String storage_limit = sharedPreferences.getString(getString(R.string.storage_limit), "0");


        /**
         * You can set storage limit for videos.
         */
        if (storage_limit.equals("0")) {
            mSelectedSize = "1";
        }
        if (storage_limit.equals("1")) {
            mSelectedSize = "2";
        }
        if (storage_limit.equals("2")) {
            mSelectedSize = "3";
        }
        if (storage_limit.equals("3")) {
            mSelectedSize = "4";
        }
        if (storage_limit.equals("4")) {
            mSelectedSize = "5";
        }
        if (storage_limit.equals("5")) {
            mSelectedSize = "6";
        }

        //Toast.makeText(getApplicationContext(),"storage_limit : "+mSelectedSize,Toast.LENGTH_LONG).show();
        runLooperStorage();
        if (hb_thresholdt.equals("0")) {
            int_hb_threshold = 9;
        }
        if (hb_thresholdt.equals("1")) {
            int_hb_threshold = 10;
        }
        if (hb_thresholdt.equals("2")) {
            int_hb_threshold = 11;
        }
        if (hb_thresholdt.equals("3")) {
            int_hb_threshold = 12;
        }


        if (camera_type.equals("0")) {
            cAmera_type = "BACK_CAMERA";
        }
        if (camera_type.equals("1")) {
            cAmera_type = "RTSP";
        }
        //Toast.makeText(this, "camera type :"+cAmera_type, Toast.LENGTH_SHORT).show();

        if (camera_type.equals("0")) {
            sPeed_type = "GPS";
        }
        if (speed_type.equals("1")) {
            sPeed_type = "Accelerometer";

        }
        if (speed_type.equals("2")) {
            sPeed_type = "Sensor";
        }
        if (speed_type.equals("3")) {
            img_bluetooth.setVisibility(View.VISIBLE);
            sPeed_type = "OBD";
        }

        /**
         * Here we select speed unit.
         */
        if (speed_units.equals("0")) {
            sPeed_units = "km/hr";
        }
        if (speed_units.equals("1")) {
            sPeed_units = "mph";

        }
        /*if (fragmentDuration.equals("0")){
            videoDuration=60;
        }
        if (fragmentDuration.equals("1")){
            videoDuration=120;
        }
        if (fragmentDuration.equals("2")){
            videoDuration=180;
        }
        if (fragmentDuration.equals("3")){
            videoDuration=240;
        }
        if (fragmentDuration.equals("4")){
            videoDuration=300;
        }*/
        //Toast.makeText(getApplicationContext(),"video duration : " +videoDuration, Toast.LENGTH_SHORT).show();

        if (nfeSpeed_type.equals("0")) {
            nfeSpeed = 10;
            //Toast.makeText(getApplicationContext(),"nfe speed : " +nfeSpeed, Toast.LENGTH_SHORT).show();
        }
        if (nfeSpeed_type.equals("1")) {
            nfeSpeed = 20;
            //Toast.makeText(getApplicationContext(), "nfe speed : " + nfeSpeed, Toast.LENGTH_SHORT).show();
        }
        if (nfeSpeed_type.equals("2")) {
            nfeSpeed = 30;
            //Toast.makeText(getApplicationContext(),"nfe speed : " +nfeSpeed, Toast.LENGTH_SHORT).show();

        }
        if (nfeSpeed_type.equals("3")) {
            nfeSpeed = 40;
            //Toast.makeText(getApplicationContext(),"nfe speed : " +nfeSpeed, Toast.LENGTH_SHORT).show();

        }
        if (nfeSpeed_type.equals("4")) {
            nfeSpeed = 50;
            //Toast.makeText(getApplicationContext(),"nfe speed : " +nfeSpeed, Toast.LENGTH_SHORT).show();

        }
        if (nfeSpeed_type.equals("5")) {
            nfeSpeed = 60;
            //Toast.makeText(getApplicationContext(),"nfe speed : " +nfeSpeed, Toast.LENGTH_SHORT).show();
        }

        if (cvColor_type.equals("0")) {
            cv_textColor = "red";

        }
        if (cvColor_type.equals("1")) {
            cv_textColor = "green";

        }
        if (cvColor_type.equals("2")) {
            cv_textColor = "blue";

        }
        //Toast.makeText(this, "color :"+cv_textColor, Toast.LENGTH_SHORT).show();

        //Toast.makeText(getApplicationContext(),"nfe speed : " +nfeSpeed, Toast.LENGTH_SHORT).show();

        /*if(fcw_min_dis.equals("0")){
            int_fcw_min_dis=5.0f;
        }
        if(fcw_min_dis.equals("1")){
            int_fcw_min_dis=10.0f;
        }
        if(fcw_min_dis.equals("2")){
            int_fcw_min_dis=15.0f;
        }
        if(fcw_min_dis.equals("3")){
            int_fcw_min_dis=20.0f;
        }
        //Toast.makeText(this, "distance :"+int_fcw_min_dis, Toast.LENGTH_SHORT).show();

        if(fcw_speed.equals("0")){
            int_fcw_speed=30;
        }
        if(fcw_speed.equals("1")){
            int_fcw_speed=40;
        }
        if(fcw_speed.equals("2")){
            int_fcw_speed=50;
        }
        if(fcw_speed.equals("3")){
            int_fcw_speed=60;
        }
        //Toast.makeText(getApplicationContext(),"fcw speed : " +int_fcw_speed, Toast.LENGTH_SHORT).show();

        if(osw_speed.equals("0")){
            int_osw_speed=30;
        }
        if(osw_speed.equals("1")){
            int_osw_speed=40;
        }
        if(osw_speed.equals("2")){
            int_osw_speed=50;
        }
        if(osw_speed.equals("3")){
            int_osw_speed=60;
        }
        if(osw_speed.equals("4")){
            int_osw_speed=70;
        }
        if(osw_speed.equals("5")){
            int_osw_speed=80;
        }
        if(osw_speed.equals("6")){
            int_osw_speed=100;
        }
        if(osw_speed.equals("7")){
            int_osw_speed=120;
        }
        if(osw_speed.equals("8")){
            int_osw_speed=140;
        }
        if(osw_speed.equals("9")){
            int_osw_speed=160;
        }

        //Toast.makeText(getApplicationContext(),"os speed : " +int_osw_speed, Toast.LENGTH_SHORT).show();

        if(hb_speed.equals("0")){
            int_hb_speed=30;
        }
        if(fcw_speed.equals("1")){
            int_hb_speed=40;
        }
        if(fcw_speed.equals("2")){
            int_hb_speed=50;
        }
        if(fcw_speed.equals("3")){
            int_hb_speed=60;
        }
        //Toast.makeText(getApplicationContext(),"hb speed : " +int_hb_speed, Toast.LENGTH_SHORT).show();

        if(hb_count.equals("0")){
            int_hb_count=2;
        }
        if(fcw_speed.equals("1")){
            int_hb_count=3;
        }
        if(fcw_speed.equals("2")){
            int_hb_count=4;
        }
        if(fcw_speed.equals("3")){
            int_hb_count=5;
        }*/
        //Toast.makeText(getApplicationContext(),"hb count : " +int_hb_count, Toast.LENGTH_SHORT).show();

        SensorManage.registerListener((SensorEventListener) this, SensorManage.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoadercallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoadercallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        if (Build.VERSION.SDK_INT < 23) {
            //Do not need to check the permission
        } else {
            // if (checkAndRequestPermissions()) {
            //If you have already permitted the permission
            /**
             * Starting Video Recording Here.
             */
            startVideoRecording();
            /**
             * Enable Gps here.
             */
            enableGPS();
            //  }
        }
        /**
         * Registering sensors
         */
        mSensorManager.registerListener(accelerometerListener, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(gyroscopeSensorListener, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);

        // Start camera recording as soon as dashcam starts


        if (!checkPlayServices()) {
            Toast.makeText(CameraActivity.this, "You need to install google play services to use app properly", Toast.LENGTH_SHORT).show();
        }

        /**
         * This method calculate total distance.
         */
        calculateTotalTravelDistance();
        /**
         * This service upload videos on AWS in background.
         */
        startServiceAndAlarmManagerVideoUpload();


        try {
           /* TelephonyManager telephonyManager = (TelephonyManager) CameraActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
            Class clazz = Class.forName(telephonyManager.getClass().getName());
            Method method = clazz.getDeclaredMethod("getITelephony");
            method.setAccessible(true);
            ITelephony telephonyService = (ITelephony) method.invoke(telephonyManager);
            telephonyService.endCall();*/


           /* TelecomManager tm = (TelecomManager) this.getSystemService(Context.TELECOM_SERVICE);

            if (tm != null) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ANSWER_PHONE_CALLS) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                boolean success = tm.endCall();
                // success == true if call was terminated.
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }

/*
        try {
            declinePhone(this);
        } catch (Exception e) {
            e.printStackTrace();
        }*/


       /* IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PHONE_STATE");
        filter.addAction("android.intent.action.NEW_OUTGOING_CALL");

        registerReceiver(smsReceiver, filter);*/

        //   new detectCalls();


        firestore = FirebaseFirestore.getInstance();

        //  getAbsolutePath();


    }

    //Bluetooth Implementation
    private void setupMonitor() {
        // Start mIOGateway
        if (mIOGateway == null) {
            mIOGateway = new BluetoothIOGateway(this, mMsgHandler);
        }

        // Only if the state is STATE_NONE, do we know that we haven't started already
        if (mIOGateway.getState() == BluetoothIOGateway.STATE_NONE) {
            // Start the Bluetooth chat services
            mIOGateway.start();
        }

        // clear string builder if contains data
        if (mSbCmdResp.length() > 0) {
            mSbCmdResp.setLength(0);
        }

    }

    private void queryPairedDevices() {
        // displayLog("Try to query paired devices...");

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            PairedDevicesDialog dialog = new PairedDevicesDialog();
            dialog.setAdapter(new PairedListAdapter(this, pairedDevices), false);
            showChooserDialog(dialog);
        } else {
            //displayLog("No paired device found");
            scanAroundDevices();
        }
    }

    private void starOBDLoop() {
        ExampleRunnable runnable = new ExampleRunnable(2);
        new Thread(runnable).start();
    }

    private void startSound() {
        stopThread = false;
        PlaySoundRunnable runnable = new PlaySoundRunnable(1);
        new Thread(runnable).start();
    }


    class ExampleRunnable implements Runnable {
        int seconds;

        ExampleRunnable(int seconds) {
            this.seconds = seconds;
        }

        @Override
        public void run() {
            for (; ; ) {
                if (stopThread) {
                    return;
                }
                // playSound();
                sendOBDRequest();
                displayLog("Sending OBD Message");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class PlaySoundRunnable implements Runnable {
        int seconds;

        PlaySoundRunnable(int seconds) {
            this.seconds = seconds;
        }

        @Override
        public void run() {
            for (; ; ) {
                if (stopThread) {
                    return;
                }
                playSound();
                displayLog("Sending OBD Message");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void playSound() {
        try {
            if (toneGenerator == null) {
                toneGenerator = new ToneGenerator(AudioManager.STREAM_MUSIC, 50000);
            }
            // toneGenerator.startTone(ToneGenerator.TONE_CDMA_PIP, 200);
            toneGenerator.startTone(AudioManager.STREAM_MUSIC, 50000);
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (toneGenerator != null) {
                        Log.d(TAG, "ToneGenerator released");
                        toneGenerator.release();
                        toneGenerator = null;
                    }
                }

            }, 100);
        } catch (Exception e) {
            Log.d(TAG, "Exception while playing sound:" + e);
        }
    }


    private void startHandlerOBDReq() {
        try {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            }, 200);
        } catch (Exception e) {
            Log.d(TAG, "Exception while playing sound:" + e);
        }
    }

    private void showChooserDialog(DialogFragment dialogFragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag(TAG_DIALOG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        dialogFragment.show(ft, "dialog");
    }

    private void scanAroundDevices() {
        //displayLog("Try to scan around devices...");

        if (mReceiver == null) {
            // Register the BroadcastReceiver
            mReceiver = new DeviceBroadcastReceiver();
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mReceiver, filter);
        }

        // Start scanning
        mBluetoothAdapter.startDiscovery();
    }

    private void cancelScanning() {
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();

            //displayLog("Scanning canceled.");
        }
    }

    /**
     * Callback method for once a new device detected.
     *
     * @param device BluetoothDevice
     */
    public void onEvent(BluetoothDevice device) {
        if (mDeviceList == null) {
            mDeviceList = new ArrayList<>(10);
        }

        mDeviceList.add(device);

        // create dialog
        final Fragment fragment = this.getSupportFragmentManager().findFragmentByTag(TAG_DIALOG);
        if (fragment != null && fragment instanceof PairedDevicesDialog) {
            PairedListAdapter adapter = dialog.getAdapter();
            adapter.notifyDataSetChanged();
        } else {
            dialog = new PairedDevicesDialog();
            dialog.setAdapter(new PairedListAdapter(this, new HashSet<>(mDeviceList)), true);
            showChooserDialog(dialog);
        }
    }


    private void displayMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    private void displayLog(String msg) {
        Log.d(TAG, msg);
    }

    @Override
    public void onDeviceSelected(BluetoothDevice device) {
        cancelScanning();

        displayLog("Selected device: " + device.getName() + " (" + device.getAddress() + ")");

        // Attempt to connect to the device
        mIOGateway.connect(device, true);
    }

    @Override
    public void onSearchAroundDevicesRequested() {
        scanAroundDevices();
    }

    @Override
    public void onCancelScanningRequested() {
        cancelScanning();
    }

    private void parseResponse(String buffer) {
        switch (mCMDPointer) {
            case 0: // CMD: AT Z, no parse needed
            case 1: // CMD: AT SP 0, no parse needed
                mSbCmdResp.append("R>>");
                mSbCmdResp.append(buffer);
                mSbCmdResp.append("\n");
                break;

            case 2: // CMD: 0105, Engine coolant temperature
                int ect = showEngineCoolantTemperature(buffer);
                mSbCmdResp.append("R>>");
                mSbCmdResp.append(buffer);
                mSbCmdResp.append(" (Eng. Coolant Temp is ");
                mSbCmdResp.append(ect);
                mSbCmdResp.append((char) 0x00B0);
                mSbCmdResp.append("C)");
                mSbCmdResp.append("\n");
                break;

            case 3: // CMD: 010C, EngineRPM
                int eRPM = showEngineRPM(buffer);
                mSbCmdResp.append("R>>");
                mSbCmdResp.append(buffer);
                mSbCmdResp.append(" (Eng. RPM: ");
                mSbCmdResp.append(eRPM);
                mSbCmdResp.append(")");
                mSbCmdResp.append("\n");
                break;

            case 4: // CMD: 010D, Vehicle Speed
                int vs = showVehicleSpeed(buffer);

                mSbCmdResp.append("R>>");
                mSbCmdResp.append(buffer);
                mSbCmdResp.append(" (Vehicle Speed: ");
                mSbCmdResp.append(vs);
                mSbCmdResp.append("Km/h)");
                mSbCmdResp.append("\n");
                break;

            case 5: // CMD: 0131
                int dt = showDistanceTraveled(buffer);
                mSbCmdResp.append("R>>");
                mSbCmdResp.append(buffer);
                mSbCmdResp.append(" (Distance traveled since codes cleared: ");
                mSbCmdResp.append(dt);
                mSbCmdResp.append("Km)");
                mSbCmdResp.append("\n");
                break;

            default:
                mSbCmdResp.append("R>>");
                mSbCmdResp.append(buffer);
                mSbCmdResp.append("\n");
        }
        String str = buffer;
        String substring = str.substring(Math.max(str.length() - 4, 0));
        System.out.println("substring : " + substring);
        String[] parts = substring.split(">");
        String convertedString = parts[0].trim();
        convertHex = Integer.parseInt(convertedString, 16);
        writeToFile(String.valueOf(convertHex));
        System.out.println("convertedString : " + convertedString);
        Log.d(TAG, "parseResponse: Speed :" + convertedString);
        if (mCMDPointer >= 0) {
            mCMDPointer++;
            sendDefaultCommands();
        }
    }

    public void sendOBDRequest() {
        String command = "010D";
        sendOBD2CMD(command);
    }

    private void sendOBD2CMD(String sendMsg) {
        if (mIOGateway.getState() != BluetoothIOGateway.STATE_CONNECTED) {
            displayMessage(getString(R.string.bt_not_available));
            return;
        }

        String strCMD = sendMsg;
        strCMD += '\r';

        byte[] byteCMD = strCMD.getBytes();
        mIOGateway.write(byteCMD);
    }

    private String cleanResponse(String text) {
        text = text.trim();
        text = text.replace("\t", "");
        text = text.replace(" ", "");
        text = text.replace(">", "");

        return text;
    }

    private int showEngineCoolantTemperature(String buffer) {
        String buf = buffer;
        buf = cleanResponse(buf);

        if (buf.contains("4105")) {
            try {
                buf = buf.substring(buf.indexOf("4105"));

                String temp = buf.substring(4, 6);
                int A = Integer.valueOf(temp, 16);
                A -= 40;

                return A;
            } catch (IndexOutOfBoundsException | NumberFormatException e) {
                MyLog.e(TAG, e.getMessage());
            }
        }

        return -1;
    }

    private int showEngineRPM(String buffer) {
        String buf = buffer;
        buf = cleanResponse(buf);

        if (buf.contains("410C")) {
            try {
                buf = buf.substring(buf.indexOf("410C"));

                String MSB = buf.substring(4, 6);
                String LSB = buf.substring(6, 8);
                int A = Integer.valueOf(MSB, 16);
                int B = Integer.valueOf(LSB, 16);

                return ((A * 256) + B) / 4;
            } catch (IndexOutOfBoundsException | NumberFormatException e) {
                MyLog.e(TAG, e.getMessage());
            }
        }

        return -1;
    }

    private int showVehicleSpeed(String buffer) {
        String buf = buffer;
        buf = cleanResponse(buf);

        if (buf.contains("410D")) {
            try {
                buf = buf.substring(buf.indexOf("410D"));

                String temp = buf.substring(4, 6);

                return Integer.valueOf(temp, 16);
            } catch (IndexOutOfBoundsException | NumberFormatException e) {
                MyLog.e(TAG, e.getMessage());
            }
        }

        return -1;
    }

    private int showDistanceTraveled(String buffer) {
        String buf = buffer;
        buf = cleanResponse(buf);

        if (buf.contains("4131")) {
            try {
                buf = buf.substring(buf.indexOf("4131"));

                String MSB = buf.substring(4, 6);
                String LSB = buf.substring(6, 8);
                int A = Integer.valueOf(MSB, 16);
                int B = Integer.valueOf(LSB, 16);

                return (A * 256) + B;
            } catch (IndexOutOfBoundsException | NumberFormatException e) {
                MyLog.e(TAG, e.getMessage());
            }
        }

        return -1;
    }


    private void sendDefaultCommands() {
        flag = true;
        if (inSimulatorMode) {
            displayMessage("You are in simulator mode!");
            return;
        }

        if (mCMDPointer >= INIT_COMMANDS.length) {
            mCMDPointer = -1;
            return;
        }

        // reset pointer
        if (mCMDPointer < 0) {
            mCMDPointer = 0;
        }

        sendOBD2CMD(INIT_COMMANDS[mCMDPointer]);
    }

    private void writeToFile(String data) {
        //writing to log file
        String fn = recordfilepath() + "/" + getLogFilename();
        try {
            FileOutputStream fos = new FileOutputStream(fn, true);
            fos.write(data.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //End of Bluetooth


    /**
     * This method set the video format and create video of 30 seconds using videowriter.
     */
    public void recording(Mat bgr) {
        if (recording) {
            if (mVideoWriter == null) {
                mVideoWriter = new VideoWriter(saveVideo, VideoWriter.fourcc('M', 'J', 'P', 'G'), 9, bgr.size());
                mVideoWriter.open(saveVideo, VideoWriter.fourcc('M', 'J', 'P', 'G'), 9, bgr.size());
            }
            if (!mVideoWriter.isOpened()) {
                mVideoWriter.open(saveVideo, VideoWriter.fourcc('M', 'J', 'P', 'G'), 9, bgr.size());
            }

            mVideoWriter.write(bgr);

            this.bgr = bgr;



           /* double sum = 0;
            for (Double d : avgSpeedArray)
                sum += d;
            Double avgSpeed = sum / 10;

            // if (avgSpeed > -1) { // 20 karna hai*/

            if (recordTiming >= 30) {
                recordTiming = 0;
                getAbsolutePath();
               // compressVideoSize(saveVideo);
                Log.d("recordingTime >>>2", recordTiming + "");
                recording = false;
            }
        } else {
            if (mVideoWriter != null) {
                mVideoWriter.release();
                mVideoWriter = null;
                recording = true;
              //  compressVideoSize(saveVideo);
            }
        }
    }


    public void visibilities() {


    }

    /*private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        int permissionCLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionFLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (permissionCLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (permissionFLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (result != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (result1 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_ACCOUNTS);
            return false;
        }

        return true;
    }*/

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//check the orientation ofth e screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    //Permission Granted Successfully. Write working code here.
                } else {
                    //You did not accept the request can not use the functionality.
                }
                break;
        }
    }

    /**
     * This method create CSV file.
     * @param data
     * @param fileName
     * @return
     */
    public static boolean saveToFile(String data, String fileName) {
        try {
            new File(path).mkdir();
            File file = new File(path + fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
            String format = simpleDateFormat.format(new Date());
            String sData = format + ": " + data;
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            fileOutputStream.write((sData + System.getProperty("line.separator")).getBytes());

            return true;
        } catch (FileNotFoundException ex) {
            Log.d(TAG, ex.getMessage());
        } catch (IOException ex) {
            Log.d(TAG, ex.getMessage());
        }
        return false;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        faces = new MatOfRect();
        bgr = new Mat();
        mRgba = new Mat(height, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
        faces.release();
        bgr.release();
    }

    /**
     * This method runs according to camera frame rate and we do most of live camera related
     * calculation inside this method.
     * @param inputFrame
     * @return
     */
    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String time = mdformat.format(calendar.getTime());
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();
       /* Point p1=new Point(0,mRgba.height());
        Point p2=new Point(mRgba.width(),mRgba.height());
        Point p3=new Point((int)mRgba.width()/4,(int)mRgba.height()/2);
        Point p4=new Point((int)mRgba.width()*0.75,(int)mRgba.height()/2);
        Point p5=new Point(mRgba.width()/4,mRgba.height()*0.75);
        Point p6=new Point(mRgba.width()*0.75,mRgba.height()*0.75);
*/
        //draw red roi
     /*   Imgproc.line(mRgba,p1,p5,ROI_COLOR_RED,2);
        Imgproc.line(mRgba,p5,p6,ROI_COLOR_RED,2);
        Imgproc.line(mRgba,p6,p2,ROI_COLOR_RED,2);

        //draw yellow roi
        Imgproc.line(mRgba,p1,p3,ROI_COLOR_YELLOW,2);
        Imgproc.line(mRgba,p3,p4,ROI_COLOR_YELLOW,2);
        Imgproc.line(mRgba,p4,p2,ROI_COLOR_YELLOW,2);

*/
        //bgr = new Mat();
        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
        }

        //MatOfRect faces = new MatOfRect();
        SharedPreferences sharedPreferences = getSharedPreferences("SETTING", Context.MODE_PRIVATE);
        String Accelerometer = sharedPreferences.getString("ACCELERO_STATE", "");
        String Gyro = sharedPreferences.getString("GYRO_STATE", "");
        Beep = sharedPreferences.getString("BEEP_UPLOAD", "");
        String HbAcc = sharedPreferences.getString("HB_ACC", "");
        String SDetect = sharedPreferences.getString("DETECT_STATE", "");
        // String OswNFE=sharedPreferences.getString("OS_NFE","");
        // String HbNFE=sharedPreferences.getString("HB_NFE","");


        if (cv_textColor.equals("red")) {
            cv_text_red = 255;
            cv_text_green = 0;
            cv_text_blue = 0;
        }
        if (cv_textColor.equals("green")) {
            cv_text_red = 0;
            cv_text_green = 255;
            cv_text_blue = 0;
        }
        if (cv_textColor.equals("blue")) {
            cv_text_red = 0;
            cv_text_green = 0;
            cv_text_blue = 255;
        }

        if (Accelerometer.equals("ON")) {
//            Imgproc.putText(mRgba, "Acceleration: "+df.format(linearAccel)+" m/s2", new Point(720/2, 110), 1, 1.5, new Scalar(cv_text_red,cv_text_green,cv_text_blue));

            // Imgproc.putText(mRgba, "Accel speed: "+df.format(accel_speed)+" kmph", new Point(720/2, 140), 1, 1.5, new Scalar(cv_text_red,cv_text_green,cv_text_blue));
            //Imgproc.putText(mRgba, "OBs3ClientD speed: "+convertHex+" kmph", new Point(720/2, 200), 1, 1.5, new Scalar(cv_text_red,cv_text_green,cv_text_blue));
            //Imgproc.putText(mRgba, "GPS speed: "+Speed+" kmph", new Point(720/2, 240), 1, 1.5, new Scalar(cv_text_red,cv_text_green,cv_text_blue));


            /**
             * Writing text on camera screen for events here.
             */
            if (isTimerRunning) {
                //previous points = 720/2, 140
                Imgproc.putText(mRgba, brakeEvent + "!!!", new Point(360 - 140, 240), 1, 3, new Scalar(cv_text_red, cv_text_green, cv_text_blue), 3);
            } else {
                Imgproc.putText(mRgba, " ", new Point(360 - 140, 240), 1, 3, new Scalar(cv_text_red, cv_text_green, cv_text_blue), 3);
            }

            //  Log.d("ksensorsssss>>>>", "kkkkk");

        }


        /**
         * Writing trip events on camera scree here.
         */
        if (isGyroTimerRunning) {
            //previous points = 720/2, 140
            Imgproc.putText(mRgba, gryoEvent, new Point(360 - 140, 240), 1, 3, new Scalar(cv_text_red, cv_text_green, cv_text_blue), 3);
        } else {
            Imgproc.putText(mRgba, " ", new Point(360 - 140, 240), 1, 3, new Scalar(cv_text_red, cv_text_green, cv_text_blue), 3);
        }


        if (Gyro.equals("ON")) {
            //previous points = 720/2, 170
//            Imgproc.putText(mRgba, "Gyro: "+"("+df.format(gyroX)+", "+df.format(gyroY)+", "+df.format(gyroZ)+")", new Point(720/2, 140), 1, 1.5, new Scalar(cv_text_red,cv_text_green,cv_text_blue));
            //Imgproc.putText(mRgba, "Gyro: Y-"+gyroY, new Point(470, 260), 1, 1.5, new Scalar(0,255,0));
            //Imgproc.putText(mRgba, "Gyro: Z-"+gyroZ, new Point(470, 290), 1, 1.5, new Scalar(0,255,0));
        }

        if (mDetectorType == JAVA_DETECTOR) {
            if (mJavaDetector != null) {
                mJavaDetector.detectMultiScale(mGray, faces, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
                        new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
            }
        } else {
            Log.e(TAG, "Detection method is not selected!");
        }
        /**
         * Writing other info on camera screen here.
         */
        Imgproc.putText(mRgba, "Powered By THRSL", new Point((720 / 2) - 130, 480 - 15), 1, 1.5, new Scalar(255), 2);
        Imgproc.putText(mRgba, "Smart DashCam", new Point(40, 30), 1, 2, new Scalar(255), 2);
        Imgproc.putText(mRgba, "Time:" + time, new Point(30, 80), 1, 2, new Scalar(0, 255, 0));
        //Imgproc.putText(mRgba, "Lat:" + df.format( lat) + " Long:" + df.format(longi), new Point(width/2, 50), 1, 1.5, new Scalar(0, 255, 0));
        //Imgproc.putText(mRgba, "Lat:"+ df.format(lat),  new Point(200, 110), 1, 1.5, new Scalar(0,255,0));
        //Imgproc.putText(mRgba, "Longi:"+df.format(longi), new Point(200, 140), 1, 1.5, new Scalar(0,255,0));
        //Imgproc.putText(mRgba, "Speed:"+(int)Speed+" km/hr", new Point(width/2, 80), 1, 1.5, new Scalar(0,255,0));
        //sensor
        //speed.setText(String.valueOf(Speed));

        //  Imgproc.putText(mRgba, "Smart DashCam", new Point(40, 400), 1, 2, new Scalar(255), 2);

        //  Log.d("overspeed>><<<",OverSpeedLimit);

        /**
         * Checking speed here for calculating overspeed timer.
         */
        if (sPeed_units.equals("mph")) {
            setText(speed, String.valueOf((int) (Speed * 0.621371)), choose_unit, sPeed_units);
            if (((int) (Speed * 0.621371)) > Integer.valueOf(OverSpeedLimit) * 0.621371) {
                overSpeedTimer();
            } else {
                overSpeedHolderFlag = false;
            }

        } else {
            setText(speed, String.valueOf((int) Speed), choose_unit, sPeed_units);
            if (((int) (Speed)) > Integer.valueOf(OverSpeedLimit)) {
                //  if (((int) (Speed)) > -1) {
                overSpeedTimer();
            } else {
                overSpeedHolderFlag = false;
            }
        }

        /**
         * This method use for storing all speed points.
         */
        calculateSpeedPoints();
        //Imgproc.putText(mRgba, "Acceleration: "+df.format(linearAccel)+" m/s2", new Point(width/2, 110), 1, 1.5, new Scalar(0,255,0));

        //Imgproc.putText(mRgba, "Gyro: "+"("+df.format(gyroX)+", "+df.format(gyroY)+", "+df.format(gyroZ)+")", new Point(width/2, 140), 1, 1.5, new Scalar(0,255,0));
        //Imgproc.putText(mRgba, "Gyro: Y-"+gyroY, new Point(470, 260), 1, 1.5, new Scalar(0,255,0));
        // Imgproc.putText(mRgba, "Gyro: Z-"+gyroZ, new Point(470, 290), 1, 1.5, new Scalar(0,255,0));

        //Features for video shooting
        Rect[] facesArray = faces.toArray();
        dist = 0.0f;
        for (i = 0; i < facesArray.length; i++) {
            //reduce rectangle height and width
            facesArray[i].width *= 0.5;
            facesArray[i].height *= 0.5;
            facesArray[i].x *= 1.15;
            facesArray[i].y *= 1.15;
            //Toast.makeText(gpsTracker,
            // "beeb"+Beep, Toast.LENGTH_SHORT).show();
            if (SDetect.equals("ON")) {
                Imgproc.putText(mRgba, String.valueOf(dist), new Point(facesArray[i].x, facesArray[i].y - 5), 1, 2, new Scalar(255, 0, 0), 3);

                Imgproc.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 3);
            }
           /* if (Beep.equals("ON")) {
                toneGen1.startTone(AudioManager.STREAM_MUSIC, 200);
            }*/
           /* if ((facesArray[i].y+facesArray[i].height)>0.5*mRgba.height() && (facesArray[i].y+facesArray[i].height)<0.75*mRgba.height()){
                Imgproc.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), ROI_COLOR_YELLOW, 3);
                float height = (float) (0.265 * facesArray[i].height);
                dist = (float) ((((mFocalLength[0] * 1.25) / height) * 100));
                Imgproc.putText(mRgba, String.valueOf(dist), new Point(facesArray[i].x, facesArray[i].y - 5), 1, 2, new Scalar(255, 255, 0), 3);
            }
            if ((facesArray[i].y+facesArray[i].height)>0.75*mRgba.height()){
                float height = (float) (0.265 * facesArray[i].height);
                dist = (float) ((((mFocalLength[0] * 1.25) / height) * 100));
                Imgproc.putText(mRgba, String.valueOf(dist), new Point(facesArray[i].x, facesArray[i].y - 5), 1, 2, new Scalar(255, 0, 0), 3);

                Imgproc.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(),ROI_COLOR_RED , 3);
            }*/


            Log.d(TAG, "onCameraFrame: Focal length : " + mFocalLength[0] + " dist est : " + dist);
            Log.d(TAG, "onCameraFrame: dist :" + dist + "speed :" + Speed);

        }
        if (HbAcc.equals("ON")) {
            if (linearAccel > 3.0f) {

            } else {
            }
        }
      /*  Bitmap srcBitmap = BitmapFactory.decodeResource(getResources(), org.opencv.R.drawable.horusi);
        Utils.bitmapToMat(srcBitmap, mRgba);*/
        Imgproc.cvtColor(mRgba, bgr, Imgproc.COLOR_RGBA2BGR);
        recording(bgr);


        /**
         * Wring overspeed text on camera screen here.
         */
        if (isOverSpeedTimerRunning) {
            Imgproc.putText(mRgba, "OVER SPEEDING!!!", new Point(360 - 140, 240), 1, 3, new Scalar(cv_text_red, cv_text_green, cv_text_blue), 3);
        } else {
            Imgproc.putText(mRgba, " ", new Point(360 - 140, 240), 1, 3, new Scalar(cv_text_red, cv_text_green, cv_text_blue), 3);
        }

        //  Log.d("cameraframing>","running");

        return mRgba;
    }

    /**
     * Saving all event related data using this method.
     * @param text
     * @param value
     * @param choose_unit
     * @param value1
     */
    private void setText(final TextView text, final String value, final TextView choose_unit, final String value1) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                saveToFile(value, filename);
                text.setText(value);
                choose_unit.setText(value1);
            }
        });
    }

    public void replaceFragment() {
        GpsFragment gpsFragment = new GpsFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer, gpsFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }


    /**
     * This method used for getting location updates.
     */
    private void startLocationUpdates() {
        Log.d(TAG, "gps: Started location updates!");

        //noinspection MissingPermission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private double deg2rad(double deg) {

        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {

        return (rad * 180.0 / Math.PI);
    }

    /**
     * This method calculate distance between two lat longs.
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    private double distance(double lat1, double lon1, double lat2, double lon2) {
        // haversine great circle distance approximation, returns meters
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60; // 60 nautical miles per degree of seperation
        dist = dist * 1852; // 1852 meters per nautical mile
        return (dist);
    }

    /**
     * In this method we get location updates every seconds and we also calculate current speed here.
     */
    private void init() {
        Log.d(TAG, "gps: init");
        mLastLocationUpdateTime = 0;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);
        String Phnum = DBHandlerWalk.Instance(getApplicationContext()).getPhoneNum();
        //String Phnum="9788094958";
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Integer counter = 0;
                latitude = new double[2];
                longitude = new double[2];
                times = new Long[2];

                super.onLocationResult(locationResult);
                // location is received
                Double d1;
                Long t1;
                // Double speed = 0.0;
                d1 = 0.0;
                t1 = 0l;
                mCurrentLocation = locationResult.getLastLocation();
                lat = mCurrentLocation.getLatitude();
                longi = mCurrentLocation.getLongitude();
                latitude[counter] = mCurrentLocation.getLatitude();
                longitude[counter] = mCurrentLocation.getLongitude();
                times[counter] = mCurrentLocation.getTime();
                try {
                    // get the distance and time between the current position, and the previous position.
                    // using (counter - 1) % data_points doesn't wrap properly
                    d1 = distance(latitude[counter], longitude[counter], latitude[(counter + 1) % 2], longitude[(counter + 1) % 2]);
                    t1 = times[counter] - times[(counter + 1) % 2];
                } catch (NullPointerException e) {
                    //all good, just not enough data yet.
                }
                if (mCurrentLocation.hasSpeed()) {
                    Speed = mCurrentLocation.getSpeed() * 1.0;
                } else {
                    if (t1 == 0) Speed = 0.0;
                    else Speed = d1 / t1;
                }

                counter = (counter + 1) % 2;
                Speed = Speed * 3.6d;
                //Speed += (accel_speed + Speed)/2;
                //String FcwNFE=sharedPreferences.getString("FCW_NFE","");
                SharedPreferences sharedPreferences = getSharedPreferences("SETTING", Context.MODE_PRIVATE);
                String OswNFE = sharedPreferences.getString("OS_NFE", "");
                String HbNFE = sharedPreferences.getString("HB_NFE", "");

                calculateAvgSpeed();
                // calculateSpeedPoints();


                //Overspeed NFE end

               /* if(Speed>int_hb_speed)
                {
                    speed_track=true;
                    if(speed_buffer.size()>5)
                    {
                        speed_buffer.remove(0);
                    }
                    speed_buffer.add(Speed);
                }
*/


               /* if(speed_track)
                {
                    if(speed_buffer.size()>2){
                        if(speed_buffer.get(speed_buffer.size()-1) - speed_buffer.get(speed_buffer.size()-2) >= 9) {
                            harshbreak = true;
                            harshbreak_timecount += 1;

                            if ((Speed < 30) & (harshbreak == false)) {
                                speed_track = false;
                                speed_buffer.clear();
                            }
                        }
                    }
                }*/
                Log.d(TAG, "gps: Lat : " + mCurrentLocation.getLatitude());
                Log.d(TAG, "gps: Long : " + mCurrentLocation.getLongitude());
                Log.d(TAG, "gps: timestamp : " + mCurrentLocation.getTime() / 1000);
                Log.d(TAG, "gps: speed : " + mCurrentLocation.getSpeed());
                Log.d(TAG, "gps: Trip num : " + TripNum);
                //update the webserver once in 30 seconds
                if (((mCurrentLocation.getTime() / 1000) - mLastLocationUpdateTime) > 30) {
                    if (TripNum != 0) {
                        //wait until the db is read and the next trip num is updated
                        Log.d(TAG, "DB: Creating GPSLocation and calling dbHandler.addHandler");
                        GpsLocation loc = new GpsLocation(mCurrentLocation.getTime() / 1000,
                                String.valueOf(mCurrentLocation.getLatitude()),
                                String.valueOf(mCurrentLocation.getLongitude()), 0.0f, TripNum);
                        DBHandlerWalk.Instance(getApplicationContext()).addHandler(loc);
                        //update the gps data to the server
                        final ArrayList<DeviceLocation> devArr = new ArrayList<DeviceLocation>();
                        DeviceLocation devl = new DeviceLocation(Phnum + "@HORUSIADAS", String.valueOf(mCurrentLocation.getLatitude()),
                                String.valueOf(mCurrentLocation.getLongitude()), "gps", String.valueOf(mCurrentLocation.getTime() / 1000));
                        devArr.add(devl);
                        final HorusInterface horusInterface = HorusClient.getClient().create(HorusInterface.class);
                        //retrofit2.Call<Void> logLatLongEventCall = horusInterface.logLatLongEvent(devArr);
                        retrofit2.Call<Void> logLatLongEventCall = horusInterface.logLatLongEvent(devArr);
                        logLatLongEventCall.enqueue(new retrofit2.Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {
                                if (response.code() == 200) {
                                    Log.d(TAG, "Sent lat long details to Horus webserver successfully.");
                                } else {
                                    //String codestr = String.valueOf(response.code());
                                    Log.d(TAG, "Sending lat long details to Horus webserver failed.");
                                    Log.d(TAG, String.valueOf(response.code()) + response.body());
                                }
                            }

                            @Override
                            public void onFailure(Call<Void> call, Throwable t) {
                                Log.d(TAG, "Sending lat long details to Horus webserver failed.");
                                //  Log.d(TAG, t.getMessage());
                            }
                        });

                        mLastLocationUpdateTime = mCurrentLocation.getTime() / 1000;
                    }
                }

            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();

    }

    String getLogFilename() {
        String filepath;
        if ((mLogFilename == null) || (mLogFilename.isEmpty())) {
            //create new log filename
            filepath = logrecordfilepath();
            final Date date = new Date();
            SimpleDateFormat Formater = new SimpleDateFormat("yyyyMMdd_hhmmss");
            String datestr = Formater.format(date);
            mLogFilename = "Log_" + timeStamp + ".txt";
            try {
                FileOutputStream fos = new FileOutputStream(filepath + "/" + mLogFilename);
                String logstr = "Object Type,Time,DetectionType,Bounding Boxes,Distance,Speed,Latitude,Longitude,Acceleration\n";
                fos.write(logstr.getBytes());
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            filepath = logrecordfilepath();
            File fp = new File(filepath + "/" + mLogFilename);
            if (fp.length() > 102400) {
                //prev log file larger than 100KB, create new log file
                final Date date = new Date();
                SimpleDateFormat Formater = new SimpleDateFormat("yyyyMMdd_hhmmss");
                String datestr = Formater.format(date);
                mLogFilename = "Log_" + datestr + ".txt";
                try {
                    FileOutputStream fos = new FileOutputStream(filepath + "/" + mLogFilename);
                    String logstr = "Object Type,Time,DetectionType,Bounding Boxes,Distance,Speed,Latitude,Longitude,Acceleration\n";
                    fos.write(logstr.getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return mLogFilename;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // get angle around the z-axis rotated
        float degree = Math.round(sensorEvent.values[0]);


        // rotation animation - reverse turn degree degrees
        RotateAnimation ra = new RotateAnimation(
                DegreeStart,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        // set the compass animation after the end of the reservation status
        ra.setFillAfter(true);

        // set how long the animation for the compass image will take place
        ra.setDuration(210);

        // Start animation of compass image
        compassimage.startAnimation(ra);
        DegreeStart = -degree;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void getAccelerometerUpdates(int delay) {
        Handler handler = new Handler();
        //while(true) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                float z_accel_mean = 0.0f;
                for (int i = 0; i < z_accel.size(); i++) {
                    z_accel_mean += z_accel.get(i);
                }
                z_accel_mean /= z_accel.size();
                if (z_accel_mean > 0) {
                    accel_speed = prev_speed - linearAccel * 3.6;
                } else {
                    accel_speed = prev_speed + linearAccel * 3.6;
                }
                prev_speed = accel_speed;
                handler.postDelayed(this, 1000);
            }
        };
        //}
        handler.postDelayed(runnable, 1000);
    }

    //storage limit implementation
    private long getSizeOfFolder() {
        long size = dirSize(Environment.getExternalStorageDirectory().getPath() + "/ClubD2S/");
        return size;
    }

    private long dirSize(String path) {
        File dir = new File(path);
        if (dir.exists()) {
            long bytes = getFolderSize(dir);
            if (bytes < 1024) return (byte) bytes;
            return bytes;
        }
        return 0;
    }


    public static long getFolderSize(File dir) {
        if (dir.exists()) {
            long result = 0;
            File[] fileList = dir.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // Recursive call if it's a directory
                if (fileList[i].isDirectory()) {
                    result += getFolderSize(fileList[i]);
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length();
                }
            }
            return result; // return the file size
        }
        return 0;
    }

    private void runLooperStorage() {
        ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
        // This schedule a runnable task every 2 minutes
        long lConvert = Long.parseLong(mSelectedSize);
        int mConvert = 1024 * 1024 * 1024;
        mConvertToByte = lConvert * mConvert;
        Log.d(TAG, "onClick: mConvertToByte : " + mConvertToByte + " mFolderSize    :" + mFolderSize);
        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                while (mStorageFlag) {
                    //  Log.d(TAG, "*** runLooperStorage ***");
                    mFolderSize = getSizeOfFolder();
                    //  Log.d(TAG, "*** mFolderSize *** : " + mFolderSize);
                    if (mFolderSize > mConvertToByte) {
                        Log.d(TAG, "*** LIMIT EXCEED!!!");
                        showFiles(mStorage.getExternalStorageDirectory().toString() +
                                "/ClubD2S/Videos");
                        mFolderSize = getSizeOfFolder();
                        break;
                    }
                }
            }
        }, 0, 1, TimeUnit.MINUTES);
    }


    private void showFiles(String path) {
        Log.d(TAG, "path: " + path);
        List<File> files = mStorage.getFiles(path);
        if (files != null) {
            Collections.sort(files, OrderType.NAME.getComparator());
        }
        for (File file : files) {
            String filepath = file.getAbsolutePath().toString();
            Log.d(TAG, "showFiles1: filepath : " + filepath);
            mList.add(filepath);
            Log.d(TAG, "Size of list : " + mList.size());
            File fPath = new File(filepath);
            long length = fPath.length();
            Log.d(TAG, "length  : " + length);
            tLength = tLength + length;
            Log.d(TAG, "tLength : " + tLength);
            if (tLength > 209715200) {
                Log.d(TAG, "EXCEED!!!");
                for (String s : mList) {
                    deleteVideo(s);
                    Log.d(TAG, "Size of list : " + mList.size());
                }
                tLength = 0;
                mList.clear();
                break;

            } else {
                Log.d(TAG, "CONTINUE!!!");
                continue;
            }
        }
    }

    private void deleteVideo(String path) {
        File fdelete = new File(path);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                Log.d(TAG, "deleteVideo: path :  " + path);
            } else {
                Log.d(TAG, "file not Deleted : path :  " + path);
            }
        }
    }


    private void checkCameraOrientation() {

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        // Get the default sensor of specified type
        mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        SensorEventListener orientationListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {

                // Log.d(TAG, "Sensor Changed");
                float[] values = event.values;
                int orientation = ORIENTATION_UNKNOWN;
                float X = -values[_DATA_X];
                float Y = -values[_DATA_Y];
                float Z = -values[_DATA_Z];
                float magnitude = X * X + Y * Y;
                // Don't trust the angle if the magnitude is small compared to the y value
                if (magnitude * 4 >= Z * Z) {
                    float OneEightyOverPi = 57.29577957855f;
                    float angle = (float) Math.atan2(-Y, X) * OneEightyOverPi;
                    orientation = 90 - (int) Math.round(angle);
                    // normalize to 0 - 359 range
                    while (orientation >= 360) {
                        orientation -= 360;
                    }
                    while (orientation < 0) {
                        orientation += 360;
                    }
                }
                //^^ thanks to google for that code
                //now we must figure out which orientation based on the degrees
                //   Log.d("Oreination", ""+orientation);
                if (orientation != mOrientationDeg) {
                    mOrientationDeg = orientation;
                    //figure out actual orientation
                    if (orientation == -1) {//basically flat

                    } else if (orientation <= 45 || orientation > 315) {//round to 0
                        tempOrientRounded = 1;//portrait

                    } else if (orientation > 45 && orientation <= 135) {//round to 90
                        tempOrientRounded = 2; // Reverse Landscape
                    } else if (orientation > 135 && orientation <= 225) {//round to 180
                        tempOrientRounded = 3; //upside down
                    } else if (orientation > 225 && orientation <= 315) {//round to 270
                        tempOrientRounded = 4;//Landscape
                    }

                }

                if (mOrientationRounded != tempOrientRounded) {
                    //Orientation changed, handle the change here
                    mOrientationRounded = tempOrientRounded;
                    Log.d("Orientation >>>> ", tempOrientRounded + "");

                }


            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };


        if (mOrientation != null) {
            mSensorManager.registerListener(orientationListener, mOrientation, SensorManager.SENSOR_DELAY_GAME);
        }

    }


    /**
     * This is brake timer for put wait for 3 seconds after hard brake. Also save event in database.
     */
    private void breakTimer() {
        if (!isTimerRunning) {

            runOnUiThread(new Runnable() {
                public void run() {

                    countDownTimer = new CountDownTimer(3000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            isTimerRunning = true;
                        }

                        public void onFinish() {
                            isTimerRunning = false;
                            saveEventInDatabase(brakeEvent);
                            brakeEvent = " ";
                            hardBreakCounter++;
                        }
                    }.start();

                }
            });
        }
    }

    /**
     * Overspeed timer for put wait for 3 second after overspeed event.
     * Also save event in database.
     */

    private void overSpeedTimer() {

        if (!isOverSpeedTimerRunning) {

            runOnUiThread(new Runnable() {
                public void run() {

                    overSpeedTimer = new CountDownTimer(3000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            // Log.d("Timer Start >>>", "timer start");
                            isOverSpeedTimerRunning = true;
                        }

                        public void onFinish() {
                            //  Log.d("Timer Finish >>>", "timer finsh");

                            isOverSpeedTimerRunning = false;
                            brakeEvent = " ";

                            if (!overSpeedHolderFlag) {

                                if (Beep.equals("ON")) {
                                    toneGen1.startTone(AudioManager.STREAM_MUSIC, 800);
                                }

                                saveEventInDatabase(brakeEvent);
                                overSpeedCounter++;
                                overSpeedHolderFlag = true;
                            }

                        }
                    }.start();
                }
            });
        }
    }


    /**
     * This method used for saving trip events in database.
     * @param eventName
     */
    private void saveEventInDatabase(String eventName) {

        Log.d("background", "background");

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
        String timeStamp = ((Long) (System.currentTimeMillis() / 1000)).toString();
        String date = dateFormat.format(new Date());
        String time = timeFormat.format(Calendar.getInstance().getTime());

        if (!PreferenceHelper.getPreferences(this).contains("tripId")) {
            PreferenceHelper.setStringPreference(this, "tripId", timeStamp);
        }

        String tripId = PreferenceHelper.getStringPreference(this, "tripId");

        TripDatabaseHandler databaseHandler = new TripDatabaseHandler(this);
        databaseHandler.open();
        databaseHandler.insertTripData(timeStamp, date, time, eventName, tripId);
        databaseHandler.getTripData();

    }

    @Override
    protected void onStop() {

        super.onStop();
       // unregisterReceiver(smsReceiver);
    }


    /**
     * This method is called when we abort Trip. Also send user to Trip Report page with trip data.
     */
    private void abortTrip() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("")
                .setMessage("End The Trip?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        PreferenceHelper.cleanPerticualrPref(CameraActivity.this, "tripId");
                        onstopFlag = true;
                        onDestroyFlag = true;
                        onBackPressed();

                        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
                        String endTime = timeFormat.format(Calendar.getInstance().getTime());

                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String date = dateFormat.format(new Date());

                        tripEndDate = new Date();
                        long totalTripDuration = tripEndDate.getTime() - tripStartDate.getTime();

                        DecimalFormat df = new DecimalFormat("#");
                        df.setMaximumFractionDigits(1);

                        double distance = 0;

                        if (locations.size() > 2) {

                            for (int i = 0; i < locations.size() - 1; i++) {
                                distance = distance + getDistanceBetweenTwoLocations(locations.get(i).getLatitude(), locations.get(i).getLongitude(), locations.get(i + 1).getLatitude(), locations.get(i + 1).getLongitude());
                            }
                        }

                        double avgSpeed = distance * 3600000 / totalTripDuration;

                        Log.d("distance>>>", distance + " >>> " + " >> " + avgSpeed + " >> " + totalTripDuration);

                        Log.d("distancemm>>", df.format(distance) + " >>> " + df.format(avgSpeed));

                        // Toast.makeText(CameraActivity.this,distance + " >> "+ avgSpeed, Toast.LENGTH_LONG).show();

                        Crashlytics.log(Log.DEBUG, "LOL", distance + ">> " + " >> " + avgSpeed + " >> " + totalTripDuration);

                        Crashlytics.setString("luki", distance + ">> " + " >> " + avgSpeed + " >> " + totalTripDuration);

                        Crashlytics.setString("loca", locations.toString());

                        //  int b = 5/0;

                        Intent intent = new Intent(CameraActivity.this, TripReport.class);
                        intent.putExtra("startTime", startingTime);
                        intent.putExtra("distance", df.format(distance) + "" + " Km");
                        intent.putExtra("time", convertmiliSecondsToHMmSs(totalTripDuration));
                        intent.putExtra("speed", df.format(avgSpeed) + "" + " Km/h");
                        intent.putExtra("hardbrake", hardBreakCounter + "");
                        intent.putExtra("overSpeed", overSpeedCounter + "");
                        intent.putExtra("endTime", endTime);
                        intent.putExtra("currentDate", date);
                        intent.putExtra("speedPoints", avgSpeedPointsList);
                        intent.putExtra("startTimeMills", startTimeInMills);
                        intent.putExtra("sharpTurn", gyroCounter + " ");
                        double ddd = TimeUnit.MILLISECONDS.toHours(totalTripDuration);
                        intent.putExtra("durationInHours", ddd + "");

                        long durr = ((totalTripDuration / (1000 * 60 * 60)) % 24);

                        Log.d("duration>", durr + " " + totalTripDuration);

                        PreferenceHelper.setStringPreference(CameraActivity.this, "reportway", "camera");

                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // onstopFlag = true;
                        // onBackPressed();
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public static double round(double value, int places) {
       /* if (Double.isNaN(value)){
            Log.d("nannan>>","nan");
            return 0;
        }else {*/
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
        // }
    }


    @Override
    public void onBackPressed() {
        if (onstopFlag) {
            onstopFlag = false;
            super.onBackPressed();
        } else {
            abortTrip();
        }
    }


    /**
     * This method used for start video recording.
     */
    private void startVideoRecording() {
        long lConvert = Long.parseLong(mSelectedSize);
        int mConvert = 1024 * 1024 * 1024;
        mConvertToByte = lConvert * mConvert;
        Log.d(TAG, "onClick: mConvertToByte : " + mConvertToByte + " mFolderSize    :" + mFolderSize);
        // Toast.makeText(getApplicationContext(),"mConvertToByte : "+mConvertToByte,Toast.LENGTH_LONG).show();
        if (mFolderSize < mConvertToByte) {
            mStorageFlag = true;
           /* if (recording) {
                img_settings.setEnabled(true);
                //StopAudioRec();
              //  videoRecord.setImageResource(R.drawable.record);
                recording = false;
                focus.stop();
                mVideoWriter.release();
                Toast.makeText(CameraActivity.this, "Stop recording", Toast.LENGTH_SHORT).show();
            } else {*/
            //recordAudio();
            //  img_settings.setEnabled(false);
            //   videoRecord.setImageResource(R.drawable.record_red);


            recording = true;
            focus.setBase(SystemClock.elapsedRealtime()); // reset the counter to 0
            focus.start();
            recordfilepath();

            double sum = 0;
            for (Double d : avgSpeedArray)
                sum += d;

            Double avgSpeed = sum / 10;

            // if (avgSpeed>-1) {
               /* java.util.Date date = new java.util.Date();
                //  String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());
                String timeStamp = ((Long) (System.currentTimeMillis() / 1000)).toString();
                File file = new File(recordfilepath(), timeStamp + ".avi");
                Log.d(TAG, "file : " + file);
                try {
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            //  saveVideo = file.getAbsolutePath();
            //   }
            Toast.makeText(CameraActivity.this, "Recording", Toast.LENGTH_SHORT).show();
            //  }
        } else {
            Log.d(TAG, "onClick: Limit Exceed!");
            Toast.makeText(getApplicationContext(), "Limit Exceed!", Toast.LENGTH_SHORT).show();
        }

    }

    private void makeSensorsOnByDefault() {

        SharedPreferences sharedPreferences = getSharedPreferences("SETTING", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedPreferences.edit();
        editor1.putString("ACCELERO_STATE", "ON");
        editor1.putString("GYRO_STATE", "ON");
        editor1.apply();
    }


    /**
     * We keep last ten speed point in arraylist for calculate average speed.
     */
    private void calculateAvgSpeed() {
        if (avgSpeedArray.size() < 10) {
            avgSpeedArray.add(Speed);
        } else {
            avgSpeedArray.remove(0);
            avgSpeedArray.add(Speed);
        }

    }

    /**
     * Converting milisenconds to HMmSs format.
     * @param millis
     * @return
     */
    public String convertmiliSecondsToHMmSs(long millis) {
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
        return hms;
    }

    /**
     * This method set alarm and start service to upload video on AWS in background.
     */
    public void startServiceAndAlarmManagerVideoUpload() {
        Log.e("Main Activity", "start button clicked");

        /* if (ServiceContext!=null) {
            local_broadcast_manager = LocalBroadcastManager.getInstance(ServiceContext);
            // Log.e("Activity","Sending Broadcaste to stop service");
            Log.e("techservice", "Sending Broadcaste to stop service");
            Intent i = new Intent(Constants.PHRASE_SERVICE_BROADCAST_ID);
            i.putExtra("command", "STOP");
            local_broadcast_manager.sendBroadcast(i);
        }*/

        Context context = this.getApplicationContext();
        if (alarm != null) {
            Log.e("uploading > ", "Set Alarm");

            alarm.SetAlarm(context);

        } else {
            //Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }
    }

    //My CountDown Timer code
   /* public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            // Toast.makeText( CameraActivity.this, "CountDown Timer Begins", Toast.LENGTH_SHORT).show();

            //  if(checkAndRequestPermissions())
            //  {
            getListOfTimeStampFromDB();
            return;
            //  }
        }

        @Override
        public void onTick(long millis) {

        }

        @Override
        public void onFinish() {
            myCountDownTimer = null;
            if (myCountDownTimer == null) {
                myCountDownTimer = new MyCountDownTimer(totalTime, 1000);
                myCountDownTimer.start();
            }

//            showToast("Time out.");
//            finish();
        }
    }
*/
    public void credentialsProvider() {

        // Initialize the Amazon Cognito credentials provider
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                ServiceContext,
                Constants.IDENTITY_POOL_ID,
                Constants.MY_REGION // Region
        );

        setAmazonS3Client(credentialsProvider);
    }


    /**
     * Create a AmazonS3Client constructor and pass the credentialsProvider.
     */
    public void setAmazonS3Client(CognitoCachingCredentialsProvider credentialsProvider) {

        // Create an S3 client
        s3 = new AmazonS3Client(credentialsProvider);

        // Set the region of your S3 bucket
        s3.setRegion(Region.getRegion(Constants.MY_REGION));

    }

    public void setTransferUtility() {

        // transferUtility = new TransferUtility(s3, getApplicationContext());
        transferUtility = TransferUtility.builder().s3Client(s3).context(ServiceContext).build();
        // transferUtility = new TransferUtility()
    }


    /**
     * We get time stamps of videos and events from database.
     * @param context
     */
    public void getListOfTimeStampFromDB(Context context) {
        Log.e("techservice", "inActivityNow");
        ServiceContext = context;

        credentialsProvider();
        setTransferUtility();

        TripDatabaseHandler tripDatabaseHandler = new TripDatabaseHandler(ServiceContext);
        tripDatabaseHandler.open();
        //  tripData = new ArrayList<>(tripDatabaseHandler.getTripData());
        tripData = new ArrayList<>();
        LinkedHashSet<TripData> LinkedTripData = new LinkedHashSet<>(tripDatabaseHandler.getTripData());

        // Log.e("Upload",""+tripData.size());
        String lastTrip = PreferenceHelper.getStringPreference(ServiceContext, Constants.LAST_UPLOADED_TIMESTAMP);
        Log.e("lastTrip", lastTrip);


        if (LinkedTripData.size() == 0) {
            local_broadcast_manager = LocalBroadcastManager.getInstance(ServiceContext);
            // Log.e("Activity","Sending Broadcaste to stop service");
            Log.e("uploading > ", "Sending Broadcaste to stop service");
            Intent i = new Intent(Constants.PHRASE_SERVICE_BROADCAST_ID);
            i.putExtra("command", "STOP");
            local_broadcast_manager.sendBroadcast(i);
        }

        if (LinkedTripData.size() > 0) {
            boolean addData = false;
            for (TripData TripData : LinkedTripData) {

                if (!lastTrip.equalsIgnoreCase("")) {

                    if (TripData.getTimeStamp().equalsIgnoreCase(lastTrip)) {

                        addData = true;
                        continue;

                    } else {
                        if (addData) {
                            tripData.add(TripData);
                        }
                    }
                } else {
                    tripData.add(TripData);
                }
            }

            if (tripData.size() > 0) {
                fileUpload = true;
                getAllListOfVideosFromDeviceFolder();
            } else {
                local_broadcast_manager = LocalBroadcastManager.getInstance(ServiceContext);
                // Log.e("Activity","Sending Broadcaste to stop service");
                Log.e("uploading > ", "Sending Broadcaste to stop service");
                Intent i = new Intent(Constants.PHRASE_SERVICE_BROADCAST_ID);
                i.putExtra("command", "STOP");
                local_broadcast_manager.sendBroadcast(i);
            }
        }


    }

    /**
     * This method give list of all videos from folder.
     */
    private void getAllListOfVideosFromDeviceFolder() {
        VideoListName = new ArrayList<>();
        MediaScannerConnection.scanFile(ServiceContext,
                new String[]{"/storage/emulated/0/ClubD2S/Videos"}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        //Do something


                       /* ArrayList<String> VideoListName  = new ArrayList<>();

                        String selection=MediaStore.Video.Media.DATA +" like?";
                        String[] selectionArgs=new String[]{"/storage/emulated/0/ClubD2S/%Videos%"};

                        String[] proj = { MediaStore.Video.Media._ID,
                                MediaStore.Video.Media.DATA,
                                MediaStore.Video.Media.DISPLAY_NAME,
                                MediaStore.Video.Media.SIZE };

                        Cursor videocursor = managedQuery(MediaStore.Video.Media.INTERNAL_CONTENT_URI,
                                proj, selection, selectionArgs, MediaStore.Video.Media.DATE_TAKEN + " DESC");

                        for(int i = 0; i<videocursor.getCount();i++)
                        {
                            int video_column_index = videocursor
                                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                            String filename = videocursor.getString(video_column_index);
                            VideoListName.add(filename);
                            videocursor.moveToNext();
                        }

                        Log.e("filename",VideoListName.size()+"");*/

                        File photoDirectory = new File("/storage/emulated/0/ClubD2S/Videos");
                        /*
                         * this checks to see if there are any previous test photo files
                         * if there are any photos, they are deleted for the sake of
                         * memory
                         */
                        Log.e("uploading > ", "start uploading list of videos");
                        if (photoDirectory.exists()) {
                            File[] dirFiles = photoDirectory.listFiles();
                            if (dirFiles.length != 0) {
                                for (int ii = 0; ii < dirFiles.length; ii++) {

                                    Log.e("uploading > ", "collection videos from SD card");

                                    int lastIndex = photoDirectory.listFiles()[ii].toString().lastIndexOf("/") + 1;
                                    int lastIndexExtension = photoDirectory.listFiles()[ii].toString().lastIndexOf(".");

                                    VideoListName.add(photoDirectory.listFiles()[ii].toString().substring(lastIndex, lastIndexExtension));

                                }
                                Log.e("uploading > ", "video list > " + VideoListName.size() + " >>  " + VideoListName.toString());   //1547648417832
                                CompareTimeToGetListOfVideosToUpload();
                            }
                        }
                    }
                });


    }

    /**
     * Here we compare time of videos with events.
     */

    private void CompareTimeToGetListOfVideosToUpload() {
        videoListToUpload = new ArrayList<>();
        TripListToUpload = new ArrayList<>();

        Log.d("uploading > ", "compare time to get list of videos to upload");

        for (int i = 0; i < tripData.size(); i++) {
            for (int j = 0; j < VideoListName.size(); j++) {
                /*Date date = new Date(Long.parseLong(VideoListName.get(j))+30*1000);
                date.getTime();  */ //t=1547639158  , k=1547645763 , k2 = 1547645793 , k3 = 1547645824

//                long diffInMs = Long.parseLong(tripData.get(i).getTimeStamp()) - Long.parseLong(VideoListName.get(j));
                long eventTimestamp = Long.parseLong(tripData.get(i).getTimeStamp());
                long videoTimestamp = Long.parseLong(VideoListName.get(j).split("_")[1]);
                java.sql.Timestamp timestamp = new java.sql.Timestamp(eventTimestamp);
                Timestamp timestamp1 = new Timestamp(videoTimestamp);
                long diffInSec = timestamp.getTime() - timestamp1.getTime();

                Log.e("uploading > ", "time diffrence >" + diffInSec);

//                long timestamp2=  System.currentTimeMillis();
//                Timestamp timestamp3 = new Timestamp(timestamp2);
//                Date date = new Date(( System.currentTimeMillis()/1000)+30*1000);
//                date.getTime();


//                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
//                try {
//                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
//                    Date parsedDateytrip = dateFormat.parse("1547645824");
//                    Date parsedDateVideo = dateFormat.parse("1547645763");
//                    java.sql.Timestamp timestamptrip = new java.sql.Timestamp(parsedDateytrip.getTime());
//                    java.sql.Timestamp timestampVideo = new java.sql.Timestamp(parsedDateVideo.getTime());
//                    long diff = timestamptrip.getTime() - timestampVideo.getTime();
//                    Log.e("tag",""+timestamptrip.getTime());
//                    Log.e("tag",""+diff);
//
//                } catch(Exception e) { //this generic but you can control another types of exception
//                    // look the origin of excption
//                }
                if (diffInSec > 0) {
                    if (diffInSec <= 30) {
                        if (!videoListToUpload.contains(VideoListName.get(j))) {
                            videoListToUpload.add(VideoListName.get(j));
                            if (!TripListToUpload.contains(tripData.get(i).getTimeStamp())) {
                                TripListToUpload.add(tripData.get(i).getTimeStamp());
                            }
                        }
                    }
                }

               /* if(Long.parseLong(tripData.get(i).getTimeStamp())>=Long.parseLong(VideoListName.get(j)) && Long.parseLong(tripData.get(i).getTimeStamp())<date.getTime()){
                    if(!videoListToUpload.contains(tripData.get(i).getTimeStamp())) {
                        videoListToUpload.add(VideoListName.get(j));
                    }
                }*/
            }

        }
        Log.e("uploading > ", "video list to upload>> " + videoListToUpload.size() + " >> " + videoListToUpload.toString());
        Log.e("uploading > ", "Trip list to upload>> " + TripListToUpload.size() + " >> " + TripListToUpload.toString());
        callUpload();
    }

    /**
     * Here we start uploading video on AWS.
     */
    private void callUpload() {
        if (videoListToUpload.size() > 0) {
           // setFileToUpload();
        }
    }

    /**
     * This method is used to upload the file to S3 by using TransferUtility class
     */
    public void setFileToUpload() {
        String phoneNumber = ServiceContext.getSharedPreferences("LOGIN_SESSION_ACTIVE", MODE_PRIVATE).getString("PHONE", "") + "_";
        File videoToUpload = new File("/storage/emulated/0/ClubD2S/Videos/" + videoListToUpload.get(0) + ".avi");
        // Toast.makeText(ServiceContext, "Trying to upload", Toast.LENGTH_LONG).show();
        Log.e("uploading > ", "uploading..");
        String key = videoListToUpload.get(0) + ".avi";
        //final String path = getFilePathfromURI(imageUri);
        Log.e("uploading > ", key);
        TransferObserver transferObserver = transferUtility.upload(
                Constants.BUCKET,      //The bucket to upload to
                key,     //The key for the uploaded object
                videoToUpload        //The file where the data to upload exists
                //CannedAccessControlList.PublicRead
        );

        Log.e("uploading > ", "uploading videos .....");

        transferObserverListener(transferObserver, videoListToUpload.get(0), videoToUpload);
    }

    /**
     * This method upload video on AWS.
     * @param transferObserver
     * @param path
     * @param videoFile
     */

    public void transferObserverListener(TransferObserver transferObserver, String path, File videoFile) {

        transferObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.e("uploading > ", state + " > " + "in transferObserver");
                if (state.toString().equalsIgnoreCase("IN_PROGRESS")) {
                    // showLoading("Uploading");
                    Log.e("uploading > ", "uploading in progress");
                } else {
                    if (fileUpload) {
                        videoListToUpload.remove(path);
                        if (videoFile.exists()) {
                            videoFile.delete();
                        }

                        PreferenceHelper.setStringPreference(ServiceContext, Constants.LAST_UPLOADED_VIDEO, path);
                        if (TripListToUpload.size() > 0) {
                            PreferenceHelper.setStringPreference(ServiceContext, Constants.LAST_UPLOADED_TIMESTAMP, TripListToUpload.get(0));
                            TripListToUpload.remove(TripListToUpload.get(0));
                        }
                        Log.e("uploading > ", "video upload done");
                        if (videoListToUpload.size() > 0) {
                            callUpload();
                            Log.e("uploading > ", "call video upload again");
                        } else {
                            if (fileUpload) {
                                fileUpload = false;
                                new ExportDatabaseCSVTask().execute("");
                                Log.e("uploading > ", "uploading csv .....");
                            }
                        }
                    } else {
                        //delete db
                        Log.e("uploading > ", "clearing DB .... ");
                        TripDatabaseHandler tripDatabaseHandler = new TripDatabaseHandler(ServiceContext);
                        tripDatabaseHandler.open();
                        tripDatabaseHandler.clearSqliteDb();

                        PreferenceHelper.setStringPreference(ServiceContext, Constants.LAST_UPLOADED_TIMESTAMP, "");
                        local_broadcast_manager = LocalBroadcastManager.getInstance(ServiceContext);
                        // Log.e("Activity","Sending Broadcaste to stop service");
                        Log.e("techservice", "Sending Broadcaste to stop service");
                        Intent i = new Intent(Constants.PHRASE_SERVICE_BROADCAST_ID);
                        i.putExtra("command", "STOP");
                        //I am typing this comment just for nothing because i am getting bored as fuck...deewana mai hua
                        local_broadcast_manager.sendBroadcast(i);


                    }
                }

            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent / bytesTotal * 100);
                Log.e("percentage", percentage + "");
                //showLoading(percentage+"");
            }


            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });

    }


    /**
     * This method Enable GPS.
     */
    private void enableGPS() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(CameraActivity.this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(CameraActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                //  Log.d("Result>>>>",result);
                onRestart();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }


    /**
     * Export CSV of events for testing.
     */
    public class ExportDatabaseCSVTask extends AsyncTask<String, Void, Boolean> {
        private final ProgressDialog dialog = new ProgressDialog(ServiceContext);

        @Override
        protected void onPreExecute() {
//            this.dialog.setMessage("Exporting database...");
//            this.dialog.show();
        }

        protected Boolean doInBackground(final String... args) {
            File dbFile = ServiceContext.getDatabasePath("tripevents.db");
            System.out.println(dbFile);  // displays the data base path in your logcat
            File exportDir = new File("/storage/emulated/0/ClubD2S");

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }

            String phoneNumber = ServiceContext.getSharedPreferences("LOGIN_SESSION_ACTIVE", MODE_PRIVATE).getString("PHONE", "") + "_";
            PreferenceHelper.setStringPreference(ServiceContext, "csvName", phoneNumber + System.currentTimeMillis() / 1000 + "myfile.csv");
            File file = new File(exportDir, PreferenceHelper.getStringPreference(ServiceContext, "csvName"));
            // File file = new File(exportDir, "myfile.csv");
            if (file.exists()) {
                file.delete();
            }
            try {
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            /*    Cursor curCSV = myDatabase.rawQuery("select * from " + Table_Name,null);
                csvWrite.writeNext(curCSV.getColumnNames());
                while(curCSV.moveToNext()) {
                    String arrStr[] ={tripData.get(0),curCSV.getString(1),curCSV.getString(2)};
                    // curCSV.getString(3),curCSV.getString(4)};
                    csvWrite.writeNext(arrStr);
                }*/
                if (tripData.size() > 0) {
                    for (int i = 0; i < tripData.size(); i++) {
                        String arrStr[] = {tripData.get(i).getTimeStamp(), tripData.get(i).getTime(), tripData.get(i).getDate(),
                                tripData.get(i).getEventName(), tripData.get(i).getTripId()};
                        csvWrite.writeNext(arrStr);
                    }
                }

                csvWrite.close();
                //curCSV.close();
                return true;
            } catch (Exception sqlEx) {
                Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
                return false;
            }
        }

        protected void onPostExecute(final Boolean success) {
           /* if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }*/
            if (success) {
                Log.e("uploading > ", "start uploading csv now");
                setCsvToUpload();
            } else {
                Log.e("uploading > ", "csv can't upload");
            }
        }


    }

    /**
     * Here we set CSV for upload on AWS.
     */
    public void setCsvToUpload() {

        File videoToUpload = new File("/storage/emulated/0/ClubD2S/" + PreferenceHelper.getStringPreference(ServiceContext, "csvName"));
        //final String path = getFilePathfromURI(imageUri);
        String key = PreferenceHelper.getStringPreference(ServiceContext, "csvName");
        Log.e("uploading > ", key + " > " + key.substring(0, key.indexOf(".")));
        TransferObserver transferObserver = transferUtility.upload(
                Constants.BUCKET,      //The bucket to upload to
                key,  //The key for the uploaded object
                videoToUpload        //The file where the data to upload exists
                //CannedAccessControlList.PublicRead2
        );

        Log.d("uploading > ", "csv upload in progress" + " > " + key.substring(0, key.indexOf(".")));
        transferObserverListener(transferObserver, PreferenceHelper.getStringPreference(ServiceContext, key.substring(0, key.indexOf("."))), videoToUpload);
    }


    /**
     * Here we calculate total distance travel during trip.
     */
    private void calculateTotalTravelDistance() {

        LocationRequest mLocationRequest;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        // mLocationRequest.setSmallestDisplacement(2);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                if (locationResult.getLastLocation() != null) {
                    Log.d("lastlocations1>>>>>", locationResult.getLastLocation().getLatitude() + " >> " +
                            locationResult.getLastLocation().getLongitude());


                    //   Log.d("lastlocationssss>>>>", locationResult.getLocations().toString());
                    // getAddressFromLocation(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude());

                    locationCollector(locationResult.getLastLocation());

                    Log.d("lastlocation5>>>>>>", locationResult.getLastLocation().getBearing() + " >>> " +
                            locationResult.getLastLocation().bearingTo(locations.get(0)));

                   /* addDataReportWithFirestore(locationResult.getLastLocation().getBearing()+"",
                            locationResult.getLastLocation().bearingTo(locations.get(0))+"",
                            "","","","","");*/



                    /*double x0 = locationResult.getLastLocation().getLatitude();
                    double y0 = locationResult.getLastLocation().getLongitude();

                    Random random = new Random();

                    // Convert radius from meters to degrees
                    double radiusInDegrees = 600000 / 111000f;

                    double u = random.nextDouble();
                    double v = random.nextDouble();
                    double w = radiusInDegrees * Math.sqrt(u);
                    double t = 2 * Math.PI * v;
                    double x = w * Math.cos(t);
                    double y = w * Math.sin(t);

                    // Adjust the x-coordinate for the shrinking of the east-west distances
                    double new_x = x / Math.cos(y0);

                    double foundLatitude = new_x + x0;
                    double foundLongitude = y + y0;
                    LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);
                  //  randomPoints.add(randomLatLng);
                    Location l1 = new Location("");
                    l1.setLatitude(randomLatLng.latitude);
                    l1.setLongitude(randomLatLng.longitude);

                    locationCollector(l1);*/

                    //  Log.d("randomlocation> ", l1.getLatitude() + " > "+ l1.getLongitude());


                }
            }


            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
                Log.d("lastlocation4>>", locationAvailability.isLocationAvailable() + "");
            }
        };

        mFLocationClient = LocationServices.getFusedLocationProviderClient(CameraActivity.this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFLocationClient.getLastLocation().addOnSuccessListener(this, location -> {

            if (location != null) {
                Log.d("lastlocation2 >>>> ", location.getLatitude() + " " + location.getLongitude());
                //getAddressFromLocation(location.getLatitude(), location.getLongitude());
                locationCollector(location);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("lastloaction3>", e.getMessage());
            }
        });


        mFLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper());

    }

    private void getAddressFromLocation(double latitude, double longitude) {

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        Log.d("lastlocation addresss", longitude + " >> " + longitude);

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            Log.d("Address >>> ", address + " >>> " + city + " >> " + state + " >> " + country + " >> " + postalCode + " >> " + knownName);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void checkGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(CameraActivity.this).build();
        googleApiClient.connect();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }

        return true;
    }


    private double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2radd(lat2))
                + Math.cos(deg2radd(lat1))
                * Math.cos(deg2radd(lat2))
                * Math.cos(deg2radd(theta));
        dist = Math.acos(dist);
        dist = rad2degg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double rad2degg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private double deg2radd(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private void locationCollector(Location location) {

        if (locations.size()==0){
            locations.add(location);
        }else {
            if (getDistanceBetweenTwoLocations(locations.get(locations.size()-1).getLatitude(),
                    locations.get(locations.size()-1).getLongitude(),location.getLatitude(),location.getLongitude()) > 0.2){
                locations.add(location);
            }
        }

        //locations.add(location);
        Log.e("uploadinglll>", locations.size() + " >>> " + locations.toString());
    }

    /**
     * This method executes when we try to cancel the ongoing trip.
     */
    private void cancelTrip() {

        PreferenceHelper.cleanPerticualrPref(CameraActivity.this, "tripId");
        onstopFlag = true;
        onDestroyFlag = true;
        onBackPressed();

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        String endTime = timeFormat.format(Calendar.getInstance().getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String date = dateFormat.format(new Date());

        tripEndDate = new Date();
        long totalTripDuration = tripEndDate.getTime() - tripStartDate.getTime();

        //  long tripTimeInHours = TimeUnit.MILLISECONDS.toHours(totalTripDuration);


        /*for (int i = 0; i<50; i++){
                            Random random = new Random();
                            float value = 10 + random.nextFloat() * (63 - 10);
                            Log.d("highest>>",value+"");
                            avgSpeedPointsList.add(value);

            randomLocation(locations.get(0));

            Log.d("distance33>",locations.toString());

        }*/

        double distance = 0;

                        /*if (locations.size()>2){

                            for (int i=0; i<locations.size()-1; i++){
                                distance = distance + getDistance(locations.get(i).getLatitude(), locations.get(i).getLongitude(), locations.get(i + 1).getLatitude(), locations.get(i + 1).getLongitude());
                            }
                        }*/

        if (locations.size() > 2) {

            for (int i = 0; i < locations.size() - 1; i++) {
                distance = distance + getDistanceBetweenTwoLocations(locations.get(i).getLatitude(), locations.get(i).getLongitude(), locations.get(i + 1).getLatitude(), locations.get(i + 1).getLongitude());
            }
        }

        double avgSpeed = distance * 3600000 / totalTripDuration;

        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(1);

        Log.d("distance>>>", distance + " > " + " > " + totalTripDuration + " > " + avgSpeed);
        Log.d("distancemm>> ", df.format(distance) + " > " + totalTripDuration + " > " + df.format(avgSpeed));
        Log.d("distanceloc>>", locations.toString());

        Crashlytics.log(Log.DEBUG, "LOLS", distance + ">> " + " >> " + avgSpeed + " >> " + totalTripDuration);

        Crashlytics.setString("luki", distance + ">> " + " >> " + avgSpeed + " >> " + totalTripDuration);

        Crashlytics.setString("loca", locations.toString());

        // addDataToFireStore(locations);
        //  addDataReportWithFirestore(startingTime,distance+"",convertmiliSecondsToHMmSs(totalTripDuration),
        //          df.format(avgSpeed), hardBreakCounter+"",overSpeedCounter+"",endTime);


        Intent intent = new Intent(CameraActivity.this, TripReport.class);
        intent.putExtra("startTime", startingTime);
        intent.putExtra("distance", df.format(distance) + "" + " Km");
        intent.putExtra("time", convertmiliSecondsToHMmSs(totalTripDuration));
        intent.putExtra("speed", df.format(avgSpeed) + "" + " Km/h");
        intent.putExtra("hardbrake", hardBreakCounter + "");
        intent.putExtra("overSpeed", overSpeedCounter + "");
        intent.putExtra("endTime", endTime);
        intent.putExtra("currentDate", date);
        intent.putExtra("speedPoints", avgSpeedPointsList);
        intent.putExtra("startTimeMills", startTimeInMills);
        intent.putExtra("sharpTurn", gyroCounter + " ");
        double ddd = TimeUnit.MILLISECONDS.toHours(totalTripDuration);
        intent.putExtra("durationInHours", ddd + "");

        double durr = ((totalTripDuration / (1000 * 60 * 60)) % 24);

        Log.d("duration>", durr + ">>> " + totalTripDuration + " >>> " + ddd);

        PreferenceHelper.setStringPreference(CameraActivity.this, "reportway", "camera");

        startActivity(intent);
        finish();
    }


    /**
     * Adding speed to arraylist.
     */
    private void calculateSpeedPoints() {
            /*if (allSpeedArray.size() < 15) {
                allSpeedArray.add(Speed);
            } else {

                double sum = 0;
                for(i = 1; i < allSpeedArray.size(); i++)
                    sum += allSpeedArray.get(i);
                float avgSpeed =  (float)sum/allSpeedArray.size();
                avgSpeedPointsList.add(avgSpeed);
                allSpeedArray.clear();
            }*/

        if (speedHolder != Speed) {
            avgSpeedPointsList.add((float) Speed);
            speedHolder = Speed;
        }


        // Log.d("sspeeding>>", allSpeedArray.size()+ " >> "+ Speed );
        // Log.d("sspeeding111>", avgSpeedPointsList.toString() + ">" + Speed);

    }



    private void cutCall(String number) {
        TelecomManager tm = (TelecomManager) this.getSystemService(Context.TELECOM_SERVICE);

        Log.e("uploading333333>","incutcall");

        if (tm != null) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ANSWER_PHONE_CALLS) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                boolean success = tm.endCall();
                Log.e("uploading222222>","sendingsms");
                sendSMS(number);

            }
            // success == true if call was terminated.
        }
    }


    public void declinePhone(String number) throws Exception {

        try {

            Log.e("uploading7777>","inside decline");

            String serviceManagerName = "android.os.ServiceManager";
            String serviceManagerNativeName = "android.os.ServiceManagerNative";
            String telephonyName = "com.android.internal.telephony.ITelephony";
            Class<?> telephonyClass;
            Class<?> telephonyStubClass;
            Class<?> serviceManagerClass;
            Class<?> serviceManagerNativeClass;
            Method telephonyEndCall;
            Object telephonyObject;
            Object serviceManagerObject;
            telephonyClass = Class.forName(telephonyName);
            telephonyStubClass = telephonyClass.getClasses()[0];
            serviceManagerClass = Class.forName(serviceManagerName);
            serviceManagerNativeClass = Class.forName(serviceManagerNativeName);
            Method getService = // getDefaults[29];
                    serviceManagerClass.getMethod("getService", String.class);
            Method tempInterfaceMethod = serviceManagerNativeClass.getMethod("asInterface", IBinder.class);
            Binder tmpBinder = new Binder();
            tmpBinder.attachInterface(null, "fake");
            serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
            IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
            Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);
            telephonyObject = serviceMethod.invoke(null, retbinder);
            telephonyEndCall = telephonyClass.getMethod("endCall");
            telephonyEndCall.invoke(telephonyObject);

            Log.e("uploading6544>","callends");

            sendSMS(number);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("unable", "msg cant dissconect call....");

        }
    }


    public void incomingCallTimer(String number) {
        incomingCallTimer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.e("uploading9877>","tick");
            }

            @Override
            public void onFinish() {
                try {
                    declinePhone(number);
                    cutCall(number);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }


    public class detectCalls extends IncomingCallReceiver {

        @Override
        protected void onIncomingCallReceived(Context ctx, String number, Date start) {
            Log.e("uploading12>>", number);
            incomingCallTimer(number.replaceAll("[-+.^:,]",""));
        }

        @Override
        protected void onIncomingCallAnswered(Context ctx, String number, Date start) {
            Log.e("uploading123>>", number);
        }

        @Override
        protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
            Log.e("uploading1234>>", number);
        }

        @Override
        protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
            Log.e("uploading12345>>", number);
        }

        @Override
        protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
            Log.e("uploading123456>>", number);
        }

        @Override
        protected void onMissedCall(Context ctx, String number, Date start) {
            Log.e("uploading>>", number);
        }
    }


    /**
     * Here we calculate distance between two points.
     * @param startLat
     * @param startLong
     * @param endLat
     * @param endLong
     * @return
     */
    private double getDistanceBetweenTwoLocations(double startLat, double startLong, double endLat, double endLong) {
        float[] result = new float[10];
        Location.distanceBetween(startLat, startLong, endLat, endLong, result);
        double distatnce = ((double) result[0]) / 1000;
        // Log.d("distancedou>", (double)result[0] + " > " + distatnce+" > "+ round(distatnce,2) + " > " + BigDecimal.valueOf(distatnce).setScale(0,RoundingMode.HALF_UP));
        //  return (BigDecimal.valueOf(distatnce).setScale(0,RoundingMode.HALF_UP)).doubleValue();
        return distatnce;
    }

    private double makeValueNormalNumber(double value) {
        return (BigDecimal.valueOf(value).setScale(0, RoundingMode.HALF_UP)).doubleValue();
    }

    private void randomLocation(Location location) {
       /* double x0 = locationResult.getLastLocation().getLatitude();
        double y0 = locationResult.getLastLocation().getLongitude();*/

        Log.d("distance1>", location.getLatitude() + " > " + location.getLongitude());

        double x0 = location.getLatitude();
        double y0 = location.getLongitude();
        Random random = new Random();

        // Convert radius from meters to degrees
        double radiusInDegrees = 600000 / 111000f;

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        double new_x = x / Math.cos(y0);

        double foundLatitude = new_x + x0;
        double foundLongitude = y + y0;
        LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);
        //  randomPoints.add(randomLatLng);
        Location l1 = new Location("");
        l1.setLatitude(randomLatLng.latitude);
        l1.setLongitude(randomLatLng.longitude);

        locationCollector(l1);
    }


    private void addDataToFireStore(ArrayList<Location> locations) {
        Map<String, Object> location = new HashMap<>();
        for (int i = 0; i < locations.size(); i++) {
            location.put(i + "", locations.get(i).getLatitude() + " -- " + locations.get(i).getLongitude());
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.US);
        firestore.collection("ClubD2S").document("TripData").collection("locations")
                .document(dateFormat.format(new Date()))
                .set(location)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("firestore>>", "success");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("firestore>>", "success");
                    }
                });

    }





    private void calculateDistance() {


        firestore.collection("ClubD2S").document("TripData").collection("locations")
                .document("25-02-2019 05:22:12")
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()) {
                    Log.d("firebaseData>>>", "success");
                    double distance1 = 0;

                    for (int i = 0; i < task.getResult().getData().size() - 1; i++) {

                        String lat = task.getResult().getData().get(i + "").toString().split(" -- ")[0];
                        String lng = task.getResult().getData().get(i + "").toString().split(" -- ")[1];

                        String lat1 = task.getResult().getData().get((i + 1) + "").toString().split(" -- ")[0];
                        String lng1 = task.getResult().getData().get((i + 1) + "").toString().split(" -- ")[1];

                        double dLat = Double.parseDouble(lat);
                        double dLng = Double.parseDouble(lng);
                        double dlat1 = Double.parseDouble(lat1);
                        double dlng1 = Double.parseDouble(lng1);

                        Log.d("latylong>>", dLat + " >> " + dLng + " >> " + dlat1 + " >>" + dlng1);

                        distance1 = distance1 + getDistanceBetweenTwoLocations(dLat, dLng, dlat1, dlng1);

                    }

                    Log.d("latylong>>", distance1 + "");
                } else {
                    Log.d("firebaseData>>>", "failure");
                }

            }
        });
    }


    /**
     * Get path of video file.
     * @return
     */
    private String getAbsolutePath() {
        String timeStamp = ((Long) (System.currentTimeMillis() / 1000)).toString();
        String phoneNumber = getSharedPreferences("LOGIN_SESSION_ACTIVE", MODE_PRIVATE).getString("PHONE", "") + "_";
        File file = new File(recordfilepath(), phoneNumber + timeStamp + ".avi");
     //   File file = new File(recordfilepath(), "7023210204_1561634640" + ".avi");
        Log.d(TAG, "file : " + file);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        saveVideo = file.getAbsolutePath();
        return saveVideo;
    }


    private void countCameraFPS(){
        frameCounter++;

        while ((System.currentTimeMillis() - startTime1) > 2000) {
            Log.d("fpsssss>>>", frameCounter + "");
            startTime1 = System.currentTimeMillis();
            frameCounter = 0;
        }
    }

    private void startService(){
        Intent intent = new Intent(CameraActivity.this, TrackingServices.class);
        startService(intent);
    }


    /**
     * Calculating Sharp turn using Gyro.
     */
    private void checkGyroscope(){

         sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
         sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

         eventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                // timestamp = event.timestamp;
              //  Log.d("timeStamp>>>",timestamp+"");

                if (timestamp != 0) {
                    final float dT = (event.timestamp - timestamp) * NS2S;
                    // Axis of the rotation sample, not normalized yet.
                    float axisX = event.values[0];
                    float axisY = event.values[1];
                    float axisZ = event.values[2];

                    // Calculate the angular speed of the sample
                    double omegaMagnitude = Math.sqrt(axisX * axisX + axisY * axisY + axisZ * axisZ);

                    if (oMagnitude.size()<10){
                        oMagnitude.add(omegaMagnitude);
                    }else {
                        oMagnitude.remove(0);
                        oMagnitude.add(omegaMagnitude);
                    }

                    // Normalize the rotation vector if it's big enough to get the axis
                    // (that is, EPSILON should represent your maximum allowable margin of error)
                    if (omegaMagnitude > 0.000000001f) {
                        axisX /= omegaMagnitude;
                        axisY /= omegaMagnitude;
                        axisZ /= omegaMagnitude;
                    }

                    checkGyro();


                    double thetaOverTwo = omegaMagnitude * dT / 2.0f;
                    float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
                    float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
                    deltaRotationVector[0] = sinThetaOverTwo * axisX;
                    deltaRotationVector[1] = sinThetaOverTwo * axisY;
                    deltaRotationVector[2] = sinThetaOverTwo * axisZ;
                    deltaRotationVector[3] = cosThetaOverTwo;

                  //  Log.d("rotationvector> ", deltaRotationVector[0]+ " > "+deltaRotationVector[1] + " > "+
                         //   deltaRotationVector[2]+" > "+deltaRotationVector[3]);

                  //  Log.d("rotationvector>1 ",axisX+" > "+axisY+" > "+axisZ + " > "+ omegaMagnitude);

                    /*writeDataToCSV(axisX+"",axisY+"",axisZ+"",omegaMagnitude+"",
                            deltaRotationVector[0]+"",deltaRotationVector[1]+"",
                            deltaRotationVector[2]+"",deltaRotationVector[3]+"");*/

                }

                timestamp = event.timestamp;
                float[] deltaRotationMatrix = new float[9];
                SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);


            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        sensorManager.registerListener(eventListener,sensor,SensorManager.SENSOR_DELAY_NORMAL);

    }

    /**
     * Setting Sharp turn event here.
     */
    private void checkGyro(){

       /* Random r = new Random();
        double randomValue = 10 + (40 - 10) * r.nextDouble();*/

      //  avgSpeedArray.add(randomValue);

        double avgSpeed = 0,avgGyro = 0;

        if (avgSpeedArray!=null && avgSpeedArray.size()>0) {
            double sum = 0;
            for (Double d : avgSpeedArray)
                sum += d;
             avgSpeed = sum / 10;
        }


        if (oMagnitude.size()>0) {
            double gyroValue = 0;
            for (Double dd : oMagnitude)
                gyroValue += dd;
             avgGyro = gyroValue / 10;
        }

      //  Log.d("gyro>>> ", avgSpeed + " >> " + avgGyro);


        if (avgSpeed > 40 && avgGyro>1.3){
            gryoEvent = "Sharp Turn";
            gyroTimer();
        }

    }


    /**
     * Timer for wait for 3 second after sharp turn event.
     */
    private void gyroTimer() {
        if (!isGyroTimerRunning) {

            runOnUiThread(new Runnable() {
                public void run() {

                    countDownTimer = new CountDownTimer(3000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            isGyroTimerRunning = true;
                        }

                        public void onFinish() {
                            isGyroTimerRunning = false;
                           // saveEventInDatabase(brakeEvent);
                            gyroCounter++;
                            gryoEvent = " ";
                        }
                    }.start();

                }
            });
        }
    }


    /**
     * For sending SMS....... Not in use currently.
     * @param number
     */
    private void sendSMS(String number){

       /* Call<LoginResponse> call = RetrofitApiClient.getRetrofit().create(ApiInterface.class)
                .login("login_initiate_webservice.php?mobile_no=" + ph);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {*/


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl("http://api.msg91.com/api/")
                .build();

                SmsInterface call1 =  retrofit.create(SmsInterface.class);
       /* try {
            Response<String> stringCall = call1.getStringResponse().execute();
            Log.e("uploading333333>",stringCall.body());
        } catch (IOException e) {
            e.printStackTrace();
        }*/


            Call<String> stringCall = call1.getStringResponse("http://api.msg91.com/api/sendhttp.php?route=4&sender=CLUBDS&mobiles="+number+"&authkey=276408A34tOG0XcEg5cd9302e&message=Hi I am driving right now. I will call you later.&country="+GetCountryZipCode());
            stringCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()){
                        Log.e("uploading555555>",response.body().toString());
                        Log.e("uploading999999>","http://api.msg91.com/api/sendhttp.php?route=4&sender=CLUBDS&mobiles="+number+"&authkey=276408A34tOG0XcEg5cd9302e&message=Hi I am driving right now. I will call you later.&country="+GetCountryZipCode());
                    }else {
                        Log.e("uploading555555>","nosuccess");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {

                }
            });



               /*stringCall.enqueue(new Callback<String>() {
                   @Override
                   public void onResponse(Call<String> call, Response<String> response) {

                       if (response.isSuccessful()){
                           Log.e("uploading555555>",response.body().toString());
                       }else {
                           Log.e("uploading555555>","nosuccess");
                       }

                   }

                   @Override
                   public void onFailure(Call<String> call, Throwable t) {

                   }
               });

        try {
            Log.e("uploading444444>","execute");
            stringCall.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }*/





               /* //   mDialog.dismiss();
                mProgressDialog.dismiss();
                if (response.body() != null) {

                    Log.d(TAG, "Got response!");
                    if (response.body() != null) {
                        if(response.body().getLoginResponse().get(0).getQueryResponse().getPassword() != null) {
                            String pswd = response.body().getLoginResponse().get(0).getQueryResponse().getPassword();
                            String status = response.body().getLoginResponse().get(0).getQueryResponse().getStatus();
                            Log.d(TAG, "password: " + pswd);
                            Log.d(TAG, "status: " + status);
                            if ((status.equalsIgnoreCase("Login successful.")) ||
                                    (status.equalsIgnoreCase("Password already sent."))) {
                                if (Utils.isNetworkAvailable(LoginActivity.this)) {
                                    otpProcess(pswd);
                                } else {
                                    Toast.makeText(LoginActivity.this, getString(R.string.connect_to_internet), Toast.LENGTH_SHORT).show();

                                }
                            }

                        }

                        else{
                            Toast.makeText(LoginActivity.this, response.body().getLoginResponse().get(0).getQueryResponse().getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }*/
           // }

            /*@Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                //     mDialog.dismiss();
                mProgressDialog.dismiss();
                Log.e("TAG", t.toString());
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();


            }
        });*/

    }


    public String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }



   /* public static boolean isValidMobileNumber(String phone) {
        if (TextUtils.isEmpty(phone))
            return false;
        final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        try {
            PhoneNumberUtils phoneNumber = phoneNumberUtil.parse(phone, Locale.getDefault().getCountry());
            PhoneNumberUtils.isM phoneNumberType = phoneNumberUtil.getNumberType(phoneNumber);
            return phoneNumberType == PhoneNumberUtil.PhoneNumberType.MOBILE;
        } catch (final Exception e) {
        }
        return false;
    }*/


   private void compressVideoSize(String videoPath){
       GiraffeCompressor.create()
               .input(videoPath)
               .output(getAbsolutePath1())
               .bitRate(2073600)
               .resizeFactor(1.0f)
               .ready().
               doOnSubscribe(new Action0() {
                   @Override
                   public void call() {

                   }
               })
               .observeOn(AndroidSchedulers.mainThread())
               .subscribe(new Subscriber<GiraffeCompressor.Result>() {
                   @Override
                   public void onCompleted() {
                       Log.e("video","onstart");

                   }

                   @Override
                   public void onError(Throwable e) {
                       Log.e("video","onerror");

                   }

                   @Override
                   public void onNext(GiraffeCompressor.Result result) {
                       Log.e("video","onnext");
                   }
               });
   }


    private String getAbsolutePath1() {
        String timeStamp = ((Long) (System.currentTimeMillis() / 1000)).toString();
        String phoneNumber = getSharedPreferences("LOGIN_SESSION_ACTIVE", MODE_PRIVATE).getString("PHONE", "") + "_";
        File file = new File(Imagesrecordfilepath(), phoneNumber + timeStamp + ".avi");
        Log.d(TAG, "file : " + file);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
       // saveVideo = file.getAbsolutePath();
        return saveVideo;
    }


}