package hitech.robotic.systemzz;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HorusClient {
    private static Retrofit retrofit = null;
    private static String BASE_URL = "http://ec2-52-42-94-192.us-west-2.compute.amazonaws.com:8080/";

    public static Retrofit getClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
