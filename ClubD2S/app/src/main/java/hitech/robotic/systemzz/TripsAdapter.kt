package hitech.robotic.systemzz

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hitech.robotic.systemzz.utils.PreferenceHelper
import kotlinx.android.synthetic.main.trip_list_row.view.*

/**
 * This is adapter class for old trips. It shows old trips in list.
 */

class TripsAdapter(val tripList:ArrayList<MutableMap<String,String?>>, val context: Context): RecyclerView.Adapter<TripsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
       return ViewHolder(LayoutInflater.from(context).inflate(R.layout.trip_list_row, p0,false))
    }

    override fun getItemCount(): Int {
        return tripList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        holder.tripNumber?.text = tripList[p1]["tripId"]
        holder.tripDate.text = tripList[p1]["currentDate"]
        holder.tripTiming.text = tripList[p1]["startTime"]
        holder.tripRow.setOnClickListener {
            PreferenceHelper.setStringPreference(context, "reportway", "trip")
            val intent = Intent(context,TripReport::class.java)
            intent.putExtra("all_data",tripList)
            intent.putExtra("position",p1)
            context.startActivity(intent)
        }
    }

    class ViewHolder(view:View):RecyclerView.ViewHolder(view){
        val tripNumber = view.trip_number
        val tripDate = view.trip_date
        val tripTiming = view.trip_timing
        val tripRow = view.trip_row
    }

}