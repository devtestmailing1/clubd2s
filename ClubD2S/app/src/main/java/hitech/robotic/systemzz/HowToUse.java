package hitech.robotic.systemzz;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ScrollView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * This class set "How to use" page.
 */

public class HowToUse extends AppCompatActivity {

    private ScrollView scrollView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.how_to_use);
        initializeView();
    }

    private void initializeView(){
        scrollView = findViewById(R.id.scroll_view);

        Animation slideIn = AnimationUtils.loadAnimation(HowToUse.this,R.anim.move_left_in_activity);
        scrollView.startAnimation(slideIn);

    }

}
