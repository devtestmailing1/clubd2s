package hitech.robotic.systemzz;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.collect.Multiset;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.core.FirestoreClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import kotlin.jvm.internal.markers.KMutableMap;

public class LeaderBoard extends AppCompatActivity {

    FirebaseFirestore firebaseFirestore;
    HashMap<String,Integer>hashMap = new HashMap<>();
    ArrayList<HashMap<String,Double>>allData = new ArrayList<>();
    double totalScore = 0, totalDistance = 0;
    int flag = 0, totalData = 0, percent=0, percentHolder =0, userOnLeaderBoard=0;

    private RecyclerView recyclerView;
    private String phone, userPhoneNumber;
    private LinearLayoutCompat linearLayoutCompat;
    private ImageView carr;
    private TextView percentage;
    private Task<QuerySnapshot> task;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaderboard);
        intilizeViews();
    }

    private void intilizeViews(){

        SharedPreferences sharedPreferences = getSharedPreferences("LOGIN_SESSION_ACTIVE",MODE_PRIVATE);
        userPhoneNumber = sharedPreferences.getString("PHONE",null);

        linearLayoutCompat = findViewById(R.id.progress_layout);
        linearLayoutCompat.setVisibility(View.VISIBLE);
        carr = findViewById(R.id.car_icon);
        percentage = findViewById(R.id.percentage);
        animateView(carr);


        recyclerView = findViewById(R.id.recycler_view);
        firebaseFirestore = FirebaseFirestore.getInstance();
       getAllPhoneNumbers();
      //  getData();
    }


    private void getAllPhoneNumbers(){

        firebaseFirestore.collection("PhoneNumbers").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()){
                        totalData = task.getResult().getDocuments().size();
                        Log.e("leader44>",task.getResult().getDocuments().size()+" > "+ task.getResult().getDocuments().get(0).getId());
                        phone = task.getResult().getDocuments().get(flag).getId();

                        if (flag==0) {
                            percent = 100/totalData;
                            percentage.setText("Loading.. "+ percent+"%");
                        }else {
                            percentHolder = percentHolder+percent;
                            percentage.setText("Loading.. "+percentHolder+"%");
                        }

                        if (flag<totalData){
                            flag = flag+1;
                            getData();
                        }
                    }
            }
        });

    }


    private void getData(){

        totalScore = 0;
        totalDistance = 0;

        firebaseFirestore.collection("ClubD2S").document("TripData").collection("report")
                .document(phone).collection("all_report").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    Log.e("leader8>",task.getResult().getDocuments().size()+" ");
                    LeaderBoard.this.task = task;
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String finalScore = document.getString("FinalScore");
                        String distance = document.getString("distance");
                        if (finalScore!=null){
                            totalScore = totalScore + Double.valueOf(finalScore);
                        }
                        if (distance!=null){
                            totalDistance = totalDistance+Double.valueOf(distance);
                        }
                      //  Log.e("leader66>",document.getString("FinalScore")+" >");

                    }
                    double avgScore = totalScore/task.getResult().getDocuments().size();
                    if (avgScore<0){
                        avgScore = 0;
                    }
                    Log.e("leader33>",avgScore+" > "+ totalScore +" >> "+phone + " >> "+ task.getResult().getDocuments().size() + " >>> " + totalDistance );
                    if (totalDistance>20 || (totalDistance>0 && phone.equalsIgnoreCase(userPhoneNumber)) ) {
                        hashMap.put("*****" + phone.substring(6, 10), (int) avgScore);
                        Log.e("input>","take input");

                        if (totalDistance>0.5){
                            Log.e("distance >> ","jyada");
                        }

                    }else {
                        if (totalDistance<0.5){
                            Log.e("deleted>","deleted");
                            totalData = totalData-1;
                            /*firebaseFirestore.collection("ClubD2S").document("TripData").collection("report")
                                    .document(phone).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.e("delete>>>","successfully");
                                }
                            });*/

                            for (QueryDocumentSnapshot documentSnapshot : LeaderBoard.this.task.getResult()){
                                documentSnapshot.getReference().delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.e("delete>>>","sfully");
                                    }
                                });
                            }


                            firebaseFirestore.collection("PhoneNumbers").document(phone).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.e("delete>>>","successfullly");
                                }
                            });
                        }
                    }

                    if (phone.equalsIgnoreCase(userPhoneNumber) && totalDistance>0.5 ){
                        userOnLeaderBoard = 1;
                    }

                }

                if (flag!=totalData) {
                    getAllPhoneNumbers();
                }

                Log.e("sizesss>",totalData +" > "+flag);
                if (totalData==flag){
                    linearLayoutCompat.setVisibility(View.GONE);
                    setAdapter();
                }
            }
        });



    }

    private void setAdapter(){

        if (userOnLeaderBoard == 0 && userPhoneNumber!=null){
            hashMap.put("*****" + userPhoneNumber.substring(6,10),0);
        }

        if (userPhoneNumber==null){
            userPhoneNumber = "**********";
        }

        Log.e("hash>",hashMap.toString());
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        LeaderBoardAdapter boardAdapter = new LeaderBoardAdapter(sortByValue(hashMap), "*****" +userPhoneNumber.substring(6,10),LeaderBoard.this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(boardAdapter);
    }


    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order)
    {

        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                if (order)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }



    private void animateView(ImageView button) {

        Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
        animation.setDuration(1000); //1 second duration for each animation cycle
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
        animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
        button.startAnimation(animation); //to start animation
    }


    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer> > list =
                new LinkedList<Map.Entry<String, Integer> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }


}
