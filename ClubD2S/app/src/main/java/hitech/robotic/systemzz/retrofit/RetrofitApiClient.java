package hitech.robotic.systemzz.retrofit;

import android.content.Context;


import java.io.IOException;
import java.util.concurrent.TimeUnit;
import hitech.robotic.systemzz.utils.Constants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by shared on 14/2/17.
 */

public class RetrofitApiClient implements Constants {


    // private static final String BASE_URL = "http://demo7.mobdigi.com/index.php/api/";
    private volatile static Retrofit retrofit = null;
    private volatile static Retrofit retrofitObj = null;
    private static Context mContext;
    // Add the interceptor to OkHttpClient
    // Define the interceptor, add authentication headers


    private static Interceptor interceptor = new Interceptor() {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request newRequest = chain.request().newBuilder().addHeader("X-AUTH-HEADER", "admin")
                   // .addHeader("Content-Type", "application/json")
                    .addHeader("adminRequest", "true")
                    .build();

           /*Request request = chain.request();

            List<String> customAnnotations = request.headers().values("@");
            if(customAnnotations.contains("@")) {
                // do something with the "custom annotations"

                request = request.newBuilder().removeHeader("@").build();
                return chain.proceed(request);
            }
            else
            {
                request.newBuilder().addHeader("x-api-key", "INW8MEDSX7134C").build();
                return chain.proceed(request);
            }*/

            return chain.proceed(newRequest);
        }
    };

    public RetrofitApiClient() {

        if (retrofit != null) {
            throw new IllegalStateException("Singleton already constructed");
        }
        if (retrofit == null) {
            throw new IllegalStateException("Construstor can't be used in Singleton");
        }
    }

    public static Retrofit getClient() {

        if (retrofitObj == null) {

            synchronized (RetrofitApiClient.class) {


                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.interceptors().add(interceptor);
                builder.connectTimeout(2, TimeUnit.MINUTES)
                        .writeTimeout(2, TimeUnit.MINUTES)
                        .readTimeout(2, TimeUnit.MINUTES);
                OkHttpClient client = builder.build();


                retrofitObj = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();
            }
        }
        return retrofitObj;
    }

    public static Retrofit getRetrofit() {
        if (retrofit == null) {

            synchronized (RetrofitApiClient.class) {

                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
        }
        return retrofit;
    }



    public static Retrofit getRetrofitSMS() {
        if (retrofit == null) {

            synchronized (RetrofitApiClient.class) {

                retrofit = new Retrofit.Builder()
                        .baseUrl(SMSURL)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .build();
            }
        }
        return retrofit;
    }


    public static void getApplicationContextHere(Context context) {
        mContext = context;
    }


}


