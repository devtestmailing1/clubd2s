package hitech.robotic.systemzz;

import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Reg extends AppCompatActivity {

    public static final String TAG="Register";
    Button registerbtn,Otpbtn;
    public EditText et_name,et_password,et_cpassword,et_number,et_anumber,et_email,et_Otp;
    public String sName,sPassword="",sCPassword,sNumber="",sANumber,sEmail="",sDevicid,sOtp="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        et_name= findViewById(R.id.edit_user_name);
        et_email= findViewById(R.id.edit_email_id);
        et_number= findViewById(R.id.edit_mobile_no);

        registerbtn= findViewById(R.id.btn_registration);

        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
    }


    public void onsignupSucess(){
        //do uploading here
        //Toast.makeText(getApplicationContext(),"SignupSucess",Toast.LENGTH_SHORT).show();
    }


    public void register(){
        initialize();
        Log.d(TAG, "onCreate: initialized sucessfully");
        if(!validate())
        {
            Toast.makeText(this,"Register has failed!, Complete the missing Fileds", Toast.LENGTH_SHORT).show();
        } else
        {
            onsignupSucess();
        }


    }

    public void initialize(){
        sName=et_name.getText().toString();
        sNumber=et_number.getText().toString().trim();
        sEmail=et_email.getText().toString().trim();
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }


    public String getsNumber() {
        return sNumber;
    }

    public void setsNumber(String sNumber) {
        this.sNumber = sNumber;
    }


    public String getsEmail() {
        return sEmail;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }
    public boolean validate(){
        boolean valid=true;
        if(sName.isEmpty()||sName.length()>32)
        {
            et_name.setError("Please enter valid Name");
            valid=false;
        }


        if(sNumber.isEmpty()||sNumber.length()>10 || sNumber.length()<10)
        {
            et_number.setError("Please enter Valid Phone Number");
            valid=false;
        }

        if(sEmail.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(sEmail).matches())
        {
            et_email.setError("Please enter valid Email");
            valid=false;
        }

        return valid;
    }
}
