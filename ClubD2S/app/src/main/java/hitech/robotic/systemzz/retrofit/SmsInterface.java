package hitech.robotic.systemzz.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface SmsInterface {
    @GET()
    Call<String>getStringResponse(@Url String url);
}
