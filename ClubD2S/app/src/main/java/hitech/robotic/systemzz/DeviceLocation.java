package hitech.robotic.systemzz;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceLocation {
    /**
     * device : 1
     * speed : 13.000000
     * rpm : 12.000000
     * latitude : 12.000000
     * longitude : 12.000000
     * timestamp : 2017-10-04T14:07:44Z
     */

    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("unixTime")
    @Expose
    private String unixTime;


    public DeviceLocation(String devid, String lat, String lng, String tp, String ts) {
        this.deviceId = devid;
        this.latitude = lat;
        this.longitude = lng;
        this.unixTime = ts;
        this.type = tp;
        Log.d("DeviceLocation", "devid : "+ deviceId + " lat : " + latitude + " lng : " + longitude + " type : " + type + " timestamp : " + unixTime);
    }

    public String getDeviceId() {
        return deviceId;
    }

/*    public void setDevice(String devid) {
        this.deviceId = devid;
    }*/

    public String getLatitude() {
        return latitude;
    }

    /*public void setLatitude(String lat) {
        this.latitude = lat;
    }*/

    public String getLongitude() {
        return longitude;
    }

    /*public void setLongitude(String lng) {
        this.longitude = lng;
    }*/

    public String getTimestamp() {
        return unixTime;
    }

    /*public void setTimestamp(String ts) {
        this.unixTime = ts;
    }*/

    public String getType() {
        return type;
    }

    /*public void setType(String type) {
        this.type = type;
    }*/
}
