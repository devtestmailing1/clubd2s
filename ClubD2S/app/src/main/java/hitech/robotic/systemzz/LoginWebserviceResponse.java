package hitech.robotic.systemzz;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginWebserviceResponse {
    @SerializedName("response")
    public List<LoginResponseResult> loginResponse;

    public List<LoginResponseResult> getLoginResponse() {
        return loginResponse;
    }

}
