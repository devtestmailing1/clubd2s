package hitech.robotic.systemzz;

import com.google.android.gms.maps.model.LatLng;

public interface LatLngPass {
    public void onDatapass(LatLng latLng);
}
