package hitech.robotic.systemzz.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import hitech.robotic.systemzz.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TwitterFragment extends androidx.fragment.app.Fragment {


    private TextView progressBarText;

    public TwitterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,
                container, false);

        WebView wv1 = view.findViewById(R.id.web_view);
        progressBarText = view.findViewById(R.id.progress_bar_text);
        progressBarText.setVisibility(View.VISIBLE);
        wv1.setVisibility(View.VISIBLE);
        wv1.loadUrl("https://twitter.com/1CroreSteps");
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.getSettings().setDomStorageEnabled(true);
        wv1.getSettings().setSaveFormData(true);
        wv1.getSettings().setAllowContentAccess(true);
        wv1.getSettings().setAllowFileAccess(true);
        wv1.canGoBack();
        wv1.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == MotionEvent.ACTION_UP
                        && wv1.canGoBack()) {
                    wv1.goBack();
                    return true;
                }
                return false;
            }
        });

        wv1.getSettings().setAllowFileAccessFromFileURLs(true);
        wv1.setWebViewClient(new WebViewClient());
        wv1.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                progressBarText.setVisibility(View.GONE);
                return true;

            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                progressBarText.setVisibility(View.GONE);
            }
        });

        return view;
    }

}
