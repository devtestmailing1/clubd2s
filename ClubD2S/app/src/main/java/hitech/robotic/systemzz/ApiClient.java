package hitech.robotic.systemzz;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class set the base URL for all apis.
 */

public class ApiClient {

    private static Retrofit retrofit = null;
    private static String BASE_URL = "https://www.onecroresteps.com/";

    /**
     * Setting Base URL here
     * for all APIs
     */

    public static Retrofit getClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
