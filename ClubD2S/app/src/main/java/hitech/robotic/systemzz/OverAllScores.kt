package hitech.robotic.systemzz

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class OverAllScores:AppCompatActivity() {

    private lateinit var firestore: FirebaseFirestore;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.trip_report_new)
    }

    private fun getAllData(){
        firestore = FirebaseFirestore.getInstance()
        val sharedPref =  getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE)
        val userPhone = sharedPref.getString("PHONE",null)
        val reportList = ArrayList<MutableMap<String,String?>>()

        firestore.collection("ClubD2S").document("TripData").collection("report")
                .document(userPhone).collection("all_report").get().addOnSuccessListener { documentSnapshot ->
                    if (documentSnapshot!=null){
                        for(documents in documentSnapshot){
                            val readWriteMaps = mutableMapOf<String,String?>()
                            readWriteMaps["FinalScore"] = documents.getString("FinalScore")
                            readWriteMaps["avgSpeed"] = documents.getString("avgSpeed")
                            readWriteMaps["distance"] = documents.getString("distance")
                            readWriteMaps["duration"] = documents.getString("duration")
                            readWriteMaps["endTime"] = documents.getString("endTime")
                            readWriteMaps["hardBreakPerThousand"] = documents.getString("hardBreakPerThousand")
                            readWriteMaps["hardbreak"] = documents.getString("hardbreak")
                            readWriteMaps["overSpeed"] = documents.getString("overSpeed")
                            readWriteMaps["overSpeedPerThousand"] = documents.getString("overSpeedPerThousand")
                            readWriteMaps["percentHardBreak"] = documents.getString("percentHardBreak")
                            readWriteMaps["percentOverSpeed"] = documents.getString("pecentOverSpeed")
                            readWriteMaps["percentPhoneUsages"] = documents.getString("percentPhoneUsages")
                            readWriteMaps["percentSharpTurn"] = documents.getString("perentSharpTurn")
                            readWriteMaps["phoneUsages"] = documents.getString("phoneUsages")
                            readWriteMaps["phoneUsagesPerThousand"] = documents.getString("phoneUsagesPerThousand")
                            readWriteMaps["sharpTurn"] = documents.getString("sharpTurn")
                            readWriteMaps["sharpTurnPerThousand"] = documents.getString("sharpTurnPerThousand")
                            readWriteMaps["startTime"] = documents.getString("startTime")
                            readWriteMaps["currentDate"] = documents.getString("currentDate")
                            readWriteMaps["tripId"] = documents.getString("tripId")
                            reportList.add(readWriteMaps)
                        }
                    }
                }
    }
}