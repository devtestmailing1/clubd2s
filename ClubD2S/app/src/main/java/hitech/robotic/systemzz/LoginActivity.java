package hitech.robotic.systemzz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hitech.robotic.systemzz.retrofit.ApiInterface;
import hitech.robotic.systemzz.retrofit.RetrofitApiClient;
import hitech.robotic.systemzz.utils.PreferenceHelper;
import hitech.robotic.systemzz.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This class defines user login page of app.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    String ph, password;

    @BindView(R.id.edit_mobile_no)
    EditText editPhoneNumber;
    @BindView(R.id.btn_login)
    Button btnOTP;
    @BindView(R.id.newUser)
    TextView newUser;
    ApiInterface apiInterface;
    RadioGroup radioGroup;
    Spinner selectCompany;
    int loginType = 1;
    String companyName;

    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(LoginActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);

        radioGroup = findViewById(R.id.radio_group);
        selectCompany = findViewById(R.id.select_company);
        String [] companies = new String[]{"Select Your Company","AIMA","ASDC","CII","FADA","IRSC","SIAM","ACMA",
        "Apollo Tyres Ltd", "Mahindra Rise", "Hella India Lighting Ltd", "Hella India Automotive Pvt Ltd", "WABCO India Ltd",
        "Brakes India Pvt Ltd", "AITWA"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, companies);

        selectCompany.setAdapter(adapter);

        SpannableString ss = new SpannableString("New User? Register here!");
        /*ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                // Toast.makeText(LoginActivity.this, "Clickable", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };*/
        ss.setSpan(new ForegroundColorSpan(Color.BLUE), 10, 24, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        newUser.setText(ss);
        newUser.setMovementMethod(LinkMovementMethod.getInstance());
        newUser.setHighlightColor(Color.TRANSPARENT);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        ph = DBHandlerWalk.Instance(getApplicationContext()).getPhoneNum();
        if (ph != "") {
            editPhoneNumber.setText(ph);
        } else {
            editPhoneNumber.setFocusable(true);
            editPhoneNumber.requestFocus();
        }
        newUser.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
            startActivity(intent);
        });


        /**
         * Checking login type here.
         */
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.individual){
                    loginType = 1;
                    selectCompany.setVisibility(View.GONE);
                    PreferenceHelper.cleanPerticualrPref(LoginActivity.this,"company");
                }else {
                    loginType = 2;
                    selectCompany.setVisibility(View.VISIBLE);
                }
            }
        });



    }

    @OnClick(R.id.btn_login)
    public void clickOtp() {
        if (validate()) {
            if (Utils.isNetworkAvailable(LoginActivity.this)) {

                companyName = selectCompany.getSelectedItem().toString();
                Log.e("company name>>",companyName.toLowerCase());



                if (loginType ==1 || (loginType == 2 && !companyName.equalsIgnoreCase("Select Your Company"))) {
                    if (loginType == 2){
                        PreferenceHelper.setStringPreference(LoginActivity.this,"company",companyName.toLowerCase().replaceAll("\\s",""));
                    }else {
                        PreferenceHelper.cleanPerticualrPref(LoginActivity.this,"company");
                    }

                    OnLogin();
                }else {
                    Toast.makeText(this, "Please Select Your Company", Toast.LENGTH_SHORT).show();
                };
            } else {
                Toast.makeText(this, getString(R.string.connect_to_internet), Toast.LENGTH_SHORT).show();
            }
            //Toast.makeText(this,"Login has failed!, Complete the missing fields", Toast.LENGTH_SHORT).show();

        }
    }

    /**
     * Starting login process here.
     */
    private void OnLogin()
    {
        mProgressDialog.show();

        Call<LoginResponse> call = RetrofitApiClient.getRetrofit().create(ApiInterface.class)
                .login("login_initiate_webservice.php?mobile_no=" + ph);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {

                //   mDialog.dismiss();
                mProgressDialog.dismiss();
                if (response.body() != null) {

                    Log.d(TAG, "Got response!");
                    if (response.body() != null) {
                        if(response.body().getLoginResponse().get(0).getQueryResponse().getPassword() != null) {
                            String pswd = response.body().getLoginResponse().get(0).getQueryResponse().getPassword();
                            String status = response.body().getLoginResponse().get(0).getQueryResponse().getStatus();
                            Log.d(TAG, "password: " + pswd);
                            Log.d(TAG, "status: " + status);
                            if ((status.equalsIgnoreCase("Login successful.")) ||
                                    (status.equalsIgnoreCase("Password already sent."))) {
                                if (Utils.isNetworkAvailable(LoginActivity.this)) {
                                    otpProcess(pswd);
                                } else {
                                    Toast.makeText(LoginActivity.this, getString(R.string.connect_to_internet), Toast.LENGTH_SHORT).show();

                                }
                            }

                        }

                        else{
                            Toast.makeText(LoginActivity.this, response.body().getLoginResponse().get(0).getQueryResponse().getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                //     mDialog.dismiss();
                mProgressDialog.dismiss();
                Log.e("TAG", t.toString());
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();


            }
        });
        }








    private void otpProcess(String pswd) {


        mProgressDialog.show();
        Call<LoginWebserviceResponse> call = RetrofitApiClient.getRetrofit().create(ApiInterface.class)
                    .loginwebservice("login_webservice.php?mobile_no=" + ph + "&pass=" + pswd);
        call.enqueue(new Callback<LoginWebserviceResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginWebserviceResponse> call, @NonNull Response<LoginWebserviceResponse> response) {
                mProgressDialog.dismiss();
                if (response.body() != null) {
                    String userid = response.body().getLoginResponse().get(0).getQueryResponse().getUserId();
                    String status = response.body().getLoginResponse().get(0).getQueryResponse().getStatus();

                    //if the user entered phone num is not the same as the one that exists in the db
                    //take user permission to delete the existing user information
                    if (!(ph.equalsIgnoreCase(DBHandlerWalk.Instance(getApplicationContext()).getPhoneNum()))) {
                        //drop all the tables and save the information you have
                        DBHandlerWalk.Instance(getApplicationContext()).deleteDatabase();
                        RegInfo reg = new RegInfo(ph, ph, ph);
                        DBHandlerWalk.Instance(getApplicationContext()).addHandler(reg);
                    }
                    //save session
                    SharedPreferences sharedPreferencesLoginData = getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferencesLoginData.edit();
                    editor.putString("PHONE", ph);
                    editor.apply();

                    Log.d(TAG, "userid: " + userid);
                    Log.d(TAG, "status: " + status);
                    Log.d(TAG, "phone: "+ph);

                    if (PreferenceHelper.getPreferences(LoginActivity.this).contains("camera") &&
                    PreferenceHelper.getStringPreference(LoginActivity.this,"camera").equalsIgnoreCase("yes")) {

                        addPhoneNumberToDatabase(ph);
                        Intent intent = new Intent(LoginActivity.this, CameraSplashActivity.class);
                        // Intent intent = new Intent(LoginActivity.this, LoginType.class);
                        // Intent intent = new Intent(LoginActivity.this, TwoOptions.class);
                        startActivity(intent);
                        finish();
                    }else{
                        addPhoneNumberToDatabase(ph);
                        Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    //goto to main page
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginWebserviceResponse> call, @NonNull Throwable t) {
                mProgressDialog.dismiss();
                Log.d(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Login has failed. Please try again." + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Validating mobile number here.
     * @return
     */
    public boolean validate() {

        ph = editPhoneNumber.getText().toString().trim();

        if (ph.isEmpty()) {
            editPhoneNumber.setError("Please enter your Phone Number");
            return false;
        } else if (ph.length() != 10) {
            editPhoneNumber.setError("Please enter valid Phone Number");
            return false;
        }
        return true;
    }

    /**
     * Adding phone number to firestore.
     * @param phone
     */
    private void addPhoneNumberToDatabase(String phone){
        Map<String,Object>data = new HashMap<>();
        data.put("phone",phone);
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection("PhoneNumbers").document(phone).set(data).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("success>","done>");
            }
        });
    }

}
