package hitech.robotic.systemzz;

import com.google.gson.annotations.SerializedName;

class RegistrationResponseResult {
    @SerializedName("QueryResponse")
    public QueryResponseResult queryResponse;

    public QueryResponseResult getQueryResponse() {
        return queryResponse;
    }
}
