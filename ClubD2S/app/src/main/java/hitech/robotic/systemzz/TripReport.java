package hitech.robotic.systemzz;

import android.app.AppOpsManager;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import hitech.robotic.systemzz.chart.LineChartActivity1;
import hitech.robotic.systemzz.utils.PreferenceHelper;


/**
 * This class is used to setup Trip Report page for user.
 */
public class TripReport extends AppCompatActivity {

    TextView startTime, distance,duration, averageSpeed, hardbreak, overSpeed, endTime,currentDate,analysis,safeTipHeading,safetyTip,
    startTimeHeading,distanceHeading,durationHeading,avgSpeedHeading,hardBreakingHeading,overSpeedHeading,endTimeHeading,phoneUsagesHeading
            ,phoneUsages,sharpTurn,sharpTurnHeading,finalScore;
    Button gotIt,shareIt;
    FrameLayout frameLayout;
    long startTimeMills, totalPhoneUsages = 0;
    private ArrayList<Float> avgSpeedPointsList = new ArrayList<>();
    private ArrayList<HashMap<String,String>> reportData = new ArrayList<>();
    private FirebaseFirestore firestore;
    String tripDuration;
    private ScrollView scrollView;
    private RelativeLayout rootView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_report_new);
        initializeViews();
    }


    /**
     * Here we intialize all the fields.
     */
    private void initializeViews(){

        startTime = findViewById(R.id.start_time);
        distance = findViewById(R.id.distance);
        duration = findViewById(R.id.duration);
        averageSpeed = findViewById(R.id.average_speed);
        hardbreak = findViewById(R.id.brake_event);
        overSpeed = findViewById(R.id.overspeed_event);
       // endTime = findViewById(R.id.end_time);
        gotIt = findViewById(R.id.got_it);
        currentDate = findViewById(R.id.current_date);
        frameLayout = findViewById(R.id.frame_container);
        analysis = findViewById(R.id.analysis);
        safeTipHeading = findViewById(R.id.safe_tip_heading);
        safetyTip = findViewById(R.id.safety_tip);
        shareIt = findViewById(R.id.share_it);
        phoneUsages = findViewById(R.id.phone_usages);
        sharpTurn = findViewById(R.id.sharp_turn);
        finalScore = findViewById(R.id.final_score);
        scrollView = findViewById(R.id.scrolling_view);
        rootView = findViewById(R.id.root_view);


        /*startTimeHeading = findViewById(R.id.start_time_heading);
        distanceHeading = findViewById(R.id.distance_heading);
        durationHeading = findViewById(R.id.duration_heading);
        avgSpeedHeading = findViewById(R.id.ave_speed_heading);
        hardBreakingHeading = findViewById(R.id.hard_break_heaing);
        overSpeedHeading = findViewById(R.id.over_speed_heading);
        endTimeHeading = findViewById(R.id.end_time_heading);
        phoneUsagesHeading = findViewById(R.id.phone_usage_heading);
        sharpTurnHeading = findViewById(R.id.sharp_turn_heading);*/

        String timeStamp = ((Long) (System.currentTimeMillis() / 1000)).toString();
        if (!PreferenceHelper.getPreferences(this).contains("tripId")) {
            PreferenceHelper.setStringPreference(this, "tripId", timeStamp);
        }

        String tripId = PreferenceHelper.getStringPreference(this, "tripId");
        String path = PreferenceHelper.getStringPreference(this,"reportway");

        if (path.equalsIgnoreCase("camera")) {

            startTime.setText(getIntent().getExtras().getString("startTime") + " - " + getIntent().getExtras().getString("endTime"));
            distance.setText(getIntent().getExtras().getString("distance"));
            duration.setText(getIntent().getExtras().getString("time"));
            averageSpeed.setText(getIntent().getExtras().getString("speed"));
            hardbreak.setText(getIntent().getExtras().getString("hardbrake"));
            overSpeed.setText(getIntent().getExtras().getString("overSpeed"));
            // endTime.setText(getIntent().getExtras().getString("endTime"));
            //  currentDate.setText(getIntent().getExtras().getString("currentDate"));
            sharpTurn.setText(getIntent().getExtras().getString("sharpTurn"));
            avgSpeedPointsList = (ArrayList<Float>) getIntent().getSerializableExtra("speedPoints");
            startTimeMills = getIntent().getExtras().getLong("startTimeMills");
            tripDuration = getIntent().getExtras().getString("durationInHours");
            Log.d("listingdouble>", avgSpeedPointsList.toString());

        }else {

            reportData = (ArrayList<HashMap<String, String>>)getIntent().getSerializableExtra("all_data");
            int position = getIntent().getExtras().getInt("position");
            startTime.setText(reportData.get(position).get("startTime"));
            distance.setText(reportData.get(position).get("distance") + ""+ " Km");
            duration.setText(reportData.get(position).get("duration"));
            averageSpeed.setText(reportData.get(position).get("avgSpeed")+"" +" Km/h");
            hardbreak.setText(reportData.get(position).get("hardbreak"));
            overSpeed.setText(reportData.get(position).get("overSpeed"));
            sharpTurn.setText(reportData.get(position).get("sharpTurn"));
           // startTimeMills = Long.valueOf(reportData.get(position).get("startTime"));

            if (Integer.valueOf(reportData.get(position).get("FinalScore")) < 0){
                finalScore.setText("0");
            }else {
                finalScore.setText(reportData.get(position).get("FinalScore"));
            }

            phoneUsages.setText(reportData.get(position).get("phoneUsages"));

           // tripDuration = (reportData.get(position).get("startTime"));
            //startTime.setText(reportData.get(position).get("startTime"));

        }

        Typeface customFonts1 = Typeface.createFromAsset(getAssets(),"Roboto-Medium.ttf");
       // Typeface customFonts2 = Typeface.createFromAsset(getAssets(),"OpenSans-Regular.ttf");
        Typeface customFonts3 = Typeface.createFromAsset(getAssets(),"Roboto-Black.ttf");



       // currentDate.setTypeface(customFonts1);
        /*startTimeHeading.setTypeface(customFonts1);
        distanceHeading.setTypeface(customFonts1);
        durationHeading.setTypeface(customFonts1);
        avgSpeedHeading.setTypeface(customFonts1);
        hardBreakingHeading.setTypeface(customFonts1);
        overSpeedHeading.setTypeface(customFonts1);
        endTimeHeading.setTypeface(customFonts1);
        phoneUsagesHeading.setTypeface(customFonts1);
        sharpTurnHeading.setTypeface(customFonts1);*/

        startTime.setTypeface(customFonts3);
        distance.setTypeface(customFonts3);
        duration.setTypeface(customFonts3);
        averageSpeed.setTypeface(customFonts3);
        hardbreak.setTypeface(customFonts3);
        overSpeed.setTypeface(customFonts3);
       // endTime.setTypeface(customFonts3);
        phoneUsages.setTypeface(customFonts3);
        sharpTurn.setTypeface(customFonts3);

        gotIt.setTypeface(customFonts3);
        shareIt.setTypeface(customFonts3);
        finalScore.setTypeface(customFonts3);

        Log.d("love111>","dfdfdfd");

       // String avgSp = averageSpeed.getText().toString().substring(0,averageSpeed.getText().toString().indexOf(" "));
      //  String avgss = averageSpeed.getText().toString();

       // Log.e("love>>",avgSp + avgss);

        if (path.equalsIgnoreCase("camera")){
        if (checkPackageUsagesPermission()){
            phoneUsages.setText(usesStats());
        }else {
            phoneUsages.setText("No Permission");
        }}

        firestore = FirebaseFirestore.getInstance();

        Log.e("distance><",distance.getText().toString());



        if (path.equalsIgnoreCase("camera")) {

            String distance1 = distance.getText().toString().substring(0, distance.getText().toString().indexOf(" "));

            if (Double.valueOf(distance1)>0.5) {
                addDataReportWithFirestore(startTime.getText().toString(),
                        distance.getText().toString().substring(0, distance.getText().toString().indexOf(" ")),
                        tripDuration,
                        averageSpeed.getText().toString().substring(0, averageSpeed.getText().toString().indexOf(" ")),
                        hardbreak.getText().toString(),
                        overSpeed.getText().toString(),
                        getIntent().getExtras().getString("endTime"),
                        String.valueOf(TimeUnit.MILLISECONDS.toHours(totalPhoneUsages)),
                        sharpTurn.getText().toString().substring(0, sharpTurn.getText().toString().indexOf(" ")),
                        getIntent().getExtras().getString("currentDate"),
                        tripId, duration.getText().toString(),
                        phoneUsages.getText().toString());
            }
        }

        /**
         * This executes when Got It button executes.
         */
        gotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripReport.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        /**
         * This executes when shareit button calls.
         */
        shareIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotIt.setVisibility(View.INVISIBLE);
                shareIt.setVisibility(View.INVISIBLE);
                shareScreenShot();
            }
        });

        addChartFragment();



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(TripReport.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * This method set up speed profile chart.
     */
    private void addChartFragment(){
        if (avgSpeedPointsList.size()>2) {
            safeTipHeading.setVisibility(View.INVISIBLE);
            safetyTip.setVisibility(View.INVISIBLE);
            analysis.setVisibility(View.VISIBLE);

            Bundle bundle = new Bundle();
            bundle.putSerializable("speedPoints", avgSpeedPointsList);
            LineChartActivity1 lineChartActivity1 = new LineChartActivity1();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            lineChartActivity1.setArguments(bundle);
            transaction.add(R.id.frame_container, lineChartActivity1);
            transaction.addToBackStack(null);
            transaction.commit();
        }else {
            analysis.setVisibility(View.INVISIBLE);
        }
    }


    /**
     * This method is called when user click on share it button.
     */
    private void shareScreenShot(){
       // View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
       shareImage(store(loadBitmapFromView(scrollView,1440,2560), "TripReport.png"));
    }

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width , height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
        v.draw(c);
        return b;
    }

    public Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public static Bitmap getScreenShot(View view) {
        View screenView = view.getRootView();
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    /**
     * This method created a file in folder for generated screenshot.
     * @param bm
     * @param fileName
     * @return
     */
    public static File store(Bitmap bm, String fileName){
        final String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(dirPath);
        if(!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }


    private void shareImage(File file){
        Uri uri = FileProvider.getUriForFile(TripReport.this, BuildConfig.APPLICATION_ID+".provider",file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        try {
            startActivity(Intent.createChooser(intent, "Share Screenshot"));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(TripReport.this, "No App Available", Toast.LENGTH_SHORT).show();
        }

        gotIt.setVisibility(View.VISIBLE);
        shareIt.setVisibility(View.VISIBLE);
    }

    /**
     * This method check the permission for phone usages.
     * @return
     */
    private boolean checkPackageUsagesPermission(){
        AppOpsManager appOps = (AppOpsManager) this
                .getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow("android:get_usage_stats",
                android.os.Process.myUid(), this.getPackageName());
        boolean granted = mode == AppOpsManager.MODE_ALLOWED;
        return granted;
    }

    /**
     * This method provides the phone usages stats of user.
     * @return
     */
    private String usesStats(){

        int lauchCount;
        UsageEvents.Event currentEvent;
        List<UsageEvents.Event> allEvents = new ArrayList<>();
        HashMap<String, AppUsageInfo> map = new HashMap <String, AppUsageInfo> ();

        final UsageStatsManager usageStatsManager=(UsageStatsManager)this.getSystemService(Context.USAGE_STATS_SERVICE);
      //  final Map<String,UsageStats> queryUsageStats=usageStatsManager.queryAndAggregateUsageStats(startTimeMills, System.currentTimeMillis());
        UsageEvents usageEvents = usageStatsManager.queryEvents(startTimeMills, System.currentTimeMillis());
        Log.d("log>>>",usageStatsManager.toString());
        /*for (int i=0; i<queryUsageStats.size(); i++){
            Log.d("usess1>",queryUsageStats.get(i).getPackageName());
            Log.d("usess1>",queryUsageStats.get(i).getPackageName().getClass().getSimpleName());
            Log.d("usess1>",queryUsageStats.get(i).getLastTimeUsed()+"");
            Log.d("usess1>",queryUsageStats.get(i).getTotalTimeInForeground()+"");
            totalPhoneUsages = totalPhoneUsages+ queryUsageStats.get(i).getTotalTimeInForeground();
        }*/

       /* for (Map.Entry<String, UsageStats> entry : queryUsageStats.entrySet())
        {
            System.out.println(entry.getKey() + " / " + entry.getValue());
            Log.d("usess1>",entry.getValue().getPackageName());
            Log.d("usess1>",entry.getValue().getPackageName().getClass().getSimpleName());
            Log.d("usess1>",entry.getValue().getLastTimeUsed()+"");
            Log.d("usess1>",entry.getValue().getTotalTimeInForeground()+"");
            totalPhoneUsages = totalPhoneUsages+ entry.getValue().getTotalTimeInForeground();

        }*/

        while (usageEvents.hasNextEvent()) {
            currentEvent = new UsageEvents.Event();
            usageEvents.getNextEvent(currentEvent);
            if (currentEvent.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND ||
                    currentEvent.getEventType() == UsageEvents.Event.MOVE_TO_BACKGROUND) {
                allEvents.add(currentEvent);
                String key = currentEvent.getPackageName();
// taking it into a collection to access by package name
                if (map.get(key)==null)
                    map.put(key,new AppUsageInfo(key));
            }
        }

        for (int i=0;i<allEvents.size()-1;i++){
            UsageEvents.Event E0=allEvents.get(i);
            UsageEvents.Event E1=allEvents.get(i+1);

//for launchCount of apps in time range
            if (!E0.getPackageName().equals(E1.getPackageName()) && E1.getEventType()==1 && !E1.getPackageName().contains("robotic") &&
            !E1.getPackageName().contains("launcher") && !E1.getPackageName().contains("systemui")
                    && !E1.getPackageName().contains("home")){
// if true, E1 (launch event of an app) app launched
                map.get(E1.getPackageName()).launchCount++;
                Log.d("launch1>>>",E1.getPackageName()+"");
            }

//for UsageTime of apps in time range
            if (E0.getEventType()==1 && E1.getEventType()==2
                    && E0.getClassName().equals(E1.getClassName()) && !E0.getPackageName().contains("robotic")
                    && !E0.getPackageName().contains("launcher")&& !E0.getPackageName().contains("systemui")
                    && !E0.getPackageName().contains("home")){
                long diff = E1.getTimeStamp()-E0.getTimeStamp();
                totalPhoneUsages+=diff; //gloabl Long var for total usagetime in the timerange
                map.get(E0.getPackageName()).timeInForeground+= diff;
                Log.d("launch2>>>",E0.getPackageName());
            }
        }

        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(totalPhoneUsages),
                TimeUnit.MILLISECONDS.toMinutes(totalPhoneUsages) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(totalPhoneUsages) % TimeUnit.MINUTES.toSeconds(1));
        return hms;

    }


    /************** Send Report To Firebase *************************/
    /**
     * Send data to firestore.
     * @param StartTime
     * @param distance
     * @param duration
     * @param avgSpeed
     * @param hardbreak
     * @param overSpeed
     * @param endTime
     * @param phoneUsages
     * @param sharpTurn
     * @param tripDate
     * @param tripId
     * @param durationToShowOnly
     * @param phoneUsagesTime
     */

    private void addDataReportWithFirestore(String StartTime, String distance, String duration, String avgSpeed,
                                            String hardbreak, String overSpeed, String endTime,String phoneUsages, String sharpTurn,
                                            String tripDate, String tripId, String durationToShowOnly, String phoneUsagesTime) {
        Map<String, Object> report = new HashMap<>();
        report.put("startTime", StartTime);
        report.put("distance", distance);
        report.put("duration", durationToShowOnly);
        report.put("avgSpeed", avgSpeed);
        report.put("hardbreak", hardbreak);
        report.put("overSpeed", overSpeed);
        report.put("endTime", endTime);
        report.put("phoneUsages",phoneUsagesTime);
        report.put("sharpTurn",sharpTurn);
        report.put("currentDate",tripDate);
        report.put("tripId",tripId);

        double phoneUsagesPerT = 0;

        Double mAvgSpeed = Double.valueOf(avgSpeed);
        Double mDistance = Double.valueOf(distance);
        double mDuration = Double.valueOf(duration);
        int mHardBreak = Integer.valueOf(hardbreak);
        int mOverSpeed = Integer.valueOf(overSpeed);
        int mPhoneUsages = Integer.valueOf(phoneUsages);
        int mSharpTurn = Integer.valueOf(sharpTurn);

        if (mDistance==0.0) {
            mDistance = 0.1;
        }

        Double overSpeedingperT = mOverSpeed*1000/mDistance;
        Double hardBreakPerT = mHardBreak*1000/mDistance;
        Double sharpTurnPerT = mSharpTurn*1000/mDistance;
        if (mDuration!=0.0) {
             phoneUsagesPerT = mPhoneUsages / mDuration;
        }
        Double pOverSpeeding = 0.1*overSpeedingperT;
        Double pHardBreak = 0.1*hardBreakPerT;
        Double pSharpTurn = 2.5*sharpTurnPerT;
        Double pPhoneUsages = 0.05*phoneUsagesPerT;

        Log.d("finalscore>>>",mDistance + " >> "+ overSpeedingperT + " >> "+ mOverSpeed + " >> "+ mHardBreak + " >> "+hardBreakPerT);

        report.put("overSpeedPerThousand",overSpeedingperT+"");
        report.put("hardBreakPerThousand",hardBreakPerT+"");// yahan se string banaya
        report.put("sharpTurnPerThousand",sharpTurnPerT+"");
        report.put("phoneUsagesPerThousand",sharpTurnPerT+"");

        report.put("percentOverSpeed",pOverSpeeding+"");
        report.put("percentHardBreak",pHardBreak+"");
        report.put("percentSharpTurn",pSharpTurn+"");
        report.put("percentPhoneUsages",pPhoneUsages+"");

        Double score = 100 - (pOverSpeeding + pHardBreak + pSharpTurn + pPhoneUsages);

        report.put("FinalScore", score.intValue()+"");

        Log.d("finalscore>>>",pOverSpeeding + " >> "+ pHardBreak + " >> "+ pSharpTurn + " >> "+ pPhoneUsages + " >> "+score);

        if (score.intValue()<0) {

            finalScore.setText("0");
        }else {
            finalScore.setText(score.intValue() + "");
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        Log.d("reportData>>>", StartTime + " >>> "+ distance +" >>> "+ duration + " >>>>> "+ avgSpeed + " >>>> "+
                hardbreak + " >>>>> "+ overSpeed + " >>>>> "+ endTime + " >>>> "+phoneUsages + " >>>>> "+ sharpTurn
        + " >>>>>> ");

        Log.d(" thousand >>> ", sharpTurnPerT + " >>>> "+ hardBreakPerT + " >>> "+ phoneUsagesPerT + " >>>>> "+overSpeedingperT);

        Log.d(" Percent >>>>  ", pOverSpeeding + " >>>> "+ pHardBreak + " >>>>> "+ pSharpTurn + " >>>> "+ pPhoneUsages);

        SharedPreferences sharedPreferencesLoginData = getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE);
        String str = sharedPreferencesLoginData.getString("PHONE", null);

        firestore.collection("ClubD2S").document("TripData").collection("report")
                .document(str) .collection("all_report").document(dateFormat.format(new Date()))
                .set(report)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("firestore>>", "success");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("firestore>>", "success");
                    }
                });

        addPhoneNumberToDatabase(str);

    }


    /**
     * Adding phone numbers to the database.
     * @param phone
     */
    private void addPhoneNumberToDatabase(String phone){
        Map<String,Object>data = new HashMap<>();
        data.put("phone",phone);
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection("PhoneNumbers").document(phone).set(data).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("success>","done>");
            }
        });
    }

    /****************************************************************/

}
