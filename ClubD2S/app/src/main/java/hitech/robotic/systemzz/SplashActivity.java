package hitech.robotic.systemzz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import hitech.robotic.systemzz.utils.Typewriter;

/**
 * This class setup Splash page of app which load when app opens first time.
 */

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 2500;
    private Typewriter SplashTextContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        SplashTextContent = findViewById(R.id.splash_text_content);
        SplashTextContent.setCharacterDelay(50);
        SplashTextContent.animateText("Sadak Suraksha \n Jeevan Raksha");
        //todo: maintain login session
        SharedPreferences sharedPreferencesLastData = getSharedPreferences("LOGIN_SESSION_ACTIVE", Context.MODE_PRIVATE);
        String str = sharedPreferencesLastData.getString("PHONE", null);

        if((str != null) && (DBHandlerWalk.Instance(getApplicationContext()).registeredUser()))
        {
            //login session is active, so go to main activity

            new Handler().postDelayed(new Runnable() {


                 /** Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company*/

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    //Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                    Intent intent = new Intent(SplashActivity.this,HomeActivity.class);
                    startActivity(intent);
                    // close this activity
                    finish();
                }
            }, SPLASH_TIME_OUT);

        } else{
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                   // Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }, SPLASH_TIME_OUT);

        }

    }

}
