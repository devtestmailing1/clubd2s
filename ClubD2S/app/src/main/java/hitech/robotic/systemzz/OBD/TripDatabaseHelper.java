package hitech.robotic.systemzz.OBD;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TripDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "tripevents.db";
    public static final String TRIPEVENT_TABLE_NAME = "EventTable";
    public static final String COLUMN_DATE = "Date";
    public static final String COLUMN_TIMESTAMP = "TimeStamp";
    public static final String COLUMN_TIME = "Time";
    public static final String COLUMN_EVENTNAME = "EventName";
    public static final String COLUMN_TRIPID = "EventId";


    String CREATE_EVENT_TABLE = "create table " + TRIPEVENT_TABLE_NAME + "("
            + COLUMN_TIMESTAMP + " TEXT PRIMARY KEY, "
            + COLUMN_DATE + " TEXT, "
            + COLUMN_TIME + " TEXT, "
            + COLUMN_EVENTNAME + " TEXT, "
            + COLUMN_TRIPID + " TEXT);";



    public TripDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        getWritableDatabase();
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(CREATE_EVENT_TABLE);
        Log.d("Fetch Data>>", "on Create" );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("databae updgrad >>", "");

    }

}
