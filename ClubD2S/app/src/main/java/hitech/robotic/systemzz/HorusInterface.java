package hitech.robotic.systemzz;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface HorusInterface {
    @POST("/addUser")
    Call<List<HorusUser>> registerUser(@Body HorusUser hu);


    @POST("/event")
    Call<Void> logLatLongEvent(@Body List<DeviceLocation> loc);

    @POST("/event")
    Call<Void> logNFE(@Body List<NFEevent> nfe);
}
