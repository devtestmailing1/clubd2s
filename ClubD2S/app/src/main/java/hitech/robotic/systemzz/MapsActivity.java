package hitech.robotic.systemzz;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hitech.robotic.systemzz.retrofit.ApiInterface;

//import com.karumi.dexter.Dexter;
//import com.karumi.dexter.PermissionToken;
//import com.karumi.dexter.listener.PermissionDeniedResponse;
//import com.karumi.dexter.listener.PermissionGrantedResponse;
//import com.karumi.dexter.listener.PermissionRequest;
//import com.karumi.dexter.listener.single.PermissionListener;
/*
import com.tarek360.instacapture.Instacapture;
import com.tarek360.instacapture.listener.ScreenCaptureListener;
*/

public class MapsActivity extends AppCompatActivity implements OnShowcaseEventListener, OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapClickListener,
        GoogleApiClient.ConnectionCallbacks,
        LocationListener {
    public static final int PERMISSION_REQUEST_CODE = 99;
    public static final int MIN_STEPS_TO_CONTRIBUTE = 100;
    private static final String TAG = "MapsActivity";
    final Date date = new Date();
    public int TripNum;
    Boolean a = false;
    Button stopwalk;
    FloatingActionButton floatingHome, floatingMe, floatingShare, floatingcontribute;
    Boolean visibility = true;
    double speed = 0.0;
    double distance = 0.0;
    int Contributeflag = 0;
    double time = 0.0;
    String SCAN_PATH;
    File[] allFiles;
    Marker startLocationMarker;
    float zoomlevel = 16.0f;
    String Name;
    int i = 0;
    Marker currentMarker = null;
    Polyline line;
    Boolean loca = true;
    Vector<Location> points = new Vector<Location>();
    PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
    float[] prevdistance;
    SimpleDateFormat Formater = new SimpleDateFormat("dd MMM yyyy");
    String datestr = Formater.format(date);
    private long mLastUpdateTime;
    private GoogleMap mMap;
    private GoogleApiClient client;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private Marker currentLocationmarker;
    private TextView tdistance;
    private EditText editText;
    private Button startwalk;
    private RelativeLayout rMaplayout;
    private LinearLayout rDistancelayout;
    private TextView datetext;
    private TextView tDistance, tSteps, tWalks, tName;
    private float totalDistance = 0.0f;
    private int totalSteps = 0;
    private Location startLocation;
    private Location stopLocation;
    private Location initLocation;
    private Unbinder bind;
    private MediaScannerConnection conn;
    private String mPath = Environment.
            getExternalStorageDirectory().toString() + "/ClubD2S";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        RegInfo reg = DBHandlerWalk.Instance(getApplicationContext()).findRegHandler();
        initView();
        stopwalk.setEnabled(false);
        Name = reg.getName();
        prevdistance = new float[5];
        tName.setText(String.valueOf(Name + "'s Walk"));
        //to access the screenshot as signature
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        bind = ButterKnife.bind(this);


       /* GPSTracker gpsTracker = new GPSTracker(this, MapsActivity.this);
        double latitude;
        double longitude;
        LatLng latLng;
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            latLng = new LatLng(latitude, longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomlevel));

        } else {
            gpsTracker.showSettingsAlert();
        }*/


        floatingHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent me = new Intent(MapsActivity.this, MainActivity.class);
                startActivity(me);
            }
        });
        floatingMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent me = new Intent(MapsActivity.this, Me.class);
                startActivity(me);
            }
        });
        floatingShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File image = getLatestFilefromDir(mPath);
                if (image != null) {
                    openScreenshot(image);
                    shareScreenshot(image);
                } else
                    Toast.makeText(getApplicationContext(), "No image found", Toast.LENGTH_LONG).show();
            }
        });

        floatingcontribute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (totalSteps > MIN_STEPS_TO_CONTRIBUTE) {
                    File image = getLatestFilefromDir(mPath);
                    if (image != null) {
                        uploadscreenshot(image.toString());
                        //Contributeflag=0;
                    } else
                        Toast.makeText(getApplicationContext(), "No image found", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Too few steps to contribute", Toast.LENGTH_LONG).show();
                }
            }
        });
        //tTime.setText(String.valueOf(time));
        //tSpeed.setText(String.valueOf(totalSteps));
        //tDistance.setText(String.valueOf(totalDistance));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

    }

    private void initView() {
        startwalk = findViewById(R.id.startwalk);
        stopwalk = findViewById(R.id.stopwalk);
        rMaplayout = findViewById(R.id.maplayout);
        rDistancelayout = findViewById(R.id.distancelayout);
        //tDistance = findViewById(R.id.distance);
        tName = findViewById(R.id.nameId);
        tdistance = (TextView) findViewById(R.id.distance);
        floatingHome = findViewById(R.id.floatingbtn2);
        floatingMe = findViewById(R.id.floatingbtn3);
        floatingShare = findViewById(R.id.floatingbtn4);
        floatingcontribute = findViewById(R.id.floatingcontribute);
        datetext = findViewById(R.id.datestr);
        tSteps = findViewById(R.id.steps);

       /* ViewTarget target = new ViewTarget(R.id.floatingbtn2, this);

        ShowcaseView sv = new ShowcaseView.Builder(this)
                .withMaterialShowcase()
                *//*.setTarget(new CustomViewTarget(R.id.floatingbtn2, -100, 0, this))*//*
                .setTarget(target)
                .setContentTitle("Home")
                .setContentText("Please Click Button To Redirect the Home")
                .setShowcaseEventListener(MapsActivity.this)
                .hideOnTouchOutside()
                .build();
        sv.setBackgroundColor(getResources().getColor(R.color.blackBackground));
        sv.hideButton();
        sv.forceTextPosition(ShowcaseView.LEFT_OF_SHOWCASE);
*/
        SharedPreferences sharedPreferencesLastData = getSharedPreferences("PREV_WALK_PREF", Context.MODE_PRIVATE);
        SharedPreferences sharedContributeflag = getSharedPreferences("Contribute_Flag", Context.MODE_PRIVATE);
        Contributeflag = sharedContributeflag.getInt("flag", 0);
        tdistance.setText(sharedPreferencesLastData.getString("DISTANCE", "0 m"));
        datetext.setText(sharedPreferencesLastData.getString("DATE", datestr));
        tSteps.setText(sharedPreferencesLastData.getString("STEPS", "0"));

      //  if (sharedPreferencesLastData.getString("STEPS", "0").equals("0")){
            androidx.appcompat.app.AlertDialog dialog = new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage("Please click the Start Walk button to record a new walk.\n\nThe last walk statistics are shown.")
                    .create();
            dialog.show();
      //  }
    }

    private void buildAlertMessageNoGps() {
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Toast.makeText(getApplicationContext(), "Please enable GPS to use this function.", Toast.LENGTH_LONG).show();
                        Intent back = new Intent(MapsActivity.this, MainActivity.class);
                        startActivity(back);
                    }
                });
        final androidx.appcompat.app.AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(MapsActivity.this);
        } else {
            buildAlertMessageNoGps();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
    }

    @SuppressLint("RestrictedApi")
    public void onClick(View v) {
        if (v.getId() == R.id.startwalk) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            GPSTracker gpsTracker = new GPSTracker(this, MapsActivity.this);
            double latitude = 0;
            double longitude = 0;
            LatLng latLng = null;
            if (gpsTracker.getIsGPSTrackingEnabled()) {
                latitude = gpsTracker.getLatitude();
                longitude = gpsTracker.getLongitude();
                latLng = new LatLng(latitude, longitude);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomlevel));

            } else {
                gpsTracker.showSettingsAlert();
            }


            //    LatLng latLng = new LatLng(startLocation.getLatitude(), startLocation.getLongitude());         // Get start location, save to DB
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Starting");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            currentLocationmarker = mMap.addMarker(markerOptions);
            startLocationMarker = currentLocationmarker;
            AlertDialog.Builder alert = new AlertDialog.Builder(MapsActivity.this);
            alert.setCancelable(false);
            alert.setTitle("Confirm Message!");
            if (Contributeflag == 1) {
                alert.setMessage("Do you want to Contribute your previous walk?");
                LatLng finalLatLng = latLng;
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startLocation = LocationServices.FusedLocationApi.getLastLocation(client);
                        mMap.clear();
                        File image = getLatestFilefromDir(mPath);
                        if (image != null) {
                            uploadscreenshot(image.toString());
                            //Contributeflag=0;
                        } else {
                            Toast.makeText(getApplicationContext(), "No image found", Toast.LENGTH_LONG).show();
                        }
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(finalLatLng);
                        markerOptions.title("Starting");
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        currentLocationmarker = mMap.addMarker(markerOptions);
                        rMaplayout.setPadding(0, 0, 0, 0);


                        // Get Stop location(Latlng), save to DBtance;
                        startwalk.setClickable(false);
                        startwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_disable));
                        stopwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_enable));
                        stopwalk.setEnabled(true);
                        stopwalk.setClickable(true);
                      //  floatingMe.setVisibility(View.INVISIBLE);
                        floatingHome.setVisibility(View.INVISIBLE);
                        floatingcontribute.setVisibility(View.INVISIBLE);
                        floatingShare.setVisibility(View.INVISIBLE);
                        if (line != null) {
                            totalDistance = 0;
                            totalSteps = 0;
                            line.remove();
                        }
                        TripNum = getNextTripNum();

                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "Your previous walk has been discarded", Toast.LENGTH_SHORT).show();
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }

                        GPSTracker gpsTracker = new GPSTracker(getApplicationContext(), MapsActivity.this);
                        double latitude = 0;
                        double longitude = 0;
                        LatLng latLng = null;
                        if (gpsTracker.getIsGPSTrackingEnabled()) {
                            latitude = gpsTracker.getLatitude();
                            longitude = gpsTracker.getLongitude();
                            latLng = new LatLng(latitude, longitude);
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomlevel));

                        } else {
                            gpsTracker.showSettingsAlert();
                        }


                        //    LatLng latLng = new LatLng(startLocation.getLatitude(), startLocation.getLongitude());         // Get start location, save to DB
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        markerOptions.title("Starting");
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        currentLocationmarker = mMap.addMarker(markerOptions);
                        startLocationMarker = currentLocationmarker;
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startLocation = LocationServices.FusedLocationApi.getLastLocation(client);
                        mMap.clear();
                        markerOptions.position(finalLatLng);
                        markerOptions.title("Starting");
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        currentLocationmarker = mMap.addMarker(markerOptions);
                        rMaplayout.setPadding(0, 0, 0, 0);


                        // Get Stop location(Latlng), save to DBtance;
                        startwalk.setClickable(false);
                        startwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_disable));
                        stopwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_enable));

                        stopwalk.setEnabled(true);
                        stopwalk.setClickable(true);
                     //   floatingMe.setVisibility(View.INVISIBLE);
                        floatingHome.setVisibility(View.INVISIBLE);
                        floatingcontribute.setVisibility(View.INVISIBLE);
                        floatingShare.setVisibility(View.INVISIBLE);
                        if (line != null) {
                            totalDistance = 0;
                            totalSteps = 0;
                            line.remove();
                        }
                        TripNum = getNextTripNum();
                    }
                });

                AlertDialog dialog = alert.create();
                dialog.show();
            } else {
                alert.setMessage("Do you want to Start walk?");
                LatLng finalLatLng = latLng;
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        tName.setText(String.valueOf(Name + "'s Walk"));
                        final Date date = new Date();
                        SimpleDateFormat Formater = new SimpleDateFormat("dd MMM yyyy");
                        String datestr = Formater.format(date);
                        datetext.setText(datestr);
                        tdistance.setText("0");
                        tSteps.setText("0");
                        startLocation = LocationServices.FusedLocationApi.getLastLocation(client);
                        mMap.clear();
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(finalLatLng);
                        markerOptions.title("Starting");
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        currentLocationmarker = mMap.addMarker(markerOptions);
                        rMaplayout.setPadding(0, 0, 0, 0);


                        // Get Stop location(Latlng), save to DBtance;
                        startwalk.setClickable(false);
                        startwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_disable));
                        stopwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_enable));
                        stopwalk.setEnabled(true);
                        stopwalk.setClickable(true);
                      //  floatingMe.setVisibility(View.INVISIBLE);
                        floatingHome.setVisibility(View.INVISIBLE);
                        floatingcontribute.setVisibility(View.INVISIBLE);
                        floatingShare.setVisibility(View.INVISIBLE);

                        if (line != null) {
                            totalDistance = 0;
                            totalSteps = 0;
                            line.remove();
                        }
                        TripNum = getNextTripNum();

                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        currentLocationmarker.remove();
                    }
                });

                AlertDialog dialog = alert.create();
                dialog.show();
            }
        }
    }


    protected synchronized void buildGoogleApiClient() {
        client = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        client.connect();
    }

    public void Capture() {
    }

    @Override
    public void onLocationChanged(Location location) {

        GPSTracker gpsTracker = new GPSTracker(this, MapsActivity.this);
        double latitude = 0;
        double longitude = 0;
        LatLng latLng = null;
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

        } else {
            gpsTracker.showSettingsAlert();
        }

        if (initLocation == null) {
            if (!(location.getLatitude() == 0.0 && location.getLongitude() == 0.0)) {
                initLocation = location;
                latLng = new LatLng(initLocation.getLatitude(), initLocation.getLongitude());         // Get start location, save to DB
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomlevel));
                startwalk.setVisibility(View.VISIBLE);
                stopwalk.setVisibility(View.VISIBLE);
            }

        }
        if (!startwalk.isClickable()) {
            tName.setText(String.valueOf(Name + "'s Walk"));
            datetext.setText(datestr);
            String dist = String.format("%.0f m", (totalDistance));
            tdistance.setText(dist);
            tSteps.setText(String.valueOf(totalSteps));
            Location prevLocation = lastLocation;
            lastLocation = location;
            //Toast.makeText(getApplicationContext(),"Prev: " + prevLocation + " Last: " + lastLocation, Toast.LENGTH_SHORT).show();
            if (((location.getTime() / 1000) - mLastUpdateTime) > 30) {
                //log it to local db once every 30 secs
                if (TripNum != 0) {
                    //wait until the db is read and the next trip num is updated
                    Log.d(TAG, "DB: Creating GPSLocation and calling dbHandler.addHandler");
                    GpsLocation loc = new GpsLocation(location.getTime() / 1000,
                            String.valueOf(location.getLatitude()),
                            String.valueOf(location.getLongitude()), TripNum);
                    DBHandlerWalk.Instance(getApplicationContext()).addHandler(loc);
                    mLastUpdateTime = location.getTime() / 1000;
                }
            }
            if (prevLocation != null && lastLocation != null) {
                float distance = prevLocation.distanceTo(lastLocation);
                for (int i = prevdistance.length - 1; i >= 1; i--) {
                    prevdistance[i] = prevdistance[i - 1];
                }
                prevdistance[0] = distance;
                /* if (distance < 10.0f) {*/
                int steps = (int) (1.32 * distance);
                totalDistance += distance;
                totalSteps = (int) (1.32 * totalDistance);
                //totalSteps += steps;
                //TripInfo trip = new TripInfo(TripNum, lastLocation.getTime() / 1000, steps, distance);
                //DBHandlerWalk.Instance(getApplicationContext()).addHandler(trip);
               /* } else {
                    lastLocation = prevLocation;//revert the erratic changes
                }*/
                latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                options.add(latLng);
                line = mMap.addPolyline(options);
            } else if (lastLocation == null) {
                float average = 0.0f;
                for (int i = 0; i < prevdistance.length; i++) {
                    average += prevdistance[i];
                }
                average /= prevdistance.length;
                // Toast.makeText(getApplicationContext(),"Average: " + average, Toast.LENGTH_SHORT).show();
                totalDistance += average;
                totalSteps = (int) (1.32 * totalDistance);
                for (int i = prevdistance.length - 1; i >= 1; i--) {
                    prevdistance[i] = prevdistance[i - 1];
                }
                prevdistance[0] = average;
            }
        }
    }

    public int getNextTripNum() {
        int nxtTripNum = 1;
        //extract last entry in gps table, inc the trip num
        nxtTripNum = DBHandlerWalk.Instance(getApplicationContext()).getNextTripNum();
        Log.d(TAG, "Next Trip num : " + nxtTripNum);
        return nxtTripNum;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission is granted
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (client == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    //permision is denied
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }
        return true;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (client.isConnected()) {
            locationRequest = new LocationRequest();
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(5000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, this);
            }
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            initLocation = LocationServices.FusedLocationApi.getLastLocation(client);
            GPSTracker gpsTracker = new GPSTracker(this, MapsActivity.this);
            double latitude = 0;
            double longitude = 0;
            LatLng latLng = null;
            if (gpsTracker.getIsGPSTrackingEnabled()) {
                latitude = gpsTracker.getLatitude();
                longitude = gpsTracker.getLongitude();
                latLng = new LatLng(latitude, longitude);

            } else {
                gpsTracker.showSettingsAlert();
            }
            if (initLocation != null) {
                if (!(initLocation.getLatitude() == 0.0 && initLocation.getLongitude() == 0.0)) {
                    //  LatLng latLng = new LatLng(initLocation.getLatitude(), initLocation.getLongitude());         // Get start location, save to DB
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomlevel));
                    startwalk.setVisibility(View.VISIBLE);
                    stopwalk.setVisibility(View.VISIBLE);
                } else {
                    initLocation = null;
                }
            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onBackPressed() {
        if (startwalk.isClickable()) {
            super.onBackPressed();

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.stopwalk)
    public void clickStopWalk() {
        if (totalSteps > MIN_STEPS_TO_CONTRIBUTE) {
            rMaplayout.setPadding(0, 0, 0, 0);
            rDistancelayout.setVisibility(View.VISIBLE);
            startwalk.setVisibility(View.INVISIBLE);
            stopwalk.setVisibility(View.INVISIBLE);
          //  floatingMe.setVisibility(View.INVISIBLE);
            floatingHome.setVisibility(View.INVISIBLE);
            floatingcontribute.setVisibility(View.INVISIBLE);
            floatingShare.setVisibility(View.INVISIBLE);

            TripInfo trip = new TripInfo(TripNum, startLocation.getTime() / 1000, lastLocation.getTime() / 1000, totalSteps, totalDistance, 0);
            DBHandlerWalk.Instance(getApplicationContext()).addHandler(trip);
            SharedPreferences sharedPreferencesLastData = getSharedPreferences("PREV_WALK_PREF", Context.MODE_PRIVATE);
            SharedPreferences sharedContributeflag = getSharedPreferences("Contribute_Flag", Context.MODE_PRIVATE);
            SharedPreferences.Editor editorflag = sharedContributeflag.edit();
            SharedPreferences.Editor editor = sharedPreferencesLastData.edit();
            editor.putString("DISTANCE", tdistance.getText().toString());
            editor.putString("DATE", datetext.getText().toString());
            editor.putString("STEPS", tSteps.getText().toString());
            editorflag.putInt("flag", Contributeflag);
            editor.apply();
            editorflag.apply();
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            GPSTracker gpsTracker = new GPSTracker(this, MapsActivity.this);
            double latitude = 0;
            double longitude = 0;
            LatLng latLng = null;
            if (gpsTracker.getIsGPSTrackingEnabled()) {
                latitude = gpsTracker.getLatitude();
                longitude = gpsTracker.getLongitude();
                latLng = new LatLng(latitude, longitude);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomlevel));

            } else {
                gpsTracker.showSettingsAlert();
            }
            //   stopLocation = LocationServices.FusedLocationApi.getLastLocation(client);
            //  LatLng latLng = new LatLng(stopLocation.getLatitude(), stopLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            currentLocationmarker = mMap.addMarker(markerOptions);
            final Date date = new Date();
            SimpleDateFormat Formater = new SimpleDateFormat("dd MMM yyyy");
            String datestr = Formater.format(date);
            tdistance = findViewById(R.id.distance);
            //tDistance.setText(String.valueOf());
            tName.setText(String.valueOf(Name + "'s Walk"));
            datetext.setText(datestr);
            String dist = String.format("%.0f m", totalDistance);
            tdistance.setText(dist);
            tSteps.setText(String.valueOf(totalSteps));
            stopwalk.setVisibility(View.INVISIBLE);
            startwalk.setVisibility(View.INVISIBLE);


           /* Dexter.withActivity(this)
                    .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    screenShot();
                                }
                            }, 1000);
                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {

                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                        }
                    }).check();*/
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(MapsActivity.this);
            alert.setCancelable(false);
            alert.setTitle("Confirm Message!");
            alert.setMessage("Do you want to Stop walk?");
            alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Contributeflag = 0;
                    options.getPoints().clear();
                    stopwalk.setClickable(false);
                    startwalk.setClickable(true);
                   // floatingMe.setVisibility(View.VISIBLE);
                    floatingHome.setVisibility(View.VISIBLE);
                    floatingcontribute.setVisibility(View.VISIBLE);
                    floatingShare.setVisibility(View.VISIBLE);
                    startwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_enable));
                    stopwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_disable));
                    Toast.makeText(getApplicationContext(), "Sorry! Your walk is too short to contribute", Toast.LENGTH_SHORT).show();
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    currentLocationmarker.remove();
                    startwalk.setVisibility(View.VISIBLE);
                    stopwalk.setVisibility(View.VISIBLE);
                }
            });

            AlertDialog dialog = alert.create();
            dialog.show();
        }
    }

    /*private void screenShot() {
        final Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);


        try {

            Instacapture.INSTANCE.capture(MapsActivity.this, new ScreenCaptureListener() {
                @Override
                public void onCaptureStarted() {
                    Log.d("ScreenCapture", "onCaptureStarted: ");
                }

                @Override
                public void onCaptureFailed(Throwable throwable) {
                    Log.d("ScreenCapture", "onCaptureFailed: " + throwable.getMessage());
                }

                @Override
                public void onCaptureComplete(final Bitmap bitmap) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(MapsActivity.this);
                    alert.setCancelable(false);
                    alert.setTitle("Confirm Message!");
                    alert.setMessage("Do you want to Stop walk?");
                    alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @SuppressLint("RestrictedApi")
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Contributeflag = 1;
                            // Get Stop location(Latlng), save to DBtance;
                            stopwalk.setClickable(false);
                            startwalk.setClickable(true);
                            startwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_enable));
                            stopwalk.setBackground(getResources().getDrawable(R.drawable.drawable_btn_maps_disable));
                            //rDistancelayout.setVisibility(View.INVISIBLE);
                            //rMaplayout.setPadding(0,0,0,0);
                            File directory = new File(mPath);
                            if (!(directory.exists())) {
                                new File(mPath).mkdirs();
                            }
                            String filenamestr = "/ClubD2S" + now + "_" + totalSteps + "steps_" + ((int) totalDistance) + "m" + ".jpg";
                            File imageFile = new File(mPath + filenamestr);
                            FileOutputStream outputStream = null;

                            Log.d("ScreenCapture", "onCaptureComplete: " + bitmap);

                            try {
                                outputStream = new FileOutputStream(imageFile);
                                int quality = 100;
                                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                                outputStream.flush();
                                outputStream.close();
                                //Toast toast;
                                Toast.makeText(getApplicationContext(), "Screenshot saved to: " + mPath, Toast.LENGTH_LONG).show();
                                startwalk.setVisibility(View.VISIBLE);
                                stopwalk.setVisibility(View.VISIBLE);
                               // floatingMe.setVisibility(View.VISIBLE);
                                floatingHome.setVisibility(View.VISIBLE);
                                floatingcontribute.setVisibility(View.VISIBLE);
                                floatingShare.setVisibility(View.VISIBLE);
                                options.getPoints().clear();
                                //toast.setGravity(Gravity.CENTER,0,0);
                                //toast.show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            currentLocationmarker.remove();
                            startwalk.setVisibility(View.VISIBLE);
                            stopwalk.setVisibility(View.VISIBLE);
                        }
                    });

                    AlertDialog dialog = alert.create();
                    dialog.show();

                }
            });

        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
            Log.d("ScreenCapture", "screenShot: " + e.getMessage());
        }
    }*/

    private void uploadscreenshot(final String screenshot) {
        if (Contributeflag == 1) {
            final String ph = DBHandlerWalk.Instance(getApplicationContext()).getPhoneNum();
            final ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Log.d(TAG, "Sending login initiate msg");
            retrofit2.Call<LoginResponse> loginResponseCall =
                    apiInterface.login("login_initiate_webservice.php?mobile_no=" + ph);
            loginResponseCall.enqueue(new retrofit2.Callback<LoginResponse>() {
                @Override
                public void onResponse(retrofit2.Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                    Log.d(TAG, "Got response!");
                    String password = response.body().getLoginResponse().get(0).getQueryResponse().getPassword();
                    String status = response.body().getLoginResponse().get(0).getQueryResponse().getStatus();
                    Log.d(TAG, "onResponse: " + password);
                    Log.d(TAG, "onResponse: " + status);
                    retrofit2.Call<LoginWebserviceResponse> loginWebserviceCall =
                            apiInterface.loginwebservice("login_webservice.php?mobile_no=" + ph + "&pass=" + password);
                    loginWebserviceCall.enqueue(new retrofit2.Callback<LoginWebserviceResponse>() {

                        @Override
                        public void onResponse(retrofit2.Call<LoginWebserviceResponse> call, retrofit2.Response<LoginWebserviceResponse> response) {
                            String userid = response.body().getLoginResponse().get(0).getQueryResponse().getUserId();
                            String status = response.body().getLoginResponse().get(0).getQueryResponse().getStatus();
                            Log.d(TAG, "userid: " + userid);
                            Log.d(TAG, "status: " + status);
                            Date dt = Calendar.getInstance().getTime();
                            SimpleDateFormat curFormater = new SimpleDateFormat("MMM dd,yyyy");
                            String dtstr = curFormater.format(dt);//Aug 01,2018
                            Log.d(TAG, "Current date : " + dtstr);

                            int stepsfortheday = DBHandlerWalk.Instance(getApplicationContext()).getTripStepCount(dtstr);
                            retrofit2.Call<LoginWebserviceResponse> loginWebserviceCall =
                                    apiInterface.loginwebservice("daily_steps_webservice.php?user_id=" + userid + "&step_date=" + dtstr + "&steps_count=" + totalSteps + "&steps_km=Steps&dailysteps_photo_upload=" + screenshot);
                            loginWebserviceCall.enqueue(new retrofit2.Callback<LoginWebserviceResponse>() {

                                @Override
                                public void onResponse(retrofit2.Call<LoginWebserviceResponse> call, retrofit2.Response<LoginWebserviceResponse> response) {
                                    String status = response.body().getLoginResponse().get(0).getQueryResponse().getStatus();
                                    Log.d(TAG, "status: " + status);
                                    Toast.makeText(getApplicationContext(), "Contribution Success! Total steps : " + String.valueOf(stepsfortheday), Toast.LENGTH_SHORT).show();
                                    Contributeflag = 0;

                                }

                                @Override
                                public void onFailure(retrofit2.Call<LoginWebserviceResponse> call, Throwable t) {
                                    Log.d(TAG, "onFailure: " + t.getMessage());
                                    Toast.makeText(getApplicationContext(), "Contribution failed, Try again!!", Toast.LENGTH_SHORT).show();

                                }
                            });

                        }

                        @Override
                        public void onFailure(retrofit2.Call<LoginWebserviceResponse> call, Throwable t) {
                            Log.d(TAG, "onFailure: " + t.getMessage());
                            //todo: display server communication error
                        }
                    });

                    //todo: goto to main page
                }

                @Override
                public void onFailure(retrofit2.Call<LoginResponse> call, Throwable t) {
                    //todo: display a msg to user saying invalid ph num. please register on onecroresteps websit
                    //open the website
                    Log.d(TAG, "onFailure: " + t.getMessage());
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "You do not have any steps to contribute.", Toast.LENGTH_LONG).show();
        }
    }


    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    public void openFolder() {
        Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/ClubD2S");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(selectedUri, "image/*");
        startActivity(intent);
    }

    private void shareScreenshot(File imageFile) {
        Uri uri = Uri.fromFile(imageFile);//Convert file path into Uri for sharing
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.sharing_text));
        intent.putExtra(Intent.EXTRA_STREAM, uri);//pass uri here
        startActivity(Intent.createChooser(intent, getString(R.string.share_title)));
    }

    private File getLatestFilefromDir(String dirPath) {
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile;
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

    }

    private class CustomViewTarget implements Target {

        private final View mView;
        private int offsetX;
        private int offsetY;

        public CustomViewTarget(View view) {
            mView = view;
        }

        public CustomViewTarget(int viewId, int offsetX, int offsetY, Activity activity) {
            this.offsetX = offsetX;
            this.offsetY = offsetY;
            mView = activity.findViewById(viewId);
        }


        @Override
        public Point getPoint() {
            int[] location = new int[2];
            mView.getLocationInWindow(location);
            int x = location[0] + mView.getWidth() / 2 + offsetX;
            int y = location[1] + mView.getHeight() / 2 + offsetY;
            return new Point(x, y);
        }
    }

}