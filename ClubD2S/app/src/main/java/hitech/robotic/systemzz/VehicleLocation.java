package hitech.robotic.systemzz;

public class VehicleLocation {

    /**
     * device : 1
     * speed : 13.000000
     * rpm : 12.000000
     * latitude : 12.000000
     * longitude : 12.000000
     * timestamp : 2017-10-04T14:07:44Z
     */

    private int device;
    private String speed;
    private String rpm;
    private String latitude;
    private String longitude;
    private String timestamp;

    public VehicleLocation(int device, String speed, String rpm, String latitude, String longitude, String timestamp) {
        this.device = device;
        this.speed = speed;
        this.rpm = rpm;
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getRpm() {
        return rpm;
    }

    public void setRpm(String rpm) {
        this.rpm = rpm;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
