package hitech.robotic.systemzz

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.previous_trips.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * In this class we are getting all data of previous trips and showing to user.
 */

class PreviousTrips:AppCompatActivity() {

    lateinit var firstore:FirebaseFirestore;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.previous_trips)
        getTripList()
    }


    /**
     * Getting trip from firestore.
     */
    private fun getTripList(){

        firstore = FirebaseFirestore.getInstance()
        val sharedPref =  getSharedPreferences("LOGIN_SESSION_ACTIVE",Context.MODE_PRIVATE)
        val userPhone = sharedPref.getString("PHONE",null)
        var simpleDateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.US)

        //val reportList = arrayListOf(mutableMapOf<String,String?>())
        val reportList = ArrayList<MutableMap<String,String?>>()
        rc_view.visibility = View.GONE
        blinkIcon(download_icon)

        firstore.collection("ClubD2S").document("TripData").collection("report")
                .document(userPhone).collection("all_report").orderBy("tripId", Query.Direction.DESCENDING).get().addOnSuccessListener { documentSnapshot ->
                    if (documentSnapshot!=null){
                        download_layout.visibility = View.GONE
                        rc_view.visibility = View.VISIBLE
                        download_icon.visibility = View.GONE
                        Log.d("pervious4>",documentSnapshot.size().toString())
                        for(documents in documentSnapshot){
                            val readWriteMaps = mutableMapOf<String,String?>()
                            readWriteMaps["FinalScore"] = documents.getString("FinalScore")
                            readWriteMaps["avgSpeed"] = documents.getString("avgSpeed")
                            readWriteMaps["distance"] = documents.getString("distance")
                            readWriteMaps["duration"] = documents.getString("duration")
                            readWriteMaps["endTime"] = documents.getString("endTime")
                            readWriteMaps["hardBreakPerThousand"] = documents.getString("hardBreakPerThousand")
                            readWriteMaps["hardbreak"] = documents.getString("hardbreak")
                            readWriteMaps["overSpeed"] = documents.getString("overSpeed")
                            readWriteMaps["overSpeedPerThousand"] = documents.getString("overSpeedPerThousand")
                            readWriteMaps["percentHardBreak"] = documents.getString("percentHardBreak")
                            readWriteMaps["percentOverSpeed"] = documents.getString("pecentOverSpeed")
                            readWriteMaps["percentPhoneUsages"] = documents.getString("percentPhoneUsages")
                            readWriteMaps["percentSharpTurn"] = documents.getString("perentSharpTurn")
                            readWriteMaps["phoneUsages"] = documents.getString("phoneUsages")
                            readWriteMaps["phoneUsagesPerThousand"] = documents.getString("phoneUsagesPerThousand")
                            readWriteMaps["sharpTurn"] = documents.getString("sharpTurn")
                            readWriteMaps["sharpTurnPerThousand"] = documents.getString("sharpTurnPerThousand")
                            readWriteMaps["startTime"] = documents.getString("startTime")
                            readWriteMaps["currentDate"] = documents.getString("currentDate")
                            readWriteMaps["tripId"] = documents.getString("tripId")

                            if (readWriteMaps["distance"]?.toDouble()!! <0.2){
                                documents.reference.delete().addOnSuccessListener {
                                    Log.e("deleted","successfully")
                                }
                            }else {

                                reportList.add(readWriteMaps)
                            }
                        }

                        Log.d("previoustrip>",reportList.size.toString() + " >> ")
                        Log.d("previous2>", reportList.toString())

                        if (reportList.size>0) {
                            rc_view.layoutManager = LinearLayoutManager(this)
                            rc_view.adapter = TripsAdapter(reportList, this)
                        }else{
                            no_trip_layout.visibility = View.VISIBLE
                            rc_view.visibility = View.GONE
                        }
                    }
                }

    }

    private fun blinkIcon(view: View){
        val anim = AlphaAnimation(0.0f,1.0f)
        anim.duration = 400
        anim.startOffset = 20
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = Animation.INFINITE
        view.startAnimation(anim)
    }

}