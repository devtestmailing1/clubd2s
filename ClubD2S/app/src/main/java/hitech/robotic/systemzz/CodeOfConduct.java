package hitech.robotic.systemzz;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import hitech.robotic.systemzz.utils.PreferenceHelper;

/**
 * This class sets the code of conduct page for user.
 */

public class CodeOfConduct extends AppCompatActivity {

    CheckBox checkBox;
    Button button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.code_of_conduct);

        checkBox = findViewById(R.id.checkbox);
        button = findViewById(R.id.submit);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    PreferenceHelper.setStringPreference(CodeOfConduct.this, "coc", "yes");
                    Toast.makeText(CodeOfConduct.this, "Thank you for accepting.", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }else {
                    Toast.makeText(CodeOfConduct.this, "Please accept code of conduct first.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (PreferenceHelper.getPreferences(CodeOfConduct.this).contains("coc")){
            checkBox.setChecked(true);
            checkBox.setText("Already Accepted. Thank You");
            checkBox.setEnabled(false);
            button.setEnabled(false);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
