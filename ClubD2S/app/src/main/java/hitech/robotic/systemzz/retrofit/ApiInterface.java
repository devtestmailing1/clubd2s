package hitech.robotic.systemzz.retrofit;

import hitech.robotic.systemzz.LoginResponse;
import hitech.robotic.systemzz.LoginWebserviceResponse;
import hitech.robotic.systemzz.RegistrationResponse;
import hitech.robotic.systemzz.RegistrationWebserviceResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET
    Call<LoginResponse> login(@Url String url);

    @GET
    Call<LoginWebserviceResponse> loginwebservice(@Url String url);

    @GET
    Call<RegistrationResponse> registration(@Url String url);

    @GET
    Call<RegistrationWebserviceResponse> registrationwebservice(@Url String url);

}
