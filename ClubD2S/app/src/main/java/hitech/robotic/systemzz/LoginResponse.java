package hitech.robotic.systemzz;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Login response handler class.
 */

public class LoginResponse {

    @SerializedName("response")
    public List<LoginResponseResult> loginResponse;

    public List<LoginResponseResult> getLoginResponse() { return loginResponse; }
}
