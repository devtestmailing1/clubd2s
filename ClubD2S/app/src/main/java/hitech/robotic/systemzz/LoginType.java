package hitech.robotic.systemzz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class LoginType extends AppCompatActivity implements View.OnClickListener {

    private CardView individual, corporate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_type);
        initializeViews();
    }

    private void initializeViews(){
        individual = findViewById(R.id.individual);
        corporate = findViewById(R.id.corporate);
        individual.setOnClickListener(this);
        corporate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.individual:
                Intent intent = new Intent(LoginType.this, CameraSplashActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.corporate:
                Intent intent1 = new Intent(LoginType.this, SelectCorporate.class);
                startActivity(intent1);
                finish();
                break;
        }
    }
}
