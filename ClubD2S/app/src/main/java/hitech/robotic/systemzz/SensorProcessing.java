package hitech.robotic.systemzz;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnFailureListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

import hitech.robotic.systemzz.OBD.TripDatabaseHandler;
import hitech.robotic.systemzz.utils.PreferenceHelper;

public class SensorProcessing extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private SensorEventListener accelerometerListener, eventListener;
    ArrayList<Float> z_accel = new ArrayList<Float>();
    private int accel_window_size = 15, hardBreakCounter = 0, overSpeedCounter = 0;
    double accelX, accelY, accelZ, linearAccel, mLastX, mLastY, mLastZ, Speed = 0, speedHolder = 0, latitude[], longitude[], lat, longi;
    private float[] linear_acceleration = new float[3];
    private boolean mInitialized, isTimerRunning = false, isOverSpeedTimerRunning = false, overSpeedHolderFlag = false, isGyroTimerRunning;
    private final float NOISE = (float) 2.0;
    private CopyOnWriteArrayList<Double> avgSpeedArray = new CopyOnWriteArrayList<>();
    private String brakeEvent = " ",sPeed_units = " ", OverSpeedLimit,Beep, gryoEvent;
    private SensorManager mSensorManager, sensorManager;
    private Sensor mAccelerometer;
    GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 500;
    LocationCallback locationCallback;
    ArrayList<Location> locations = new ArrayList<>();
    private FusedLocationProviderClient mFusedLocationClient, mFLocationClient;
    private CountDownTimer overSpeedTimer,countDownTimer;
    ToneGenerator toneGen1;
    private ArrayList<Float> avgSpeedPointsList = new ArrayList<>();
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Long times[];
    private Location mCurrentLocation;
    private LocationSettingsRequest mLocationSettingsRequest;
    private float timestamp;
    private Sensor sensor;
    private static final float NS2S = 1.0f / 1000000000.0f;
    CopyOnWriteArrayList<Double> oMagnitude = new CopyOnWriteArrayList<>();
    private final float[] deltaRotationVector = new float[4];
    private int gyroCounter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorManager.registerListener(accelerometerListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(accelerometerListener, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);

        toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 50000);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        OverSpeedLimit = String.valueOf(sharedPreferences.getInt("SEEKBAR_VALUE", 60));
        Beep = sharedPreferences.getString("BEEP_UPLOAD", "");
        String speed_units = sharedPreferences.getString(getString(R.string.speed_Units), "0");

        if (speed_units.equals("0")) {
            sPeed_units = "km/hr";
        }
        if (speed_units.equals("1")) {
            sPeed_units = "mph";

        }


        enableGPS();

        if (!checkPlayServices()) {
            Toast.makeText(SensorProcessing.this, "You need to install google play services to use app properly", Toast.LENGTH_SHORT).show();
        }

        calculateTotalTravelDistance();

    }

    private void startAccelorameter(){
        accelerometerListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {

                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];
                float z = sensorEvent.values[2];

                if (z_accel.size() < accel_window_size) {
                    z_accel.add(z);
                } else {
                    z_accel.remove(0);
                    z_accel.add(z);
                }

                accelX = x;
                accelY = y;
                accelZ = z;
                final float alpha = 0.8f;

                linear_acceleration = sensorEvent.values;
                linearAccel = Math.sqrt((linear_acceleration[0] * linear_acceleration[0]) +
                        (linear_acceleration[1] * linear_acceleration[1]) +
                        (linear_acceleration[2] * linear_acceleration[2]));


                if (!mInitialized) {
                    mLastX = x;
                    mLastY = y;
                    mLastZ = z;
                    mInitialized = true;
                } else {
                    double deltaX = (mLastX - x);
                    double deltaY = (mLastY - y);
                    double deltaZ = (mLastZ - z);

                    if (deltaX < NOISE) deltaX = (float) 0.0;
                    if (deltaY < NOISE) deltaY = (float) 0.0;
                    if (deltaZ < NOISE) deltaZ = (float) 0.0;

                    if (z > 3) {
                        //Toast.makeText(getApplicationContext(),"Moving back: " + z, Toast.LENGTH_SHORT).show();
                        Log.d("Accel: ", "+ve accel: " + z_accel.get(0));
                    }
                    if (z < -3) {
                        Log.d("Accel: ", "-ve accel: " + z_accel.get(0));
                        //Toast.makeText(getApplicationContext(),"Moving forward: " + z, Toast.LENGTH_SHORT).show();
                    }

                    mLastX = x;
                    mLastY = y;
                    mLastZ = z;

                    double sum = 0;
                    float sumZ = 0;
                    for (Double d : avgSpeedArray)
                        sum += d;

                    for (Float k : z_accel)
                        sumZ += k;

                    Double avgSpeed = sum / 10;
                    float avgZ = sumZ / accel_window_size;


                    // if (linearAccel > 5 && avgSpeed > 20) { //yahan bi 20 karna hai

                    if (avgZ < -3) {
                        Log.d("Hard break hit >>>", "hard break hit");
                        brakeEvent = "HARD BRAKE";
                        Log.d("xyzm ", " x > " + x + " y > " + y + " z > " + z + " > " + linearAccel + " > " + avgZ);
                        breakTimer();

                    }
                    //  }
                }

            }


            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    }



    private void breakTimer() {
        if (!isTimerRunning) {

            runOnUiThread(new Runnable() {
                public void run() {

                     new CountDownTimer(3000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            isTimerRunning = true;
                        }

                        public void onFinish() {
                            isTimerRunning = false;
                            saveEventInDatabase(brakeEvent);
                            brakeEvent = " ";
                            hardBreakCounter++;
                        }
                    }.start();

                }
            });
        }
    }


    private void saveEventInDatabase(String eventName) {

        Log.d("background", "background");

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
        String timeStamp = ((Long) (System.currentTimeMillis() / 1000)).toString();
        String date = dateFormat.format(new Date());
        String time = timeFormat.format(Calendar.getInstance().getTime());

        if (!PreferenceHelper.getPreferences(this).contains("tripId")) {
            PreferenceHelper.setStringPreference(this, "tripId", timeStamp);
        }

        String tripId = PreferenceHelper.getStringPreference(this, "tripId");

        TripDatabaseHandler databaseHandler = new TripDatabaseHandler(this);
        databaseHandler.open();
        databaseHandler.insertTripData(timeStamp, date, time, eventName, tripId);
        databaseHandler.getTripData();

    }


    private void enableGPS() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(SensorProcessing.this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            try {
                                status.startResolutionForResult(SensorProcessing.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            break;
                    }
                }
            });
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }

        return true;
    }


    private void calculateTotalTravelDistance() {

        LocationRequest mLocationRequest;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        // mLocationRequest.setSmallestDisplacement(2);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                if (locationResult.getLastLocation() != null) {
                    Log.d("lastlocations1>>>>>", locationResult.getLastLocation().getLatitude() + " >> " +
                            locationResult.getLastLocation().getLongitude());

                    locationCollector(locationResult.getLastLocation());

                    Log.d("lastlocation5>>>>>>", locationResult.getLastLocation().getBearing() + " >>> " +
                            locationResult.getLastLocation().bearingTo(locations.get(0)));

                }
            }


            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
                Log.d("lastlocation4>>", locationAvailability.isLocationAvailable() + "");
            }
        };

        mFLocationClient = LocationServices.getFusedLocationProviderClient(SensorProcessing.this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFLocationClient.getLastLocation().addOnSuccessListener(this, location -> {

            if (location != null) {
                Log.d("lastlocation2 >>>> ", location.getLatitude() + " " + location.getLongitude());
                locationCollector(location);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("lastloaction3>", e.getMessage());
            }
        });

        mFLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper());

    }


    private void locationCollector(Location location) {

        if (locations.size()==0){
            locations.add(location);
        }else {
            if (getDistanceBetweenTwoLocations(locations.get(locations.size()-1).getLatitude(),
                    locations.get(locations.size()-1).getLongitude(),location.getLatitude(),location.getLongitude()) > 0.2){
                locations.add(location);
            }
        }
        Log.e("uploadinglll>", locations.size() + " >>> " + locations.toString());
    }


    private double getDistanceBetweenTwoLocations(double startLat, double startLong, double endLat, double endLong) {
        float[] result = new float[10];
        Location.distanceBetween(startLat, startLong, endLat, endLong, result);
        double distatnce = ((double) result[0]) / 1000;
        // Log.d("distancedou>", (double)result[0] + " > " + distatnce+" > "+ round(distatnce,2) + " > " + BigDecimal.valueOf(distatnce).setScale(0,RoundingMode.HALF_UP));
        //  return (BigDecimal.valueOf(distatnce).setScale(0,RoundingMode.HALF_UP)).doubleValue();
        return distatnce;
    }


    private void continueRunner(){

        if (sPeed_units.equals("mph")) {

            if (((int) (Speed * 0.621371)) > Integer.valueOf(OverSpeedLimit) * 0.621371) {
                overSpeedTimer();
            } else {
                overSpeedHolderFlag = false;
            }

        } else {

            if (((int) (Speed)) > Integer.valueOf(OverSpeedLimit)) {
                //  if (((int) (Speed)) > -1) {
                overSpeedTimer();
            } else {
                overSpeedHolderFlag = false;
            }
        }

        calculateSpeedPoints();

    }


    private void overSpeedTimer() {

        if (!isOverSpeedTimerRunning) {

            runOnUiThread(new Runnable() {
                public void run() {

                    overSpeedTimer = new CountDownTimer(3000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            // Log.d("Timer Start >>>", "timer start");
                            isOverSpeedTimerRunning = true;
                        }

                        public void onFinish() {
                            //  Log.d("Timer Finish >>>", "timer finsh");

                            isOverSpeedTimerRunning = false;
                            brakeEvent = " ";

                            if (!overSpeedHolderFlag) {

                                if (Beep.equals("ON")) {
                                    toneGen1.startTone(AudioManager.STREAM_MUSIC, 800);
                                }

                                saveEventInDatabase(brakeEvent);
                                overSpeedCounter++;
                                overSpeedHolderFlag = true;
                            }

                        }
                    }.start();
                }
            });
        }
    }


    private void calculateSpeedPoints() {

        if (speedHolder != Speed) {
            avgSpeedPointsList.add((float) Speed);
            speedHolder = Speed;
        }
    }

    private void getSpeed(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                Integer counter = 0;
                latitude = new double[2];
                longitude = new double[2];
                times = new Long[2];

                super.onLocationResult(locationResult);
                // location is received
                Double d1;
                Long t1;
                // Double speed = 0.0;
                d1 = 0.0;
                t1 = 0l;
                mCurrentLocation = locationResult.getLastLocation();
                lat = mCurrentLocation.getLatitude();
                longi = mCurrentLocation.getLongitude();
                latitude[counter] = mCurrentLocation.getLatitude();
                longitude[counter] = mCurrentLocation.getLongitude();
                times[counter] = mCurrentLocation.getTime();
                try {

                    d1 = distance(latitude[counter], longitude[counter], latitude[(counter + 1) % 2], longitude[(counter + 1) % 2]);
                    t1 = times[counter] - times[(counter + 1) % 2];
                } catch (NullPointerException e) {

                }
                if (mCurrentLocation.hasSpeed()) {
                    Speed = mCurrentLocation.getSpeed() * 1.0;
                } else {
                    if (t1 == 0) Speed = 0.0;
                    else Speed = d1 / t1;
                }

                counter = (counter + 1) % 2;
                Speed = Speed * 3.6d;

                calculateAvgSpeed();
            }
        };

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }


    private double deg2rad(double deg) {

        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {

        return (rad * 180.0 / Math.PI);
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        // haversine great circle distance approximation, returns meters
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60; // 60 nautical miles per degree of seperation
        dist = dist * 1852; // 1852 meters per nautical mile
        return (dist);
    }

    private void calculateAvgSpeed() {
        if (avgSpeedArray.size() < 10) {
            avgSpeedArray.add(Speed);
        } else {
            avgSpeedArray.remove(0);
            avgSpeedArray.add(Speed);
        }

    }


    private void checkGyroscope(){

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        eventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                // timestamp = event.timestamp;
                //  Log.d("timeStamp>>>",timestamp+"");

                if (timestamp != 0) {
                    final float dT = (event.timestamp - timestamp) * NS2S;
                    // Axis of the rotation sample, not normalized yet.
                    float axisX = event.values[0];
                    float axisY = event.values[1];
                    float axisZ = event.values[2];

                    // Calculate the angular speed of the sample
                    double omegaMagnitude = Math.sqrt(axisX * axisX + axisY * axisY + axisZ * axisZ);

                    if (oMagnitude.size()<10){
                        oMagnitude.add(omegaMagnitude);
                    }else {
                        oMagnitude.remove(0);
                        oMagnitude.add(omegaMagnitude);
                    }

                    // Normalize the rotation vector if it's big enough to get the axis
                    // (that is, EPSILON should represent your maximum allowable margin of error)
                    if (omegaMagnitude > 0.000000001f) {
                        axisX /= omegaMagnitude;
                        axisY /= omegaMagnitude;
                        axisZ /= omegaMagnitude;
                    }

                    checkGyro();


                    double thetaOverTwo = omegaMagnitude * dT / 2.0f;
                    float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
                    float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
                    deltaRotationVector[0] = sinThetaOverTwo * axisX;
                    deltaRotationVector[1] = sinThetaOverTwo * axisY;
                    deltaRotationVector[2] = sinThetaOverTwo * axisZ;
                    deltaRotationVector[3] = cosThetaOverTwo;

                    //  Log.d("rotationvector> ", deltaRotationVector[0]+ " > "+deltaRotationVector[1] + " > "+
                    //   deltaRotationVector[2]+" > "+deltaRotationVector[3]);

                    //  Log.d("rotationvector>1 ",axisX+" > "+axisY+" > "+axisZ + " > "+ omegaMagnitude);

                    /*writeDataToCSV(axisX+"",axisY+"",axisZ+"",omegaMagnitude+"",
                            deltaRotationVector[0]+"",deltaRotationVector[1]+"",
                            deltaRotationVector[2]+"",deltaRotationVector[3]+"");*/

                }

                timestamp = event.timestamp;
                float[] deltaRotationMatrix = new float[9];
                SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);


            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        sensorManager.registerListener(eventListener,sensor,SensorManager.SENSOR_DELAY_NORMAL);

    }

    private void checkGyro(){

       /* Random r = new Random();
        double randomValue = 10 + (40 - 10) * r.nextDouble();*/

        //  avgSpeedArray.add(randomValue);

        double avgSpeed = 0,avgGyro = 0;

        if (avgSpeedArray!=null && avgSpeedArray.size()>0) {
            double sum = 0;
            for (Double d : avgSpeedArray)
                sum += d;
            avgSpeed = sum / 10;
        }


        if (oMagnitude.size()>0) {
            double gyroValue = 0;
            for (Double dd : oMagnitude)
                gyroValue += dd;
            avgGyro = gyroValue / 10;
        }

        //  Log.d("gyro>>> ", avgSpeed + " >> " + avgGyro);


        if (avgSpeed > 40 && avgGyro>1.3){
            gryoEvent = "Sharp Turn";
            gyroTimer();
        }

    }



    private void gyroTimer() {
        if (!isGyroTimerRunning) {

            runOnUiThread(new Runnable() {
                public void run() {

                    countDownTimer = new CountDownTimer(3000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            isGyroTimerRunning = true;
                        }

                        public void onFinish() {
                            isGyroTimerRunning = false;
                            // saveEventInDatabase(brakeEvent);
                            gyroCounter++;
                            gryoEvent = " ";
                        }
                    }.start();

                }
            });
        }
    }


}
