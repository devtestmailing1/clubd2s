package hitech.robotic.systemzz

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity

class TwoOptions:AppCompatActivity(), View.OnClickListener {

    private lateinit var startNewTrip:LinearLayout
    private lateinit var oldTrips:LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.two_options)
        initializeViews()
    }

    private fun initializeViews(){
        startNewTrip = findViewById(R.id.start_new_trip)
        oldTrips = findViewById(R.id.old_trips)

        startNewTrip.setOnClickListener(this)
        oldTrips.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
       when (v?.id) {
           R.id.start_new_trip -> goAhead(HomeActivity::class.java)
           R.id.old_trips -> goAhead(HomeActivity::class.java)
       }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        val intent  = Intent(this,HomeActivity::class.java)
        startActivity(intent)
    }

    private fun goAhead(className: Class<HomeActivity>) {
        val intent  = Intent(this,className)
        startActivity(intent)
        finish()
    }
}