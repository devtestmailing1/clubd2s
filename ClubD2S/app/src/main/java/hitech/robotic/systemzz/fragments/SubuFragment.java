package hitech.robotic.systemzz.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;

import hitech.robotic.systemzz.HttpHandler;

import hitech.robotic.systemzz.R;

public class SubuFragment extends Fragment {

    private static final String TAG = "Subbu";
    public String jsonstr;
    WebView wv1;
    String url="https://www.onecroresteps.com/tracker_url_webservice.php";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_subu, container, false);

        new GetContacts().execute();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                wv1= view.findViewById(R.id.web_view1);
                Log.d(TAG, "onCreate: "+jsonstr);
                wv1.loadUrl(jsonstr);
                wv1.getSettings().setJavaScriptEnabled(true);
                wv1.getSettings().setDomStorageEnabled(true);
                wv1.getSettings().setSaveFormData(true);
                wv1.getSettings().setAllowContentAccess(true);
                wv1.getSettings().setAllowFileAccess(true);
                wv1.canGoBack();
                wv1.setOnKeyListener(new View.OnKeyListener() {

                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK
                                && event.getAction() == MotionEvent.ACTION_UP
                                && wv1.canGoBack()) {
                            wv1.goBack();
                            return true;
                        }
                        return false;
                    }
                });
                wv1.getSettings().setAllowFileAccessFromFileURLs(true);
                wv1.setWebViewClient(new WebViewClient());
                wv1.setWebViewClient(new WebViewClient() {

                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);

                        return true;
                    }
                });
            }
        }, 1000);


        return view;
    }
    private class GetContacts extends AsyncTask<Void, Void, Void> {



        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            jsonstr = sh.makeServiceCall(url);

            jsonstr = parsing(jsonstr);
            Log.d(TAG, "doInBackground: "+jsonstr);


            return null;
        }
        String parsing(String s)
        {
            int loc = s.indexOf("Tracker_URL");
            String json = s.substring(loc + 1);
            json = json.substring(json.indexOf("h"));
            json = json.replaceAll("\\\\" , "");
            json = json.replaceAll("\\}" , "");
            json = json.replaceAll("\\]" , "");
            json = json.replaceAll("\\\"" , "");
            return json;
        }

    }

}
