package hitech.robotic.systemzz;

public class RegInfo {

    private int regid;
    private String name;
    private String emailid;
    private String phonenum;

    RegInfo(){}
    RegInfo(String nm, String eid, String ph )
    {
        this.name = nm;
        this.emailid = eid;
        this.phonenum = ph;
    }
    public int getRegid() {        return regid; }

    public void setRegid(int regid) {        this.regid = regid;    }

    public String getName() {        return name;    }

    public void setName(String name) {        this.name = name;    }

    public String getEmailid() {        return emailid;    }

    public void setEmailid(String emailid) {        this.emailid = emailid;    }

    public String getPhonenum() {        return phonenum;    }

    public void setPhonenum(String phonenum) {        this.phonenum = phonenum;    }

}
