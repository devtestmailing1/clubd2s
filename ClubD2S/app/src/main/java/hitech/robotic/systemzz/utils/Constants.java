package hitech.robotic.systemzz.utils;


import android.os.Environment;

import com.amazonaws.regions.Regions;

public interface Constants {
    String PATH = "/data/data";
    //String BASE_URL = "http://14.141.23.100:8089/hitech-api/";
    String BASE_URL ="https://www.onecroresteps.com/";
    String PHRASE_SERVICE_BROADCAST_ID = "com.cryogenos.safephrase.PHRASE_SERVICE_BROADCAST_ID";
    String SMSURL = "http://api.msg91.com/api/sendhttp.php?route=4&sender=CLUBDS&mobiles=9315940569&authkey=276408A34tOG0XcEg5cd9302e&message=Hello! This is a test message&country=91";
    //Database

    String DBNAME = "hiTechDB";
    String VIDEO_DETAILS_TABLE = "videos_data";

    String ID = "id";

    String NAME = "name";

    String DURATION = "duration";

    String SIZE = "size";
    String LAST_UPLOADED_VIDEO = "lastUploadedVideo";
    String LAST_UPLOADED_TIMESTAMP = "lastUploadedTimeStamp";

    String GUEST = "guest";

    String USER_TYPE = "user_type";

    String STORAGE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/hiTech/";

    Regions MY_REGION = Regions.AP_SOUTH_1;
    String IDENTITY_POOL_ID = "ap-south-1:60b62822-1981-4d8d-93cf-56a80f699751";
    String BUCKET = "sample-data-android";
}