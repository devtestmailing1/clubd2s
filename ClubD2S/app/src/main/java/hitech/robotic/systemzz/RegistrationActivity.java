package hitech.robotic.systemzz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hitech.robotic.systemzz.retrofit.ApiInterface;
import retrofit2.Call;
import retrofit2.Response;


/**
 * This class set up registration process for user.
 */
public class
RegistrationActivity extends AppCompatActivity {
    private static final String TAG = "RegistrationActivity";
    String nm,em,ph,Otp;

    @BindView(R.id.edit_user_name)
    EditText editUserName;
    @BindView(R.id.edit_email_id)
    EditText editEmailId;
    @BindView(R.id.edit_mobile_no)
    EditText editMobileNo;
    @BindView(R.id.edit_otp)
    EditText editOtp;
    @BindView(R.id.btn_registration)
    Button btnRegistration;
    @BindView(R.id.btn_otp)
    Button btnOTP;

    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mProgressDialog = new ProgressDialog(RegistrationActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);

        ButterKnife.bind(this);
        //if user is already registered, go to map activity directly
    }


    /**
     * This method sends OTP during user's registration.
     */
    public void sendOTPVerify(){
        mProgressDialog.show();
        final HorusInterface horusInterface = HorusClient.getClient().create(HorusInterface.class);
        final ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Log.d(TAG, "Sending registration details to Horusi Webserver");
        HorusUser usr = new HorusUser(1, em,ph,nm,"none");
        Call<List<HorusUser>>registerUserCall =
                horusInterface.registerUser(usr);
        registerUserCall.enqueue(new retrofit2.Callback<List<HorusUser>>() {
            @Override
            public void onResponse(Call<List<HorusUser>> call, Response<List<HorusUser>> response) {
               mProgressDialog.dismiss();
                Log.d(TAG, "onResponse: Sent user details to Horus webserver successfully!");
                RegInfo reg = new RegInfo(nm, em, ph);
                DBHandlerWalk.Instance(getApplicationContext()).addHandler(reg);
            }

            @Override
            public void onFailure(Call<List<HorusUser>> call, Throwable t) {
                mProgressDialog.dismiss();
                Log.d(TAG, "onFailure: Failed to send details to Horus webserver. "+t.getMessage());
            }
        });

        mProgressDialog.show();
        Log.d(TAG, "Sending registration initiate msg");
        Call<RegistrationResponse> registrationResponseCall =
                apiInterface.registration("/registration_initiate_webservice.php?mobile_no="+ph);
        registrationResponseCall.enqueue(new retrofit2.Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
               mProgressDialog.dismiss();
                Log.d(TAG, "Got response!");
                String status = response.body().getRegistrationResponse().get(0).getQueryResponse().getStatus();
                String mobile = response.body().getRegistrationResponse().get(0).getQueryResponse().getMobile();
                String otp = response.body().getRegistrationResponse().get(0).getQueryResponse().getOtp();
                Log.d(TAG, "status: " + status);
                Log.d(TAG, "mobile: " + mobile);
                Log.d(TAG, "otp: " + otp);
                if (status.equalsIgnoreCase("OTP sent successfully.")) {
                    Log.d(TAG, "Registration initiate successful!");
                    editEmailId.setVisibility(View.INVISIBLE);
                    editMobileNo.setVisibility(View.INVISIBLE);
                    editUserName.setVisibility(View.INVISIBLE);
                    editOtp.setVisibility(View.VISIBLE);
                    btnOTP.setVisibility(View.INVISIBLE);
                    btnRegistration.setVisibility(View.VISIBLE);
                } else if (status.equalsIgnoreCase("user already registered.")) {
                    Toast.makeText(getApplicationContext(),"User is already registered.",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
                else {
                    Log.d(TAG, "Registration initiate failed." + status);
                    Toast.makeText(getApplicationContext(), "Registration initiate failed. "+status, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Registration initiate failed. "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    @OnClick(R.id.btn_registration)
    public void clickRegister(){
        Otp=editOtp.getText().toString().trim();
        if(Otp.isEmpty())
        {
            editOtp.setError("Please enter OTP");
        } else
        {
            mProgressDialog.show();
            final ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<RegistrationWebserviceResponse> registrationWebserviceCall =
                    apiInterface.registrationwebservice("/otp_verify_webservice.php?mobile_no=" + ph + "&otp=" + Otp);
            registrationWebserviceCall.enqueue(new retrofit2.Callback<RegistrationWebserviceResponse>() {

                @Override
                public void onResponse(Call<RegistrationWebserviceResponse> call, Response<RegistrationWebserviceResponse> response) {
                   mProgressDialog.dismiss();
                    String status = response.body().getRegistrationResponse().get(0).getQueryResponse().getStatus();
                    String mobile = response.body().getRegistrationResponse().get(0).getQueryResponse().getMobile();
                    String otp = response.body().getRegistrationResponse().get(0).getQueryResponse().getOtp();
                    String pswd = response.body().getRegistrationResponse().get(0).getQueryResponse().getPassword();
                    Log.d(TAG, "status: "+ status);
                    Log.d(TAG, "mobile: "+ mobile);
                    Log.d(TAG, "otp: "+ otp);
                    Log.d(TAG, "password: "+ pswd);
                    Toast.makeText(getApplicationContext(),"SignupSucess",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                    startActivity(intent);
                    //goto to main page
                }

                @Override
                public void onFailure(Call<RegistrationWebserviceResponse> call, Throwable t) {
                   mProgressDialog.dismiss();
                    Log.d(TAG, "onFailure: " + t.getMessage());
                }
            });
        }

    }

    @OnClick(R.id.btn_otp)
    public void clickOtp(){
        intialize();
        if(!validate())
        {
            Toast.makeText(this,"Register has failed!, Complete the missing Fileds", Toast.LENGTH_SHORT).show();
        } else
        {
            sendOTPVerify();
        }
    }

    public void intialize()
    {
        nm = editUserName.getText().toString();
        em = editEmailId.getText().toString().trim();
        ph = editMobileNo.getText().toString().trim();
        Otp=editOtp.getText().toString().trim();
    }

    /**
     * Here We are validating all the fields.
     * @return
     */
    public boolean validate(){
        boolean valid=true;
        if(nm.isEmpty()||nm.length()>20)
        {
            editUserName.setError("Please enter valid Name");
            valid=false;
        }


        if(ph.isEmpty()||ph.length()>10 || ph.length()<10)
        {
            editMobileNo.setError("Please enter Valid Phone Number");
            valid=false;
        }

        if(em.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(em).matches())
        {
            editEmailId.setError("Please enter valid Email");
            valid=false;
        }

        return valid;
    }

}
