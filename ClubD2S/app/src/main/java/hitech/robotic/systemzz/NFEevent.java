package hitech.robotic.systemzz;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class NFEevent {

    @SerializedName("accelerator")
    @Expose
    private String accelerator;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("unixTime")
    @Expose
    private String unixTime;
    @SerializedName("mediaLink")
    @Expose
    private String mediaLink;
    @SerializedName("nfe")
    @Expose
    private String nfe;
    @SerializedName("rpm")
    @Expose
    private int rpm;
    @SerializedName("speed")
    @Expose
    private float speed;
    @SerializedName("video")
    @Expose
    private Boolean video;

    public NFEevent(String devid, String lat, String lng, String tp,
                    String tm, String lnk, String nfevent, float sp,
                    int rpmval, Boolean vd, String acc, String dist)
    {
        this.deviceId = devid;
        this.latitude = lat;
        this.longitude = lng;
        this.type = tp;
        this.unixTime = tm;
        this.accelerator = acc;
        this.mediaLink = lnk;
        this.nfe = nfevent;
        this.speed = sp;
        //todo: add distance after it is implemented in webserver
        this.rpm = rpmval;
        this.video = vd;
    }

    public String getAccelerator() { return accelerator; }

    public String getDeviceId() {
        return deviceId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getType() {
        return type;
    }

    public String getUnixTime() {
        return unixTime;
    }

    public String getMediaLink() {
        return mediaLink;
    }

    public String getNfe() {
        return nfe;
    }

    public int getRpm() {
        return rpm;
    }

    public float getSpeed() {
        return speed;
    }

    public Boolean getVideo() {
        return video;
    }

}
