package hitech.robotic.systemzz;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;

public class LeaderBoardAdapter extends RecyclerView.Adapter<LeaderBoardAdapter.ViewHolderr> {

    private HashMap<String,Integer>hashMap;
    private String phone;
    private Typeface customeFonts3;

    public LeaderBoardAdapter(HashMap<String,Integer> hashMap, String phone, Context context){
        this.hashMap = hashMap;
        this.phone = phone;
        this.customeFonts3 = Typeface.createFromAsset(context.getAssets(),"Roboto-Black.ttf");
    }


    @NonNull
    @Override
    public ViewHolderr onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.leaderboard_row,parent,false);
        ViewHolderr viewHolderr = new ViewHolderr(view);
        return viewHolderr;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderr holder, int position) {
        if (phone.equalsIgnoreCase(String.valueOf(hashMap.keySet().toArray()[position]))){
            holder.phone.setText("Your Rank");
        }else {
            holder.phone.setText(String.valueOf(hashMap.keySet().toArray()[position]));
        }
        holder.score.setText(String.valueOf(hashMap.get(hashMap.keySet().toArray()[position])));
        int p = position;
        holder.rank.setText((p+1)+"");
        holder.phone.setTypeface(customeFonts3);
        holder.score.setTypeface(customeFonts3);
        holder.rank.setTypeface(customeFonts3);
        Log.e("adapterdata>", String.valueOf(hashMap.keySet().toArray()[position]));
        Log.e("adapterdata1>",String.valueOf(hashMap.get(hashMap.keySet().toArray()[position])));
    }

    @Override
    public int getItemCount() {
        return hashMap.size();
    }

    public static class ViewHolderr extends RecyclerView.ViewHolder{
        public TextView phone, score, rank;
        public ViewHolderr(@NonNull View itemView) {
            super(itemView);
            phone = itemView.findViewById(R.id.phone_number);
            score = itemView.findViewById(R.id.ranking);
            rank = itemView.findViewById(R.id.rank);
        }
    }

}
