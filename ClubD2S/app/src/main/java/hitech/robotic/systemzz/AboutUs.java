package hitech.robotic.systemzz;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * This class shows the about us page.
 */

public class AboutUs extends AppCompatActivity {


    /**
     * This class only show layout of about us.
     */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);
    }
}
