package hitech.robotic.systemzz.OBD;

public class TripData {

    private String timeStamp;
    private String tripDate;
    private String tripTime;
    private String tripEventName;
    private String tripId;

    TripData(String timeStamp, String tripDate, String tripTime, String tripEventName, String tripId)
    {
        this.timeStamp = timeStamp;
        this.tripDate = tripDate;
        this.tripTime = tripTime;
        this.tripEventName = tripEventName;
        this.tripId = tripId;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getDate() {
        return tripDate ;
    }

    public void setTime(String tripTime) {
        this.tripTime = tripTime;
    }

    public String getTime() {
        return tripTime;
    }

    public void setEventName(String eventName){
        this.tripEventName = eventName;
    }

    public String getEventName(){
        return tripEventName;
    }

    public void setTripId(String tripId){
        this.tripId = tripId;
    }

    public String getTripId(){
        return tripId;
    }

}
