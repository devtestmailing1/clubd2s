package hitech.robotic.systemzz.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hitech.robotic.systemzz.DBHandlerWalk;
import hitech.robotic.systemzz.GpsLocation;
import hitech.robotic.systemzz.MainActivity;
import hitech.robotic.systemzz.Me;
import hitech.robotic.systemzz.R;
import hitech.robotic.systemzz.RegInfo;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalkFragment extends androidx.fragment.app.Fragment implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapClickListener,
        GoogleApiClient.ConnectionCallbacks,
        LocationListener {

    public static final int PERMISSION_REQUEST_CODE = 99;
    private static final String TAG = "MapsActivity";
    public int TripNum;
    Boolean a = false;
    @BindView(R.id.stopwalk)
    Button stopwalk;
    FloatingActionButton floatingAdd, floatingHome, floatingMe;
    Boolean visibility = true;
    double speed = 0.0;
    double distance = 0.0;
    double time = 0.0;
    Marker startLocationMarker;
    float zoomlevel = 16.0f;
    String Name;
    int i = 0;
    Marker currentMarker = null;
    Polyline line;
    Boolean loca = true;
    Vector<Location> points = new Vector<Location>();
    PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
    private long mLastUpdateTime;
    private GoogleMap mMap;
    private GoogleApiClient client;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private Marker currentLocationmarker;
    private EditText editText;
    private Button startwalk;
    private RelativeLayout rMaplayout;
    private LinearLayout rDistancelayout;
    private TextView tDistance, tSteps, tWalks, tName;
    private float totalDistance = 0.0f;
    private int totalSteps = 0;
    private Location startLocation;
    private Location stopLocation;
    private Location initLocation;
    private Unbinder bind;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_maps, container, false);

        RegInfo reg = DBHandlerWalk.Instance(getActivity().getApplicationContext()).findRegHandler();
        initView(view);
        Name = reg.getName();

        bind = ButterKnife.bind(this, view);


        floatingAdd.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {
                if (i == 0) {
                    floatingHome.setVisibility(View.VISIBLE);
                    floatingMe.setVisibility(View.VISIBLE);
                    i++;
                } else {
                    floatingHome.setVisibility(View.INVISIBLE);
                    floatingMe.setVisibility(View.INVISIBLE);
                    i = 0;
                }


            }
        });
        floatingHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent me = new Intent(getActivity(), MainActivity.class);
                startActivity(me);
            }
        });
        floatingMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent me = new Intent(getActivity(), Me.class);
                startActivity(me);
            }
        });
        //tTime.setText(String.valueOf(time));
        //tSpeed.setText(String.valueOf(totalSteps));
        //tDistance.setText(String.valueOf(totalDistance));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        return view;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (client.isConnected()) {
            locationRequest = new LocationRequest();
            locationRequest.setInterval(1000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, this);
            }
            if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            initLocation = LocationServices.FusedLocationApi.getLastLocation(client);
            if (initLocation != null) {
                LatLng latLng = new LatLng(initLocation.getLatitude(), initLocation.getLongitude());         // Get start location, save to DB
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomlevel));
                startwalk.setVisibility(View.VISIBLE);
                stopwalk.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        {
            if (initLocation == null) {
                initLocation = location;
                LatLng latLng = new LatLng(initLocation.getLatitude(), initLocation.getLongitude());         // Get start location, save to DB
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomlevel));
                startwalk.setVisibility(View.VISIBLE);
                stopwalk.setVisibility(View.VISIBLE);
            }
            Location prevLocation = lastLocation;
            lastLocation = location;
            if (((location.getTime() / 1000) - mLastUpdateTime) > 30) {
                //log it to local db once every 30 secs
                if (TripNum != 0) {
                    //wait until the db is read and the next trip num is updated
                    Log.d(TAG, "DB: Creating GPSLocation and calling dbHandler.addHandler");
                    GpsLocation loc = new GpsLocation(location.getTime() / 1000,
                            String.valueOf(location.getLatitude()),
                            String.valueOf(location.getLongitude()), TripNum);
                    DBHandlerWalk.Instance(getActivity().getApplicationContext()).addHandler(loc);
                    mLastUpdateTime = location.getTime() / 1000;
                }
            }
            if (prevLocation != null && lastLocation != null) {
                float distance = prevLocation.distanceTo(lastLocation);
                if (distance < 10.0f) {
                    int steps = (int) (1.32 * distance);
                    totalDistance += distance;
                    totalSteps += steps;
                    //TripData trip = new TripData(TripNum, lastLocation.getTime() / 1000, steps, distance);
                    //DBHandlerWalk.Instance(getApplicationContext()).addHandler(trip);
                } else {
                    lastLocation = prevLocation;//revert the erratic changes
                }
                LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                options.add(latLng);
                line = mMap.addPolyline(options);
            }

            if (location.hasAccuracy()) {
                if (location.getAccuracy() < 30) {/*
                if (line != null) {
                    mMap.clear();
                    line.remove();
                }
                if (currentLocationmarker != null)
                {
                    currentLocationmarker.remove();
                }
                Toast.makeText(getApplicationContext(), "Lat: " + lastLocation.getLatitude() + " Long: " + lastLocation.getLongitude(), Toast.LENGTH_LONG).show();
                LatLng latLng=new LatLng(location.getLatitude(),location.getLongitude());
                if (latLng !=null)
                {
                    points.add(location);  //We can store location here
                    options.add(latLng);
                }
                line = mMap.addPolyline(options);
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("Starting");
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

                currentLocationmarker=mMap.addMarker(markerOptions);
                //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                //mMap.animateCamera(CameraUpdateFactory.zoomBy(10));

                //if (client != null)
                //{
                //    LocationServices.FusedLocationApi.removeLocationUpdates(client,this);
                //}

            */
                }
            }
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private void initView(View view) {
        startwalk = view.findViewById(R.id.startwalk);
        rMaplayout = view.findViewById(R.id.maplayout);
        rDistancelayout = view.findViewById(R.id.distancelayout);
        //tDistance = view.findViewById(R.id.distance);
        tName = view.findViewById(R.id.nameId);
        floatingHome = view.findViewById(R.id.floatingbtn2);
        floatingMe = view.findViewById(R.id.floatingbtn3);
        //TextView tWalks = (TextView) view.findViewById(R.id.walk);
        //TextView tDistance = (TextView) view.findViewById(R.id.distance);
        TextView tSteps = view.findViewById(R.id.steps);
    }

    private void buildAlertMessageNoGps() {
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Toast.makeText(getActivity().getApplicationContext(), "Please enable GPS to use this function.", Toast.LENGTH_LONG).show();
                        Intent back = new Intent(getActivity(), MainActivity.class);
                        back.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(back);
                    }
                });
        final androidx.appcompat.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }

    @Override
    public void onResume() {
        super.onResume();
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            buildAlertMessageNoGps();
        }
    }


    @SuppressLint("RestrictedApi")
    public void onClick(View v) {
        if (v.getId() == R.id.stopwalk) {
            if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            stopLocation = LocationServices.FusedLocationApi.getLastLocation(client);
            LatLng latLng = new LatLng(stopLocation.getLatitude(), stopLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            currentLocationmarker = mMap.addMarker(markerOptions);

            final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setCancelable(false);
            alert.setTitle("Confirm Message!");
            alert.setMessage("Do you want to Stop walk?");
            alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Get Stop location(Latlng), save to DB
                    float distance = stopLocation.distanceTo(lastLocation);
                    int steps = (int) (1.32 * distance);
                    totalDistance += distance;
                    totalSteps += steps;

                    Toast.makeText(getActivity().getApplicationContext(), "Distance: " + distance + " Steps: " + steps, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    /*Dexter.withActivity(MapsActivity.this)
                            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .withListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted(PermissionGrantedResponse response) {
                                    screenShot();
                                }

                                @Override
                                public void onPermissionDenied(PermissionDeniedResponse response) {

                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                                }
                            }).check();*/


                    intent.putExtra("Steps", steps);
                    startActivity(intent);

                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    currentLocationmarker.remove();
                }
            });

            AlertDialog dialog = alert.create();
            dialog.show();

        }

        if (v.getId() == R.id.startwalk) {
            if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            floatingMe.setVisibility(View.INVISIBLE);
            floatingHome.setVisibility(View.INVISIBLE);
            floatingAdd.setVisibility(View.INVISIBLE);
            startLocation = LocationServices.FusedLocationApi.getLastLocation(client);
            LatLng latLng = new LatLng(startLocation.getLatitude(), startLocation.getLongitude());         // Get start location, save to DB
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Starting");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            currentLocationmarker = mMap.addMarker(markerOptions);
            startLocationMarker = currentLocationmarker;
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setCancelable(false);
            alert.setTitle("Confirm Message!");
            alert.setMessage("Do you want to Start walk?");
            alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    rDistancelayout.setVisibility(View.INVISIBLE);
                    rMaplayout.setPadding(0, 0, 0, 0);

                    // Get Stop location(Latlng), save to DBtance;
                    startwalk.setClickable(false);
                    startwalk.setBackgroundColor(getResources().getColor(R.color.grey));
                    stopwalk.setClickable(true);
                    stopwalk.setBackgroundColor(getResources().getColor(R.color.black));
                    TripNum = getNextTripNum();

                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    currentLocationmarker.remove();
                }
            });

            AlertDialog dialog = alert.create();
            dialog.show();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        client = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        client.connect();
    }

    public void Capture() {
    }

    public int getNextTripNum() {
        int nxtTripNum = 1;
        //extract last entry in gps table, inc the trip num
        nxtTripNum = DBHandlerWalk.Instance(getActivity().getApplicationContext()).getNextTripNum();
        Log.d(TAG, "Next Trip num : " + nxtTripNum);
        return nxtTripNum;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission is granted
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (client == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    //permision is denied
                    Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
                }
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            }
            return false;
        }
        return true;
    }

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.stopwalk)
    public void clickStopWalk() {
        rMaplayout.setPadding(0, 270, 0, 0);
        rDistancelayout.setVisibility(View.VISIBLE);
        startwalk.setVisibility(View.INVISIBLE);
        stopwalk.setVisibility(View.INVISIBLE);
        floatingMe.setVisibility(View.INVISIBLE);
        floatingHome.setVisibility(View.INVISIBLE);
        floatingAdd.setVisibility(View.INVISIBLE);

        //TripData trip = new TripData(TripNum,lastLocation.getTime()/1000, totalSteps, totalDistance);
        //DBHandlerWalk.Instance(getApplicationContext()).addHandler(trip);
        //Toast.makeText(getApplicationContext(),"Distance: " + totalDistance + " Steps: " + totalSteps, Toast.LENGTH_LONG).show();
        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        stopLocation = LocationServices.FusedLocationApi.getLastLocation(client);
        LatLng latLng = new LatLng(stopLocation.getLatitude(), stopLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        currentLocationmarker = mMap.addMarker(markerOptions);

        //tDistance.setText(String.valueOf());
        tName.setText(String.valueOf(Name + "'s Walk Route"));
        //tSteps.setText(String.valueOf(totalSteps));
        tSteps.setText((int) totalDistance + " m");
        //tWalks.setText("0");
        stopwalk.setVisibility(View.INVISIBLE);
        startwalk.setVisibility(View.INVISIBLE);


       /* Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                screenShot();
                            }
                        }, 1000);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                }).check();*/
    }

   /* private void screenShot() {
        final Date now = new Date();
        //Toast.makeText(getApplicationContext(), now.getDate(), Toast.LENGTH_LONG).show();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {

            Instacapture.INSTANCE.capture(getActivity(), new ScreenCaptureListener() {
                @Override
                public void onCaptureStarted() {
                    Log.d("ScreenCapture", "onCaptureStarted: ");
                }

                @Override
                public void onCaptureFailed(Throwable throwable) {
                    Log.d("ScreenCapture", "onCaptureFailed: " + throwable.getMessage());
                }

                @Override
                public void onCaptureComplete(final Bitmap bitmap) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setCancelable(false);
                    alert.setTitle("Confirm Message!");
                    alert.setMessage("Do you want to Stop walk?");
                    alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @SuppressLint("RestrictedApi")
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Get Stop location(Latlng), save to DBtance;
                            stopwalk.setClickable(false);
                            startwalk.setClickable(true);
                            startwalk.setBackgroundColor(getResources().getColor(R.color.black));
                            stopwalk.setBackgroundColor(getResources().getColor(R.color.grey));
                            startLocationMarker.remove();
                            //rDistancelayout.setVisibility(View.INVISIBLE);
                            //rMaplayout.setPadding(0,0,0,0);
                            String mPath = Environment.
                                    getExternalStorageDirectory().toString() + "/ClubD2S";
                            File directory = new File(mPath);
                            if (!(directory.exists())) {
                                new File(mPath).mkdirs();
                            }
                            File imageFile = new File(mPath + "/ClubD2S" + now + ".jpg");
                            FileOutputStream outputStream = null;

                            Log.d("ScreenCapture", "onCaptureComplete: " + bitmap);

                            try {
                                outputStream = new FileOutputStream(imageFile);
                                int quality = 100;
                                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                                outputStream.flush();
                                outputStream.close();
                                //Toast toast;
                                Toast.makeText(getActivity().getApplicationContext(), "Screenshot saved to: " + mPath, Toast.LENGTH_LONG).show();
                                currentLocationmarker.remove();
                                startwalk.setVisibility(View.VISIBLE);
                                stopwalk.setVisibility(View.VISIBLE);
                                floatingMe.setVisibility(View.VISIBLE);
                                floatingHome.setVisibility(View.VISIBLE);
                                floatingAdd.setVisibility(View.VISIBLE);
                                //toast.setGravity(Gravity.CENTER,0,0);
                                //toast.show();

                                shareScreenshot(imageFile);
                                //openScreenshot(imageFile);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            currentLocationmarker.remove();
                            startwalk.setVisibility(View.VISIBLE);
                            stopwalk.setVisibility(View.VISIBLE);
                            rMaplayout.setPadding(0, 0, 0, 0);
                            rDistancelayout.setVisibility(View.INVISIBLE);
                        }
                    });

                    AlertDialog dialog = alert.create();
                    dialog.show();

                }
            });

        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
            Log.d("ScreenCapture", "screenShot: " + e.getMessage());
        }
    }*/

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    public void openFolder() {
        Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/ClubD2S");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(selectedUri, "image/*");
        startActivity(intent);
    }

    private void shareScreenshot(File imageFile) {
        Uri uri = Uri.fromFile(imageFile);//Convert file path into Uri for sharing
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.sharing_text));
        intent.putExtra(Intent.EXTRA_STREAM, uri);//pass uri here
        startActivity(Intent.createChooser(intent, getString(R.string.share_title)));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }
}
