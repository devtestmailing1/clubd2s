package hitech.robotic.systemzz.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.PointF;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static android.content.Context.WINDOW_SERVICE;

public class Utils {
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static int screenWidth = 0;
    private static int screenHeight = 0;

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static boolean checkReadSMSPermission(Context mContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(mContext, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static double getVideoFileDuration(Context context, File file) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(context, Uri.fromFile(file));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        double timeInMillisec = Long.parseLong(time);
        double seconds = timeInMillisec / 1000.0;
        retriever.release();

        return seconds;
    }

    public static int getScreenHeight(Context c) {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        }

        return screenHeight;
    }


    public static int getScreenWidth(Context c) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }

        return screenWidth;
    }

    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp) {
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }


    public static String getDateFromString(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            SimpleDateFormat postFormater = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            String newDateStr = postFormater.format(date);
            newDateStr = newDateStr.replace(".", "");
            return newDateStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getFormatTime(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("HH:mm");
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mm a");
            String newDateStr = postFormater.format(date);
            newDateStr = newDateStr.replace(".", "");
            return newDateStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getOrderFormatDate(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM");
            String newDateStr = postFormater.format(date);
            //System.out.println("Date  : " + newDateStr);
            return newDateStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static boolean isValidDateRange(String fromDate, String toDate) {
        if (fromDate.equalsIgnoreCase("") && toDate.equalsIgnoreCase("")) {
            return true;
        }
        if (fromDate.equalsIgnoreCase("") || toDate.equalsIgnoreCase("")) {
            return false;
        }


        try {
            Date datefrom = new SimpleDateFormat("yyyy-MM-dd").parse(fromDate);
            Date dateto = new SimpleDateFormat("yyyy-MM-dd").parse(toDate);
            if (datefrom.compareTo(dateto) < 0) {
                return true;
            } else
                return false;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static String getDateString(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yy");
            String newDateStr = postFormater.format(date);
            return newDateStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static long getDateTimeMillisFromString(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            long millis = Calendar.getInstance().getTime().getTime() - date.getTime();

            return millis;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String generateCacheKeyWithParam(String url, Map<String, String> params) {
        for (Map.Entry<String, String> entry : params.entrySet()) {
            url += entry.getKey() + "=" + entry.getValue();
        }
        return url;
    }

    public static byte[] loadFile(File file) {
        try {
            InputStream is = new FileInputStream(file);

            long length = file.length();
            if (length > Integer.MAX_VALUE) {
                // File is too large
            }
            byte[] bytes = new byte[(int) length];

            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length
                    && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }

            if (offset < bytes.length) {
                throw new IOException("Could not completely read file " + file.getName());
            }

            is.close();
            return bytes;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     * Check Internet connection available or not.
     *
     * @param context : current activity context
     * @return boolean
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static boolean isGuestUser(Context context) {

        return PreferenceHelper.getBooleanPreference(context, Constants.GUEST);
    }

    /**
     * Open Dialog and Settings Internet connection available or not.
     *
     * @param mContext : current activity context
     * @return boolean
     */

    public static void displayAlert(final Context mContext) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setTitle("Network Error");
        alertDialog.setMessage("Would you like to turn on data!");
        alertDialog.setCancelable(false);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                mContext.startActivity(intent);
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @SuppressWarnings("deprecation")
    public static void showAlertDialog(final Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).finish();
            }
        });
        alertDialog.show();
    }

    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(Context context) {
        if (((Activity) context).getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public static void showSoftKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

    public static PointF PointOnCircle(float radius, float angleInDegrees, PointF origin) {
        // Convert from degrees to radians via multiplication by PI/180
        float x = (float) (radius * Math.cos(angleInDegrees * Math.PI / 180F)) + origin.x;
        float y = (float) (radius * Math.sin(angleInDegrees * Math.PI / 180F)) + origin.y;
        return new PointF(x, y);
    }


    public static void showLogoutConfirmDialog(final Context context) {
        new AlertDialog.Builder(context).setMessage("Are you sure to logout..?").setTitle("Confirmation").setCancelable(true).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
//                logOut(context);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).show();
    }

    static ProgressDialog mCustomProgressDialog;
    static ProgressDialog mProgressDialog;

    public static Dialog showProgressDialog(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCustomProgressDialog = new ProgressDialog(context);
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
            return mCustomProgressDialog;
        } else {

            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
            return mProgressDialog;
        }


    }

    public static void dismissProgressDialog() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (mCustomProgressDialog != null && mCustomProgressDialog.isShowing()) {
                mCustomProgressDialog.dismiss();
            }
        } else {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }


    }


    public static boolean isValidMail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidMobile(String mobile) {
        return Patterns.PHONE.matcher(mobile).matches();
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String getCurrentTime() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
        String formattedDate = df.format(c.getTime());

        formattedDate = formattedDate.replace(".", "");
        return formattedDate;
    }

    public static String getDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("MMMM d, yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static Date getDate(String date) {

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date formattedDate = null;
        try {
            formattedDate = df.parse(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTime(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdfOut = new SimpleDateFormat("hh:mm a");
        String time = "";
        try {
            time = sdfOut.format(sdf.parse(date));

            time = time.replace(".", "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static String getAndroidId(Context mContext) {
        String androidId = null;
        try {
            androidId = Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return androidId;
    }

//    public static boolean checkPlayServices(Context context) {
//        int resultCode = GooglePlayServicesUtil
//                .isGooglePlayServicesAvailable(context);
//        if (resultCode != ConnectionResult.SUCCESS) {
//            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context,
//                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
//            } else {
//                Toast.makeText(context,
//                        "This device is not supported.", Toast.LENGTH_LONG)
//                        .show();
//                ((Activity) context).finish();
//            }
//            return false;
//        }
//        return true;
//    }

    public static String formatPrice(double price) {
        return String.format("%.2f", price);
    }


//    private static void logOut(final Context mContext) {
//        // Tag used to cancel the request
//        final ProgressDialog pDialog = new ProgressDialog(mContext);
//        pDialog.setMessage("Loading...");
//        pDialog.setCancelable(false);
//        pDialog.show();
//
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("userid", POLPreference.getStringPreference(mContext, ID));
////        params.put("reg_id", POLPreference.getStringPreference(mContext, GCM_REG_ID));
//        params.put("type", "android");
//        params.put("fcm_token_id", POLPreference.getStringPreference(mContext, FCM_TOKEN_ID));
//        CustomRequest request = new CustomRequest(Request.Method.POST, BASE_URL + "user/logout", params, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                // TODO Auto-generated method stub
//
//                try {
//                    if (response.get("response_code").equals("1")) {
////                        Toast.makeText(mContext, response.getString("message"), Toast.LENGTH_SHORT).show();
//                        if (!POLPreference.getStringPreference(mContext, FCM_TOKEN_ID).isEmpty() && mClearFCMTokenIDCallBack != null) {
//                            mClearFCMTokenIDCallBack.clearFcmTokenId();
//                        }
//                        Toast.makeText(mContext, "You have successfully logged out", Toast.LENGTH_SHORT).show();
//                        POLPreference.setBooleanPreference(mContext, IS_LOGIN, false);
//                        UsersListDAO usersListDAO = new UsersListDAO(mContext);
//                        usersListDAO.deleteAllDataFromDB();
//                        Intent i = new Intent(mContext, LoginActivity.class);
//                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        mContext.startActivity(i);
//                        ((Activity) mContext).finish();
//                    } else {
//                        Toast.makeText(mContext, response.getString("message"), Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    Toast.makeText(mContext, "Error in Logout", Toast.LENGTH_SHORT).show();
//                }
//                if (pDialog != null) {
//                    pDialog.dismiss();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                // TODO Auto-generated method stub
//                if (pDialog != null) {
//                    pDialog.dismiss();
//                    Toast.makeText(mContext, "Error in Logout", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        AppController.getInstance().addToRequestQueue(request, "logout");
//    }






    public static boolean checkSupportedFromat(String attachmentPath) {
        if (/*attachmentPath.contains(".pdf") ||
                attachmentPath.contains(".doc") ||*/
                attachmentPath.contains(".png") ||
                        attachmentPath.contains(".jpg") ||
                        attachmentPath.contains(".jpeg"))


            return true;
        else return false;
    }

    public interface Method {
        void execute();
    }






    public static  boolean isYesterday(Date d){
        return DateUtils.isToday(d.getTime() + DateUtils.DAY_IN_MILLIS);
    }

    public static  boolean isTommorrow(Date d){
        return DateUtils.isToday(d.getTime() - DateUtils.DAY_IN_MILLIS);
    }
//DateUtils.isToday(date.getTime())  //it is used to check Todays date.

    /**
     * Get a diff between two dates
     *
     * @param oldDate the old date
     * @param newDate the new date
     * @return the diff value, in the days
     */
    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }



    public static RequestBody toRequestBody(String value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }



}