package hitech.robotic.systemzz.fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import hitech.robotic.systemzz.HttpHandler;
import hitech.robotic.systemzz.R;

import java.util.Objects;



public class HomeFragment extends Fragment {

    private static final String TAG = "Home";
    public String jsonstr;
    String url = "http://new.clubd2s.com/";
    private int internet = 0;
    private WebView wv1;
    private RelativeLayout relativeLayout;
    TextView progressBarText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        wv1 = view.findViewById(R.id.web_view);
        relativeLayout = view.findViewById(R.id.internet);
        progressBarText = view.findViewById(R.id.progress_bar_text);
        progressBarText.setVisibility(View.VISIBLE);
      //  checkConnectivity();

        return view;
    }

    private int checkConnectivity() {
        boolean enabled = true;

        if (getActivity() != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) Objects.requireNonNull(getActivity()).getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = Objects.requireNonNull(connectivityManager).getActiveNetworkInfo();

            if ((info == null || !info.isConnected() || !info.isAvailable())) {
                internet = 0;//sin conexion
                //Toast.makeText(getApplicationContext(), "Sin conexión a Internet...", Toast.LENGTH_SHORT).show();
                enabled = false;
            } else {
                internet = 1;//conexión
                new GetContacts().execute();
            }
        }
        return internet;
    }

    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            jsonstr = sh.makeServiceCall(url);

            jsonstr = parsing(jsonstr);
            Log.d(TAG, "doInBackground: " + jsonstr);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (getActivity() != null) {
                if (internet == 1) {
                    progressBarText.setVisibility(View.GONE);
                    wv1.setVisibility(View.VISIBLE);
                    wv1.loadUrl(url);
                    wv1.getSettings().setDomStorageEnabled(true);
                    wv1.getSettings().setSaveFormData(true);
                    wv1.getSettings().setAllowContentAccess(true);
                    wv1.getSettings().setAllowFileAccess(true);
                    wv1.getSettings().setAllowFileAccessFromFileURLs(true);
                    wv1.canGoBack();
                    wv1.setOnKeyListener(new View.OnKeyListener() {

                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK
                                    && event.getAction() == MotionEvent.ACTION_UP
                                    && wv1.canGoBack()) {
                                wv1.goBack();
                                return true;
                            }
                            return false;
                        }
                    });
                    wv1.getSettings().setJavaScriptEnabled(true);
                    wv1.setWebViewClient(new WebViewClient());
                    wv1.setWebViewClient(new WebViewClient() {

                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            Log.d(TAG, "shouldOverrideUrlLoading: "+url);
                            progressBarText.setVisibility(View.GONE);
                            return true;
                        }

                        @Override
                        public void onLoadResource(WebView view, String url) {
                            super.onLoadResource(view, url);
                            progressBarText.setVisibility(View.VISIBLE);
                        }
                    });
                } else {
                    progressBarText.setVisibility(View.GONE);
                    relativeLayout.setBackgroundResource(R.drawable.internet);
                    wv1.setVisibility(View.INVISIBLE);
                }
            }

        }

        String parsing(String s) {
            Log.e("SSSSS>>>", s+"");
            int loc = s.indexOf("Tracker_URL");
            String json = s.substring(loc + 1);
            json = json.substring(json.indexOf("h"));
            json = json.replaceAll("\\\\", "");
            json = json.replaceAll("\\}", "");
            json = json.replaceAll("\\]", "");
            json = json.replaceAll("\\\"", "");
            return json;
        }

    }


}
