package hitech.robotic.systemzz;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class save trip data in database.
 */


public class DBHandlerWalk extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "onecroresteps.db";
    private static final String TAG = "DBHandlerWalk";
    public static final String TABLE_GPS_NAME = "GpsTable";
    public static final String TABLE_GPS_COLUMN_TIMESTAMP = "Timestamp";
    public static final String TABLE_GPS_COLUMN_LAT = "Latitude";
    public static final String TABLE_GPS_COLUMN_LON = "Longitude";
    public static final String TABLE_GPS_COLUMN_TRIPNUM = "Tripnum";

    public static final String TABLE_TRIP_NAME = "TripTable";
    public static final String TABLE_TRIP_COLUMN_TRIPNUM = "TripNum";
    public static final String TABLE_TRIP_COLUMN_START_TIMESTAMP = "StartTimestamp";
    public static final String TABLE_TRIP_COLUMN_END_TIMESTAMP = "EndTimestamp";
    public static final String TABLE_TRIP_COLUMN_STEPS = "Steps";
    public static final String TABLE_TRIP_COLUMN_DISTANCE = "Distance";
    public static final String TABLE_TRIP_COLUMN_UPLOAD_STATUS = "UploadStatus";


    public static final String TABLE_REG_NAME = "RegDetails";
    public static final String TABLE_REG_COLUMN_ID = "RegistrationID";
    public static final String TABLE_REG_COLUMN_NAME = "Name";
    public static final String TABLE_REG_COLUMN_EMAILID = "EmailId";
    public static final String TABLE_REG_COLUMN_PHONENUM = "Phonenum";

    private static DBHandlerWalk instance;

    public static DBHandlerWalk Instance(Context context)
    {
        if(instance ==  null)
            instance = new DBHandlerWalk(context, DATABASE_NAME, null, DATABASE_VERSION);
        return instance;
    }

    /**
     * Initialize the database.
     */
    private DBHandlerWalk(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {

        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        Log.d(TAG, "DB: Creating database: " + DATABASE_NAME );
        //SQLiteDatabase db = this.getWritableDatabase();
        //onCreate(db);
    }

    /**
     * Creating Database
     * @param db
     */

    @Override
    public void onCreate(SQLiteDatabase db) {
        //deleteDatabase();

        Log.d(TAG, "DB: Creating new table " + TABLE_GPS_NAME + " in database " + DATABASE_NAME);
        String CREATE_GPS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_GPS_NAME + "("
                + TABLE_GPS_COLUMN_TIMESTAMP + " INTEGER PRIMARY KEY, "
                + TABLE_GPS_COLUMN_LAT + " TEXT, "
                + TABLE_GPS_COLUMN_LON + " TEXT, "
                + TABLE_GPS_COLUMN_TRIPNUM + " INTEGER)";
        Log.d(TAG, "SQLstring : " + CREATE_GPS_TABLE);

        db.execSQL(CREATE_GPS_TABLE);
        Log.d(TAG, "DB: Created new table " + TABLE_GPS_NAME + " in database " + DATABASE_NAME);
        //Log.d(TAG, "DB: Table data : ");
        //Log.d(TAG, "DB: " + loadHandler(TABLE_GPS_NAME));

        Log.d(TAG, "DB: Creating new table " + TABLE_TRIP_NAME + " in database " + DATABASE_NAME);
        String CREATE_TRIP_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TRIP_NAME + "("
                + TABLE_TRIP_COLUMN_TRIPNUM + " INTEGER PRIMARY KEY, "
                + TABLE_TRIP_COLUMN_START_TIMESTAMP + " INTEGER, "
                + TABLE_TRIP_COLUMN_END_TIMESTAMP + " INTEGER, "
                + TABLE_TRIP_COLUMN_STEPS + " INTEGER, "
                + TABLE_TRIP_COLUMN_DISTANCE + " FLOAT, "
                + TABLE_TRIP_COLUMN_UPLOAD_STATUS + " INTEGER DEFAULT 0 )";
        Log.d(TAG, "SQLstring : " + CREATE_TRIP_TABLE);

        db.execSQL(CREATE_TRIP_TABLE);
        Log.d(TAG, "DB: Created new table " + TABLE_TRIP_NAME + " in database " + DATABASE_NAME);
        //Log.d(TAG, "DB: Table data : ");
        //Log.d(TAG, "DB: " + loadHandler(TABLE_TRIP_NAME));

        //Log.d(TAG, "DB: Reg Table data : ");
        //Log.d(TAG, "DB: " + loadHandler(TABLE_REG_NAME));
    }

    /**
     * Delete database
     */
    public void deleteDatabase()
    {
        Log.d(TAG, "DB: Deleting the entire Database!!!!");
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Log.d(TAG, "DB: Deleting the table" + TABLE_GPS_NAME);
            String str = "DROP TABLE IF EXISTS " + TABLE_GPS_NAME;
            db.execSQL(str);

            Log.d(TAG, "DB: Deleting the table" + TABLE_TRIP_NAME);
            String str1 = "DROP TABLE IF EXISTS " + TABLE_TRIP_NAME;
            db.execSQL(str1);

            Log.d(TAG, "DB: Deleting the registration table");
            String str2 = "DROP TABLE IF EXISTS " + TABLE_REG_NAME;
            db.execSQL(str2);
        }
        catch (SQLException e) {
            Log.d(TAG, "Database error.");
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REG_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GPS_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRIP_NAME);
        onCreate(db);
    }

    public String loadHandler(String tableName) {
        String result = "";
        try {
            String query = "Select * FROM " + tableName;
            Log.d(TAG, "DB: loadHandler string : " + query);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            if (cursor != null) {
                Log.d(TAG, "DB: loadHandler : " + tableName);
                if (tableName == TABLE_GPS_NAME) {
                    while (cursor.moveToNext()) {
                        int result_0 = cursor.getInt(0);
                        String result_1 = cursor.getString(1);
                        String result_2 = cursor.getString(2);
                        int result_3 = cursor.getInt(3);
                        result += String.valueOf(result_0) + " " + result_1 + " " + result_2 + " "
                                + String.valueOf(result_3) + " " +
                                System.getProperty("line.separator");
                        Log.d(TAG, result);
                    }
                } else if (tableName == TABLE_REG_NAME) {
                    while (cursor.moveToNext()) {
                        int result_0 = cursor.getInt(0);
                        String result_1 = cursor.getString(1);
                        String result_2 = cursor.getString(2);
                        String result_3 = cursor.getString(3);
                        result += String.valueOf(result_0) + " " + result_1 + " " + result_2 + " "
                                + result_3
                                + System.getProperty("line.separator");
                        Log.d(TAG, result);
                    }
                } else if (tableName == TABLE_TRIP_NAME) {
                    while (cursor.moveToNext()) {
                        int result_0 = cursor.getInt(0);
                        int result_1 = cursor.getInt(1);
                        int result_2 = cursor.getInt(2);
                        float result_3 = cursor.getFloat(3);
                        result += String.valueOf(result_0) + " " + String.valueOf(result_1) + " " +
                                String.valueOf(result_2) + " " + String.valueOf(result_3) +
                                System.getProperty("line.separator");
                        Log.d(TAG, result);
                    }
                }
                cursor.close();
            }
        } catch (SQLException e) {
            Log.d(TAG, "Database error.");
        }
        Log.d(TAG, "DB: loadHandler **********");
        return result;
    }

    public void addHandler(GpsLocation gpsloc) {
        Log.d(TAG,"DB: Adding a record to the table " + TABLE_GPS_NAME);
        Log.d(TAG, gpsloc.getTimestamp() + " " + gpsloc.getLatitude() + " " +
                gpsloc.getLongitude() + " " + gpsloc.getTripnum());
        ContentValues values = new ContentValues();
        values.put(TABLE_GPS_COLUMN_TIMESTAMP, gpsloc.getTimestamp());
        values.put(TABLE_GPS_COLUMN_LAT, gpsloc.getLatitude());
        values.put(TABLE_GPS_COLUMN_LON, gpsloc.getLongitude());
        values.put(TABLE_GPS_COLUMN_TRIPNUM, gpsloc.getTripnum());
        SQLiteDatabase db = this.getWritableDatabase();
      //  db.insert(TABLE_GPS_NAME, null, values);
        //Log.d(TAG, "DB: Table data : ");
        //Log.d(TAG, "DB: " + loadHandler(TABLE_GPS_NAME));
    }

    /**
     * Setting trip data to save in database.
     * @param tripInfo
     */
    public void addHandler(TripInfo tripInfo) {
        Log.d(TAG,"DB: Adding a record to the table " + TABLE_TRIP_NAME);
        Log.d(TAG, tripInfo.getTripnum() + " " + tripInfo.getStartTimestamp() + " " + tripInfo.getEndTimestamp()
                + " " + tripInfo.getSteps()  + " " + tripInfo.getDistance() + " " + tripInfo.getUploadStatus());
        ContentValues values = new ContentValues();
        values.put(TABLE_TRIP_COLUMN_TRIPNUM, tripInfo.getTripnum());
        values.put(TABLE_TRIP_COLUMN_END_TIMESTAMP, tripInfo.getStartTimestamp());
        values.put(TABLE_TRIP_COLUMN_END_TIMESTAMP, tripInfo.getEndTimestamp());
        values.put(TABLE_TRIP_COLUMN_STEPS, tripInfo.getSteps());
        values.put(TABLE_TRIP_COLUMN_DISTANCE, tripInfo.getDistance());
        values.put(TABLE_TRIP_COLUMN_UPLOAD_STATUS, tripInfo.getUploadStatus());
      //  SQLiteDatabase db = this.getWritableDatabase();
      //  db.insert(TABLE_TRIP_NAME, null, values);
        //Log.d(TAG, "DB: Table data : ");
        //Log.d(TAG, "DB: " + loadHandler(TABLE_TRIP_NAME));
    }

    /**
     * Insert data into the database.
     * @param regInfo
     */
    public void addHandler(RegInfo regInfo) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d(TAG, "DB: Deleting the registration if it exists");
        String str2 = "DROP TABLE IF EXISTS " + TABLE_REG_NAME;
        db.execSQL(str2);
        Log.d(TAG, "DB: Creating new table " + TABLE_REG_NAME + " in database " + DATABASE_NAME);
        String CREATE_REG_TABLE = "CREATE TABLE " + TABLE_REG_NAME + "(" + TABLE_REG_COLUMN_ID +
                " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TABLE_REG_COLUMN_NAME + " TEXT, "
                + TABLE_REG_COLUMN_EMAILID + " TEXT, "
                + TABLE_REG_COLUMN_PHONENUM + " TEXT)";
        Log.d(TAG, "SQLstring : " + CREATE_REG_TABLE);

        db.execSQL(CREATE_REG_TABLE);
        Log.d(TAG, "DB: Created new table " + TABLE_REG_NAME + " in database " + DATABASE_NAME);
        Log.d(TAG,"DB: Adding a record to the table " + TABLE_REG_NAME);
        Log.d(TAG, regInfo.getRegid() + " " + regInfo.getName() + " " + regInfo.getEmailid() + " " + regInfo.getPhonenum());
        ContentValues values = new ContentValues();
        values.put(TABLE_REG_COLUMN_NAME, regInfo.getName());
        values.put(TABLE_REG_COLUMN_EMAILID, regInfo.getEmailid());
        values.put(TABLE_REG_COLUMN_PHONENUM, regInfo.getPhonenum());
        db.insert(TABLE_REG_NAME, null, values);
        //Log.d(TAG, "DB: Table data : ");
        //Log.d(TAG, "DB: " + loadHandler(TABLE_REG_NAME));
    }

    /**
     * Updating registration table in database.
     * @param regInfo
     * @return
     */
    public int updateHandler(RegInfo regInfo) {
        int retval = 0;
        Log.d(TAG, "DB: Updating Registration table in database " + DATABASE_NAME);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TABLE_REG_COLUMN_NAME, regInfo.getName());
        values.put(TABLE_REG_COLUMN_EMAILID, regInfo.getEmailid());
        values.put(TABLE_REG_COLUMN_PHONENUM, regInfo.getPhonenum());
        //we are giving the id as 1 since we are storing the details of only user for now
        regInfo.setRegid(1);
        Log.d(TAG, regInfo.getRegid() + " " + regInfo.getName() + " " + regInfo.getEmailid() + " " + regInfo.getPhonenum());
        retval = db.update(TABLE_REG_NAME, values,TABLE_REG_COLUMN_ID+"=1",null);
        //Log.d(TAG, "DB: Table data : ");
        //Log.d(TAG, "DB: " + loadHandler(TABLE_REG_NAME));
        return retval;
    }

    public GpsLocation findHandler(String tableName)
    {
        return null;
    }


    public String getPhoneNum()
    {
        RegInfo reg = findRegHandler();
        if (reg != null)
            return reg.getPhonenum();
        else
            return "";
    }
    public RegInfo findRegHandler()
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String query = "SELECT * FROM " + TABLE_REG_NAME;
            Cursor cursor = db.rawQuery(query, null);
            String result = "";
            if (cursor != null) {
                Log.d(TAG, "DB: findHandler : " + TABLE_REG_NAME);
                if (cursor.moveToFirst()) {
                    int result_0 = cursor.getInt(0);
                    String result_1 = cursor.getString(1);
                    String result_2 = cursor.getString(2);
                    String result_3 = cursor.getString(3);
                    result += String.valueOf(result_0) + " " + result_1 + " " + result_2 + " "
                            + result_3
                            + System.getProperty("line.separator");
                    Log.d(TAG, result);
                    RegInfo reg = new RegInfo(result_1, result_2, result_3);
                    return reg;
                }
            }
        } catch (SQLiteException e)
        {

        }
        return null;
    }

    public int getNextTripNum()
    {
        //get from trip table
        TripInfo trip =  getLastTripEntry();
        if (trip != null) {
            Log.d(TAG, "Last trip num : " + trip.getTripnum());
            return trip.getTripnum() + 1;
        }
        else {
            return 1;//consider it as the first trip
        }
    }

    public boolean exists(String table) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.rawQuery("SELECT * FROM " + table, null);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean registeredUser()
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String str = "Select * FROM "  + TABLE_REG_NAME;
            Cursor cursor = db.rawQuery(str, null);
            if(cursor.moveToFirst()) {
                return true;
            }
            cursor.close();
            return false;
        } catch (SQLException e) {
            return false;
        }
    }

    public int getTripStepCount(String dt)
    {
        int totalStepCount = 0;
        int tripNum = 0;
        int steps = 0;
        long startTS = 0;
        long stopTS = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy'T'hh:mm:ss");
        try {
            Log.d(TAG, "DB: date : "+dt+"formatted data : "+formatter.parse(dt+"T00:00:00"));
            Date gmt1 = formatter.parse(dt+"T00:00:00");
            startTS = gmt1.getTime()/1000;
            Log.d(TAG, "start time : "+String.valueOf(startTS));
            Date gmt2 = formatter.parse(dt+"T23:59:59");
            stopTS = gmt2.getTime()/1000;
            Log.d(TAG, "stop time : "+String.valueOf(stopTS));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String subquery = TABLE_TRIP_COLUMN_END_TIMESTAMP
                + " >= " + String.valueOf(startTS)
                + " and " + TABLE_TRIP_COLUMN_END_TIMESTAMP + " <= " + String.valueOf(stopTS)
                + " and " + TABLE_TRIP_COLUMN_UPLOAD_STATUS + " == 0";
        String query = "Select*FROM " + TABLE_TRIP_NAME + " WHERE " + subquery;
        Log.d(TAG, "Query str : "+query);
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cur = db.rawQuery(query, null);
            for(cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext())
            {
                tripNum = cur.getInt(0);
                steps = cur.getInt(3);
                Log.d(TAG, "Retrieving step count of tripnum : "+tripNum+" steps : "+ steps);
                totalStepCount += steps;
            }
            ContentValues cv = new ContentValues();
            cv.put(TABLE_TRIP_COLUMN_UPLOAD_STATUS, 1);//set the upload status as 1
            db.update(TABLE_TRIP_NAME, cv, subquery, null);
        } catch(SQLException e)
        {
            Log.d(TAG, "Database error. "+e);
        }

        return totalStepCount;
    }

    /**
     * Get last trip entry.
     * @return
     */
    public TripInfo getLastTripEntry()
    {
        TripInfo trip = new TripInfo();
        String query = "Select*FROM " + TABLE_TRIP_NAME;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cur = db.rawQuery(query, null);
            if ((cur != null) && (cur.moveToLast()))
            {
                trip.setTripnum(cur.getInt(0));
                Log.d(TAG, "Retrieving last trip record from triptable. Last tripnum : "+trip.getTripnum());
                trip.setStartTimestamp(cur.getInt(1));
                trip.setEndTimestamp(cur.getInt(2));
                trip.setSteps(cur.getInt(3));
                trip.setDistance(cur.getFloat(4));
                trip.setUploadStatus(cur.getInt(5));
            }
        } catch(SQLException e)
        {
            Log.d(TAG, "Database error. "+e);
        }

        return trip;
    }

    public GpsLocation getLastGPSEntry()
    {
        GpsLocation loc = new GpsLocation();
        String query = "Select*FROM " + TABLE_GPS_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(query, null);
            if ((cursor != null) && (cursor.moveToLast()) && (loc != null)) {
                int result_0 = cursor.getInt(0);
                loc.setTimestamp(result_0);
                String result_1 = cursor.getString(1);
                loc.setLatitude(result_1);
                String result_2 = cursor.getString(2);
                loc.setLongitude(result_2);
                int result_3 = cursor.getInt(3);
                loc.setTripnum(result_3);
                return loc;
            }
        } catch (SQLException e) {
            return null;
        }
        return null;
    }

    public boolean deleteHandler(String tableName) {
        boolean result = false;
        String query = "Select*FROM " + tableName;
        //String query = "Select*FROM" + TABLE_NAME + "WHERE" + COLUMN_ID + "= '" + String.valueOf(ID) + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        GpsLocation gpsloc = new GpsLocation();
        if ((cursor != null) && cursor.moveToFirst()) {
            db.delete(tableName, TABLE_GPS_COLUMN_TIMESTAMP,null);
            cursor.close();
            result = true;
        }
        //db.close();
        return result;
    }

    public boolean updateHandler(long timestamp, String latitude, String longitude, float speed) {
        return false;
    }
}
