package hitech.robotic.systemzz.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import hitech.robotic.systemzz.CameraActivity;
import hitech.robotic.systemzz.R;
import hitech.robotic.systemzz.utils.Constants;
import hitech.robotic.systemzz.utils.Utils;

public class UploadVideoService extends Service  implements
        ConnectivityReceiver.ConnectivityReceiverListener{


    private BroadcastReceiver command_broadcast_receiver;
    private ConnectivityReceiver mConnectivityReceiver;
    private boolean isUploadingData = false;
    private boolean isFirstTime = true;
    private NotificationCompat.Builder mBuilder;

    private NotificationManager mNotificationManager;
    @Override
    public void onCreate()
    {
        super.onCreate();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startMyOwnForeground();
            Log.e("uploading > ","start my own foreground");
        }

    }

    private void startMyOwnForeground(){

        if(mBuilder == null) {
            mBuilder =
                    new NotificationCompat.Builder(UploadVideoService.this, "notify_001");

            //uncomment it if you want to open any activity on click of notification
        /*Intent ii = new Intent(UploadVideoService.this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(UploadVideoService.this, 0, ii, 0);
        mBuilder.setContentIntent(pendingIntent);*/

            mBuilder.setSmallIcon(R.drawable.club);
            mBuilder.setContentTitle("Clubd2s ");
            mBuilder.setContentText("Uploading...");
            mBuilder.setPriority(Notification.PRIORITY_MAX);

            Log.e("uploading > ","inside notification builder");

        }
        mNotificationManager =
                (NotificationManager) UploadVideoService.this.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "YOUR_CHANNEL_ID";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
            startForeground(2, mBuilder.getNotification());

            Log.e("uploading > ","inside notification channel code");
        }

        // mNotificationManager.notify(0, mBuilder.build());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Service", "on start command service");


        //To remove intenet connection callback use below line.
//        unregisterReceiver(mConnectivityReceiver);


        /* Handle any command sent by the app here */
        command_broadcast_receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Log.e("uploading > ", "Command recieving from broadcaste");

                try {
                    String command = intent.getStringExtra("command");
                    Log.e("uploading > ", "command recieved here is" + command);

                    /* Based on the command given, perform an action */
                    switch (command) {
                        case "RESTART RECOGNIZER":
                            Log.e("uploading > ", "RESTART RECOGNIZER Here");
                            break;
                        case "STOP":
                            Log.e("uploading > ", "service stopped Here");
                            if(mConnectivityReceiver != null) {
                                unregisterReceiver(mConnectivityReceiver);
                                Log.d("uploading > ", "unregister reciever");
                            }
                            stopSelf();
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(command_broadcast_receiver, new IntentFilter(Constants.PHRASE_SERVICE_BROADCAST_ID));
        Log.e("uploading > ", "Registering broadcaste to Service ");

        Log.e("uploading > ", "command to execute executeOnService");

        if(Utils.isNetworkAvailable(UploadVideoService.this))
        {
            CameraActivity mainActivity = new CameraActivity();
            mainActivity.getListOfTimeStampFromDB(this);
            Log.e("uploading > ", "Starting get list from DB");
        }
        else{
            //Toast.makeText(this, "Initializing internet Broadcaste", Toast.LENGTH_SHORT).show();
            Log.e("uploading > ","Initializing internet Broadcaste");
            mConnectivityReceiver = new ConnectivityReceiver(this);

            registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        }

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(command_broadcast_receiver);

        Log.e("uploading > ", " command broadcast unregister in on Destroy");
        super.onDestroy();
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.e("uploading > ", "onBind method");
        return null;
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected, Context context) {
        String message = isConnected ? "Good! Connected to Internet" : "Sorry! Not connected to internet";
       if(isConnected) {
           CameraActivity mainActivity = new CameraActivity();
           mainActivity.getListOfTimeStampFromDB(this);
       }

    }

}