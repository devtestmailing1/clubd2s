package hitech.robotic.systemzz;

import com.google.gson.annotations.SerializedName;

public class LoginResponseResult {
    @SerializedName("QueryResponse")
    public QueryResponseResult queryResponse;

    public QueryResponseResult getQueryResponse() {
        return queryResponse;
    }
}
